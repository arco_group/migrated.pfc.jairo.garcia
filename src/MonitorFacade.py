#!/usr/bin/python
# -*- mode:python; coding:utf-8; tab-width:4 -*-

from dindesConf import slices, systemAdapters, systemTimes, adminInfo, mailConf
from Exceptions import UnsubscribeMonitorException, SubscribeMonitorException

import Ice
Ice.loadSlice(slices['dindesSliceLocal'])
import DInDeS

from datetime import datetime
from time import time, asctime
import smtplib
from email.mime.text import MIMEText

from MonitorModel import MonitorModel



#===============================================================================
# Monitor class
#===============================================================================
class MonitorFacade ():
    def __init__(self):
        self.__inputDataManager = InputDataManager()
        self.__informationManager = InformationReceivedManager(self.__inputDataManager)
        self.__mailNotificationManager = MailNotificationManager(self.__informationManager)
        self.__subcriptionManager = SubscriptionManager(self.__inputDataManager, self.__informationManager, self.__mailNotificationManager)

#------------------------------------------------------------------------------ 
# manager getters
#------------------------------------------------------------------------------
    def getInputDataManager(self):
        return self.__inputDataManager

    def getInformationManager(self):
        return self.__informationManager

    def getSubscriptionManager(self):
        return self.__subcriptionManager

    def getMailNotificationManager(self):
        return self.__mailNotificationManager

#------------------------------------------------------------------------------ 
# inputDataManager
#------------------------------------------------------------------------------
    def getIceInstance(self):
        return self.__inputDataManager.getArguments()

    def setIceInstance(self, iceInstance):
        self.__inputDataManager.setIceInstance(iceInstance)

#------------------------------------------------------------------------------ 
# informationManager
#------------------------------------------------------------------------------
    def createDataBase(self):
        self.__informationManager.createDataBase(self.getProbesMonitorized())

    def createLogFiles(self):
        self.__informationManager.createLogFiles()

    def createGraphs(self):
        self.__informationManager.createGraphs()

    def addAlert(self, brokenRule):
        self.__informationManager.addAlert(brokenRule)
        self.__mailNotificationManager.brokenRuleSendMail(brokenRule)

    def addInformation(self, dataSent):
        self.__informationManager.addInformation(dataSent)

    def addInfoToGeneralLog(self, info):
        self.__informationManager.addInfoToGeneralLog(info)

#------------------------------------------------------------------------------ 
# subscriptionManager
#------------------------------------------------------------------------------
    def getProbesMonitorized(self):
        return self.__subcriptionManager.getProbesMonitorized()

    def setProbesMonitorized(self, newProbeInfo):
        self.__subcriptionManager.setProbesMonitorized(newProbeInfo)

    def subscribeMonitorMethodsOnProbe(self, methods, probeProxy, iceInstance):
        self.__subcriptionManager.subscribeMethodsOnProbe(methods, probeProxy, iceInstance)

    def unsubscribeMonitorMethodsOnProbe(self):
        self.__subcriptionManager.unsubscribeMonitorMethodsOnProbe()

    def pingProbes(self):
        self.__subcriptionManager.pingProbes()

    def tryToSubscribe(self, monitorMethods, probesNotSubscribed):
        self.__subcriptionManager.tryToSubscribe(monitorMethods, probesNotSubscribed)

    def getNumberOfAlerts(self, probeProxy):
        return self.__subcriptionManager.getNumberOfAlerts(probeProxy)

    def getNumberOfData(self, probeProxy):
        return self.__subcriptionManager.getNumberOfData(probeProxy)



#===============================================================================
# InputDataManager class
#===============================================================================
class InputDataManager():
    def __init__(self):
        self.__arguments = []
        self.__iceInstance = None

#------------------------------------------------------------------------------ 
# getters & setters
#------------------------------------------------------------------------------
    def getIceInstance(self):
        return self.__iceInstance

    def setIceInstance(self, iceInstance):
        self.__iceInstance = iceInstance



#===============================================================================
# InformationReceivedManager class
#===============================================================================
class InformationReceivedManager():
    def __init__(self, argsManager):
        self.__argsManager = argsManager
        self.__monitorModel = MonitorModel()

#------------------------------------------------------------------------------ 
# getters
#------------------------------------------------------------------------------ 
    def getMonitorModel(self):
        return self.__monitorModel

#------------------------------------------------------------------------------ 
# class methods
#------------------------------------------------------------------------------
    def createDataBase(self, probesMonitorized):
        self.__monitorModel.createDataBase(probesMonitorized)

    def createLogFiles(self):
        self.__monitorModel.createLogFiles()

    def createGraphs(self):
        self.__monitorModel.createInformationGraph()
        self.__monitorModel.createAlertsGraph()

    def addAlert(self, brokenRule):
        info = 'New broken rule from ' + str(brokenRule.probeId)
        info += ': ' + brokenRule.data
        self.__monitorModel.addInfoToGeneralLog(info)
        #self.__monitorModel.addAlert(brokenRule)        

    def addInformation(self, dataSent):
        self.__monitorModel.addInformation(dataSent)

    def addInfoToGeneralLog(self, info):
        self.__monitorModel.addInfoToGeneralLog(info)

    def setProbeStatus(self, probeStatus, proxy):
        return self.__monitorModel.setProbeStatus(probeStatus, proxy)



#===============================================================================
# mail notification system
#===============================================================================
class MailNotificationManager():
    def __init__(self, infoManager):
        self.__iManager = infoManager
        self.__lastMailSentTime = 0

    def brokenRuleSendMail(self, brokenRule):
        if self.__lastMailSentTime + systemTimes['stateAlertTimeout'] < time() and mailConf['ready']:
            self.__lastMailSentTime = time()
            content = self.__createAlertMailContent(brokenRule)
            self.__sendMail(content)
            self.__logSentMessageInfo('breaking alert rule')

    def probeStatusChangedSendMail(self, info):
        if mailConf['ready']:
            content = self.__createProbeStatusContent(info)
            self.__sendMail(content)
            self.__logSentMessageInfo('changing state')

#------------------------------------------------------------------------------ 
    def __createAlertMailContent(self, brokenRule):
        mailContent = "Dindes Alert Notification\n\n"
        mailContent += "Alert on: %s\n" % brokenRule.probeId
        mailContent += "Broken rule: %s\n" % brokenRule.data
        mailContent += "At: %s\n" % brokenRule.timestamp
        return mailContent

    def __createProbeStatusContent(self, info):
        mailContent = "Dindes Alert Notification\n\n"
        mailContent += "%s\n" % info
        mailContent += "At: %s\n" % asctime()
        return mailContent

    def __sendMail(self, content):
        msg = MIMEText(content)
        msg['Subject'] = 'Dindes Notification -- ALERT --'
        msg['From'] = 'Dindes System'
        msg['To'] = adminInfo['name']
        mailServer = self.__configMailServer()
        if mailServer:
            mailServer.sendmail(mailConf['mail'], adminInfo['mail'], msg.as_string())

    def __configMailServer(self):
        try:
            mailServer = smtplib.SMTP(mailConf['server'], mailConf['port'])
            mailServer.ehlo()
            mailServer.starttls()
            mailServer.ehlo()
            mailServer.login(mailConf['mail'], mailConf['pass'])
            return mailServer
        except Exception:
            return 'Error mail configuration'

    def __logSentMessageInfo(self, reason):
        msg = 'Alert mail sent to Admin (' + adminInfo['name'] + ') for ' + reason
        self.__iManager.addInfoToGeneralLog(msg)



#===============================================================================
# SubscriptionManager class
#===============================================================================
class SubscriptionManager():
    def __init__(self, argsManager, infoManager, mailManager):
        self.__argsManager = argsManager
        self.__infoManager = infoManager
        self.__mailManager = mailManager
        # Format probesMonitored{'Probe0':{'proxy':<probeProxy>,'probeId':<probeID>, 'monitorEndpoint':endpoint}}
        self.__probesMonitorized = {}

#------------------------------------------------------------------------------ 
# getters $ setters
#------------------------------------------------------------------------------
    def getProbesMonitorized(self):
        return self.__probesMonitorized

    def setProbesMonitorized(self, newProbeInfo):
        if not self.__isSaved(newProbeInfo):
            dicKey = 'Probe' + str(len(self.__probesMonitorized))
            self.__probesMonitorized.update({dicKey:newProbeInfo})

    def __isSaved(self, newProbeInfo):
        for i in self.__probesMonitorized.values():
            if newProbeInfo == i:
                return True
        return False

    def getNumberOfAlerts(self, probeProxy):
        probe = DInDeS.ProbeQueryPrx.checkedCast(probeProxy)
        result = probe.getNumberOfAlerts()
        return result

    def getNumberOfData(self, probeProxy):
        probe = DInDeS.ProbeQueryPrx.checkedCast(probeProxy)
        result = probe.getNumberOfData()
        return result

#------------------------------------------------------------------------------ 
# class methods
#------------------------------------------------------------------------------
    def subscribeMethodsOnProbe(self, mMethods, probeProxy, iceInstance, mEndpoint=None):
        self.__argsManager.setIceInstance(iceInstance)
        try:
            probe = DInDeS.ProbeQueryPrx.checkedCast(probeProxy)

            if not mEndpoint:
                endpointName = systemAdapters['monitorAdapter'] + datetime.now().strftime('%s%f')
                adapter = iceInstance.createObjectAdapterWithEndpoints(endpointName, 'tcp')
                monProxy = adapter.addWithUUID(mMethods)
                adapter.activate()
                mEndpoint = DInDeS.MonitorPrx.checkedCast(monProxy)

            probeInfo = probe.subscribeMonitor(mEndpoint)

            self.setProbesMonitorized({'proxy': probeProxy, 'probeId': probeInfo.probeId, 'monitorEndpoint':mEndpoint})
        except Exception, e:
            raise SubscribeMonitorException('There was an error while monitor subscribing: %s' % e)

    def unsubscribeMonitorMethodsOnProbe(self):
        #print 'PROXIES FACADE: ', proxies
        if len(self.__probesMonitorized) > 0:
            for p in self.__probesMonitorized.values():
                try:
                    probe = DInDeS.ProbeQueryPrx.checkedCast(p['proxy'])
                    probe.unsubscribeMonitor(p['monitorEndpoint'])
                except Exception, e:
                    print 'UnsubscribeMonitorException: %s' % e
            self.__probesMonitorized = {}
        else:
            raise UnsubscribeMonitorException('Monitor has not subscribed to any probe')

    def pingProbes(self):
        for p in self.__probesMonitorized:
            value = self.__probesMonitorized[p]
            try:
                probe = DInDeS.ProbeQueryPrx.checkedCast(value['proxy'])
                probeStatus = probe.ping()
                print 'PING (%s) OK' % str(value['proxy'])
            except Exception, e:
                #print 'EXCEPTION: %s' % str(e)
                print 'PING (%s) Fail' % str(value['proxy'])
                probeStatus = None
            statusChanged = self.__infoManager.setProbeStatus(probeStatus, value['proxy'])

            if statusChanged:
                self.__mailManager.probeStatusChangedSendMail(statusChanged)

            if not probeStatus:
                try:
                    self.subscribeMethodsOnProbe(None, value['proxy'], self.__argsManager.getIceInstance(), value['monitorEndpoint'])
                    print 'Subscribed again'
                except Exception:
                    print 'Not subscribed yet'

    def tryToSubscribe(self, monitorMethods, probesNotSubscribed):
        for p in probesNotSubscribed:
            if self.__isNotSubcribe(p):
                error = False
                try:
                    self.subscribeMethodsOnProbe(monitorMethods, p, self.__argsManager.getIceInstance())
                    print 'Subscribed'
                except Exception, e:
                    error = True

                if not error:
                    try: #creamos una nueva base de datos
                        self.__infoManager.createDataBase(self.getProbesMonitorized())
                        self.__infoManager.createLogFiles()
                        self.__infoManager.createGraphs()
                    except Exception, e:
                        print 'Error trying to subscribe again. %s' % e

    def __isNotSubcribe(self, probeEndpoint):
        for p in self.__probesMonitorized.values():
            if probeEndpoint == p['proxy']:
                return False
        return True



