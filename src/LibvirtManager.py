#!/usr/bin/python
# -*- mode:python; coding:utf-8; tab-width:4 -*-

import libvirt
import sys

#===============================================================================
# libvirt manager
#===============================================================================
class LibvirtManager():
    def __init__(self):
        self.conn = libvirt.open('qemu:///system')

    def run(self, args):
        try:
            aQuery = ArgumentQuery(args)
            optionList = aQuery.getOptionString()

            if self.__isStart(optionList):
                self.startVirtualMachineByName(args['startVMName'])

            elif self.__isStartAll(optionList):
                self.startAllVirtualMachines()

            elif self.__isStop(optionList):
                self.stopVirtualMachineByName(args['stopVMName'])

            elif self.__isStopAll(optionList):
                self.stopAllVirtualMachines()

            elif self.__isList(optionList):
                self.listNotRunningMachines()

            elif self.__isRunning(optionList):
                self.listRunningMachines()

            else:
                print "Error with parameters"
        except Exception, e:
            print 'Error: %s' % e

#------------------------------------------------------------------------------ 
# class methods
#------------------------------------------------------------------------------ 
    def startVirtualMachineByName(self, vmNames):
        for name in vmNames:
            vm = self.conn.lookupByName(name)
            print "Starting %s..." % name
            vm.create()
            print "%s started." % name

    def startAllVirtualMachines(self):
        allStoppedMachines = self.listNotRunningMachines()
        allStoppedMachines.remove('imagenDebian')
        print 'Starting %d machines...' % len(allStoppedMachines)
        for i in allStoppedMachines:
            self.startVirtualMachineByName([i])
        print 'All VM started.'

    def stopVirtualMachineByName(self, vmNames):
        for name in vmNames:
            vm = self.conn.lookupByName(name)
            print "Stopping %s..." % name
            vm.shutdown()
            print "%s stopped." % name

    def stopAllVirtualMachines(self):
        runningMachines = self.listRunningMachines()
        print 'Stopping %d machines...' % len(runningMachines)
        for i in runningMachines:
            self.stopVirtualMachineByName([i])
        print 'All VM stopped.'

    def listNotRunningMachines(self):
        print 'Listing not running machines...'
        machines = self.conn.listDefinedDomains()
        print machines
        print 'Done.'
        return machines

    def listRunningMachines(self):
        print 'Listing running machines...'
        runningMachines = []
        machinesIDs = self.conn.listDomainsID()
        for i in machinesIDs:
            machine = self.conn.lookupByID(i)
            runningMachines.append(machine.name())
        print runningMachines
        print 'Done.'
        return runningMachines

#------------------------------------------------------------------------------ 
# private class methods
#------------------------------------------------------------------------------ 
    def __isStart(self, options):
        if options == "100000":
            return True
        else:
            return False

    def __isStartAll(self, options):
        if options == "010000":
            return True
        else:
            return False

    def __isStop(self, options):
        if options == "001000":
            return True
        else:
            return False

    def __isStopAll(self, options):
        if options == "000100":
            return True
        else:
            return False

    def __isList(self, options):
        if options == "000010":
            return True
        else:
            return False

    def __isRunning(self, options):
        if options == "000001":
            return True
        else:
            return False

#===============================================================================
# argumentsQuery
#===============================================================================
class ArgumentQuery():
    def __init__(self, args):
        self.__args = args
        self.__optionString = ['0', '0', '0', '0', '0', '0']
        self.createOptionString()

    def getOptionString(self):
        res = ''
        for i in self.__optionString:
            res += i
        return res

    def createOptionString(self):
        if len(self.__args['startVMName']) > 0:
            self.__optionString[0] = '1'
        if self.__args['startAllVM']:
            self.__optionString[1] = '1'
        if len(self.__args['stopVMName']) > 0:
            self.__optionString[2] = '1'
        if self.__args['stopAllVM']:
            self.__optionString[3] = '1'
        if self.__args['listNotRunning']:
            self.__optionString[4] = '1'
        if self.__args['listRunning']:
            self.__optionString[5] = '1'



#===============================================================================
# libvirtScript main
#===============================================================================
if __name__ == '__main__':
    lvs = LibvirtManager()
    options = lvs.analyzeArguments(sys.argv[1:])
    lvs.run(options)
