#!/usr/bin/python
# -*- mode:python; coding:utf-8; tab-width:4 -*-

from dindesConf import networkFiles
from Exceptions import NoInputArgumentsIntroducedException, UnknownPromiscModeException
from Exceptions import UnknownNetworkIfaceException, NegativeTimevalSelectedException
from Exceptions import OptionParsingError, OptionParsingExit
from Exceptions import ArgumentParsingException, ArgumentParsingExitException

import Ice

import sys
from datetime import datetime
from optparse import OptionParser

from Probe import Probe



#===============================================================================
# ArgumentManager
#===============================================================================
class ArgumentManager():
    def analyzeArguments(self, args):
        #print 'ARGUMENTS: ', args
        if len(args) > 0:
            arguments = self.__parseArguments(args)
            self.__checkProbeName(arguments)
            self.__existsNetworkIface(arguments['iface'])
            self.__checkCorrectTimeval(arguments['timeval'])
            self.__checkPromiscMode(arguments['promisc'])
            return arguments
        else:
            raise NoInputArgumentsIntroducedException('There are no typed arguments')

    def __checkProbeName(self, arguments):
        if len(arguments['probeName']) == 0:
            arguments['probeName'] = 'Probe' + datetime.now().strftime('%s%f')

    def __existsNetworkIface(self, iface):
        existsIface = False
        for line in open(networkFiles['devFile'], 'r'):
            if iface in line:
                existsIface = True
        if not existsIface:
            raise UnknownNetworkIfaceException('Unknown network iface: ' + iface)

    def __checkCorrectTimeval(self, timeval):
        if timeval < 0:
            raise NegativeTimevalSelectedException('Negative timeval: ' + str(timeval))

    def __checkPromiscMode(self, promiscMode):
        if str(promiscMode) != 'True' and str(promiscMode) != 'False':
                raise UnknownPromiscModeException('Unknown promisc mode: ' + str(promiscMode))

    def __parseArguments(self, arguments):
        result = {}
        parser = _MyOptionParser()
        parser.add_option("-n", "--name" , dest="probeName" , default='' , help="Choose a name", metavar="PROBE_NAME")
        parser.add_option("-p", "--plugin" , dest="plugin" , default=None , help="Choose a plugin", metavar="PLUGIN_NAME")
        parser.add_option("-i", "--iface"  , dest="iface"  , default='lo' , help="Choose a iface" , metavar="IFACE_NAME")
        parser.add_option("-t", "--timeval", dest="timeval", default=10   , help="Choose a time interval", metavar="TIME_INTERVAL", type='int')
        parser.add_option("-r", "--rule"   , dest="rule"   , default=[]   , help="Append a new alert rule", metavar="ALERT_RULE", action='append')
        parser.add_option("-m", "--mode", dest="promisc", default=False, help="Choose promisc mode", metavar="PROMISC_MODE")
        parser.add_option("-s", "--iceStorm", dest="topicProxy", default=None, help="Choose a topic manager proxy", metavar="TOPIC_PROXY")
        try:
            (result, args) = parser.parse_args(arguments)
            return (result.__dict__)
        except OptionParsingError, e:
            raise ArgumentParsingException('Parsing error with arguments: %s' % e.msg)
        except OptionParsingExit, e:
            raise ArgumentParsingExitException('Parser exited with result code %s' % e.status)



#=============================================================================== 
# _MyOptionParser class
#===============================================================================
class _MyOptionParser(OptionParser):
    def error(self, msg):
        raise OptionParsingError(msg)

    def exit(self, status=0, msg=None):
        raise OptionParsingExit(status, msg)



#===============================================================================
# ProbeMain class
#===============================================================================
class ProbeMain(Ice.Application):
    def __init__(self):
        self.__aManager = ArgumentManager()
        self.__probe = Probe()

    def run(self, argv):
        self.shutdownOnInterrupt()
        ic = self.communicator()

        try:
            args = self.__aManager.analyzeArguments(argv[1:])
        except ArgumentParsingException, e:
            print 'MainError: ArgumentParsingException: ', e
            return 0
        except ArgumentParsingExitException, e:
            print 'MainError: ArgumentParsingExitException: ', e
            return 0
        except NoInputArgumentsIntroducedException, e:
            print 'MainError: NoInputArgumentsIntroducedException: ', e
            return 0

        resultFail = self.__probe.configureProbe(args, ic)
        if not resultFail:
            print 'Working...'
            print 'Press Ctrl+C for stopping.'
            self.__probe.executeProbe()
            ic.waitForShutdown()
            print 'Stopped!'
        return 0

#===============================================================================
# main
#===============================================================================
if __name__ == "__main__":
    if len(sys.argv) == 1:
        print 'Usage: ProbeMain <Probe configuration arguments>'
    else:
        ProbeMain().main(sys.argv)



