#!/usr/bin/python
# -*- mode:python; coding:utf-8; tab-width:4 -*-

from dindesConf import slices, systemProxies

import Ice, IceGrid
Ice.loadSlice(slices['dindesSliceLocal'])
import DInDeS

import sys

from RouterShell import RouterShell


#===============================================================================
# ArgumentManager
#===============================================================================
class ArgumentManager():
#------------------------------------------------------------------------------ 
# checking methods
#------------------------------------------------------------------------------ 
    def thereIsSomeArguments(self, options):
        if options != None:
            return True
        else:
            return False

    def onlyRouterProxiesArgument(self, optionList):
        if int(optionList, 2) == 64:
            return True
        else:
            return False

    def addNewRouterActionOption(self, optionList):
        if int(optionList, 2) == 65:
            return True
        else:
            return False

    def deleteAppliedActionsOption(self, optionList):
        if int(optionList, 2) == 96:
            return True
        else:
            return False

    def sentActionsOption(self, optionList):
        if int(optionList, 2) == 80:
            return True
        else:
            return False

    def getRouterNameOption(self, optionList):
        if int(optionList, 2) == 72:
            return True
        else:
            return False

    def pingRouterOption(self, optionList):
        if int(optionList, 2) == 68:
            return True
        else:
            return False

    def getRouterIfacesOption(self, optionList):
        if int(optionList, 2) == 66:
            return True
        else:
            return False



#===============================================================================
# argumentsQuery
#===============================================================================
class ArgumentQuery():
    def __init__(self, args):
        self.__args = args
        self.__optionString = ['0', '0', '0', '0', '0', '0', '0']
        self.createOptionString()

    def getOptionString(self):
        res = ''
        for i in self.__optionString:
            res += i
        return res

    def createOptionString(self):
        if self.__args['routerProxy'] > -1:
            self.__optionString[0] = '1'

        if self.__args['delete']:
            self.__optionString[1] = '1'
        if self.__args['sent']:
            self.__optionString[2] = '1'
        if self.__args['name']:
            self.__optionString[3] = '1'
        if self.__args['ping']:
            self.__optionString[4] = '1'
        if self.__args['ifaces']:
            self.__optionString[5] = '1'

        if self.__args['transportProto'] or self.__args['direction'] or self.__args['ipAddress'] or self.__args['port'] or self.__args['typeAction']:
            self.__optionString[6] = '1'



#===============================================================================
# RouterShellMain class
#===============================================================================
class RouterShellMain():
    def __init__(self):
        self.__routerShell = RouterShell()
        self.__argumentManager = ArgumentManager()

#------------------------------------------------------------------------------ 
# getters
#------------------------------------------------------------------------------ 
    def getRouterShell(self):
        return self.__routerShell

    def getArgumentManager(self):
        return self.__argumentManager

#------------------------------------------------------------------------------ 
# start router shell method
#------------------------------------------------------------------------------ 
    def startRouterShell(self, argv, iceInstance):
        print 'Router Shell Working...'

        aQuery = ArgumentQuery(argv)
        optionList = aQuery.getOptionString()

        if self.__argumentManager.onlyRouterProxiesArgument(optionList):
            proxies = self.getRouterProxies(iceInstance)
            self.printRouterProxies(proxies, argv['routerProxy'])

        elif self.__argumentManager.addNewRouterActionOption(optionList):
            print 'Sending new action to router...'
            proxies = self.getRouterProxies(iceInstance)
            if int(argv['routerProxy']) + 1 <= len(proxies):
                self.createNewAction(argv)
                self.sendActionsToRouter(proxies[argv['routerProxy']])
            else:
                print 'That router doesn\'t exist'
            print 'Done!'

        elif self.__argumentManager.deleteAppliedActionsOption(optionList):
            print 'Deleting all sent actions...'
            proxies = self.getRouterProxies(iceInstance)
            if int(argv['routerProxy']) + 1 <= len(proxies):
                self.deleteActions(proxies[argv['routerProxy']])
            else:
                print 'That router doesn\'t exist'
            print 'Done!'

        elif self.__argumentManager.sentActionsOption(optionList):
            print 'Getting all sent actions from router...'
            proxies = self.getRouterProxies(iceInstance)
            if int(argv['routerProxy']) + 1 <= len(proxies):
                print 'Sent actions: ', self.getSentActions(proxies[argv['routerProxy']])
            else:
                print 'That router doesn\'t exist'
            print 'Done!'

        elif self.__argumentManager.getRouterNameOption(optionList):
            print 'Getting router name...'
            proxies = self.getRouterProxies(iceInstance)
            if int(argv['routerProxy']) + 1 <= len(proxies):
                name = self.getRouterName(proxies[argv['routerProxy']])
                print 'Router name: ', name
            else:
                print 'That router doesn\'t exist'
            print 'Done!'

        elif self.__argumentManager.pingRouterOption(optionList):
            print 'Sending Ping to router...'
            proxies = self.getRouterProxies(iceInstance)
            if int(argv['routerProxy']) + 1 <= len(proxies):
                self.pingRouter(proxies[argv['routerProxy']])
            else:
                print 'That router doesn\'t exist'
            print 'Done!'

        elif self.__argumentManager.getRouterIfacesOption(optionList):
            print 'Getting router ifaces from router...'
            proxies = self.getRouterProxies(iceInstance)
            if int(argv['routerProxy']) + 1 <= len(proxies):
                ifaces = self.getRouterIfaces(proxies[argv['routerProxy']])
                print 'Router ifaces: ', ifaces
            else:
                print 'That router doesn\'t exist'
            print 'Done!'

        else:
            print 'RouterShell: option incorrect'
            #self.__argumentManager.analyzeArguments(['-h'])
            #proxies = self.getRouterProxies(iceInstance)
            #self.printRouterProxies(proxies, -1)

        print 'Stopped!'
        return 0

#------------------------------------------------------------------------------ 
# router shell methods about actions
#------------------------------------------------------------------------------
    def createNewAction(self, options):
        self.__routerShell.createNewAction(options)

    def sendActionsToRouter(self, routerProxy):
        self.__routerShell.sendActions(routerProxy)

    def deleteActions(self, routerProxy):
        self.__routerShell.deleteSentActions(routerProxy)

#------------------------------------------------------------------------------ 
# route shell getting methods
#------------------------------------------------------------------------------ 
    def pingRouter(self, routerProxy):
        self.__routerShell.pingRouter(routerProxy)

    def getSentActions(self, routerProxy):
        sentActions = self.__routerShell.getSentActions(routerProxy)
        return sentActions

    def getRouterName(self, routerProxy):
        name = self.__routerShell.getRouterName(routerProxy)
        return name

    def getRouterIfaces(self, routerProxy):
        ifaces = self.__routerShell.getRouterIfaces(routerProxy)
        return ifaces

    def getRouterProxies(self, iceInstance):
        try:
            proxy = iceInstance.stringToProxy(systemProxies['icegridQuery'])
            query = IceGrid.QueryPrx.checkedCast(proxy)
            proxies = query.findAllObjectsByType('::DInDeS::RouterQuery')
            #print "proxies: ", proxies
        except Exception, e:
            #raise ArgumentParsingException('Parsing error with arguments. Error while searching router endpoints')
            #print "ERROR: %s", e
            proxies = []
        return proxies

    def printRouterProxies(self, proxyList, number):
        if number == -1:
            if len(proxyList):
                print '\nRouter Proxies:'
                counter = 0
                for p in proxyList:
                    print '\t%d.- %s' % (counter, str(p))
                    counter += 1
            else:
                print 'No routers connected!'
        elif int(number) + 1 <= len(proxyList):
            print '%s' % str(proxyList[int(number)])
        else:
            print 'That router doesn\'t exist'



#===============================================================================
# main
#===============================================================================
if __name__ == "__main__":
    rShell = RouterShellMain()
    if len(sys.argv) == 1:
        rShell.main(sys.argv + ['-h']) 
        options = rShell.getArgumentManager().analyzeArguments(sys.argv + ['-h'])
        rShell.startRouterShell(options)
    elif len(sys.argv) > 1:
        rShell.main(sys.argv)
        options = rShell.getArgumentManager().analyzeArguments(sys.argv[1:])
        rShell.startRouterShell(options)




