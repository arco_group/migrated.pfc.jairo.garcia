#!/usr/bin/python
# -*- mode:python; coding:utf-8; tab-width:4 -*-

from dindesConf import slices, directories

import Ice
Ice.loadSlice(slices['dindesSliceLocal'])
import DInDeS

from time import asctime
from os import path



#===============================================================================
# Router Shell Model 
#===============================================================================
class RouterShellModel():
    def __init__(self):
        self.__picAndLogDir = directories['picAndLogDir']
        self.__generalLog = 'GeneralLog'
        self.__createGeneralLog()

#------------------------------------------------------------------------------ 
# class methods
#------------------------------------------------------------------------------ 
    def __createGeneralLog(self):
        if not self.__existsFile(self.__picAndLogDir + self.__generalLog + '.txt'):
            f4 = open(self.__picAndLogDir + self.__generalLog + '.txt', 'w')
            f4.close()

    def __existsFile(self, filename):
        if path.isfile(filename):
            return True
        else:
            return False

    def addInfoToLog(self, sentInfo, routerProxy):
        fileDir = self.__picAndLogDir + self.__generalLog + '.txt'
        f = open(fileDir, 'a')
        if type(sentInfo) is DInDeS.MakeAction:
            info = self.__makeActionToString(sentInfo) + ' to Router: ' + str(routerProxy)
        elif type (sentInfo) is type(''):
            info = asctime() + '.- ' + sentInfo + ' From Router: ' + str(routerProxy)
        f.write(info + '\n')
        f.close()

    def __makeActionToString(self, info):
        result = '%s.- New action sent (' % info.timestamp
        result += 'Proto:\'%s\', ' % str(info.transportProto)
        result += 'Dir:\'%s\', ' % str(info.direction)
        result += 'IP:\'%s\', ' % info.ipAddress
        result += 'Port:\'%s\', ' % str(info.transportProto)
        result += 'Type:\'%s\')' % str(info.typeAction)
        return result



