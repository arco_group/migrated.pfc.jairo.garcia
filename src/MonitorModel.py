#!/usr/bin/python
# -*- mode:python; coding:utf-8; tab-width:4 -*-

from dindesConf import directories, systemTimes
from Exceptions import CreateDatabaseException, AddDataToDatabaseException
from Exceptions import CreateDataGraphException, UnknownProbeKeyException
from Exceptions import NoProbesMonitorizedException, NoProbeSubscriptionException

import rrdtool
from os import path
from time import asctime



#===============================================================================
# Monitor Model 
#===============================================================================
class MonitorModel():
    def __init__(self):
        self.__databaseDir = directories['databaseDir']
        self.__picAndLogDir = directories['picAndLogDir']
        self.__alertsFiles = 'Alerts'
        self.__infoFiles = 'Info'
        self.__statusFiles = 'Status'
        self.__generalLog = 'GeneralLog'
        self.__alertCounter = 0
        self.__infoCounter = 0
        self.__probesMonitorized = {'':{}}  # {'Probe0':{'proxy':<probeProxy>,'probeId':<probeID>}}
        self.__probesStatus = {}


#------------------------------------------------------------------------------ 
# getters
#------------------------------------------------------------------------------ 
    def getAlertCounter(self):
        return self.__alertCounter

    def getInfoCounter(self):
        return self.__infoCounter

    def getProbesMonitorized(self):
        return self.__probesMonitorized

#------------------------------------------------------------------------------ 
# class methods
#------------------------------------------------------------------------------
# rrdtool database
#------------------------------------------------------------------------------ 
    def createDataBase(self, probesMonitorized):
        if len(probesMonitorized) == 0:
            raise NoProbeSubscriptionException('No probe subscription')
        else:
            filename = probesMonitorized.keys()[-1] #cogemos el nombre key de la última sonda guardada
            filename = filename + '.rrd'
            rdb = rrdtool.create(self.__databaseDir + filename,
                                 '--step', str(systemTimes['rrdDatabaseStep']),
                                 '--start', '0',
                                 "DS:alerts:GAUGE:700:0:100",
                                 "DS:information:GAUGE:700:0:U",
                                 "RRA:AVERAGE:0.5:1:" + str(systemTimes['rrdMaxValue']),
                                 "RRA:LAST:0.5:1:" + str(systemTimes['rrdMaxValue']),
                                 "RRA:MAX:0.5:1:" + str(systemTimes['rrdMaxValue']))
            if rdb:
                raise CreateDatabaseException('Creating database exception: %s' % rrdtool.error())
            self.__probesMonitorized = probesMonitorized #actualizamos a la última versión de las sondas que monitorizamos
            #print 'Data base created: ' + filename

#------------------------------------------------------------------------------ 
# log files
#------------------------------------------------------------------------------ 
    def createLogFiles(self):
        filename = (self.getProbesMonitorized().keys())[-1]
        if filename != '':
            f1 = open(self.__picAndLogDir + filename + self.__alertsFiles + '.txt', 'w')
            f2 = open(self.__picAndLogDir + filename + self.__infoFiles + '.txt', 'w')
            f3 = open(self.__picAndLogDir + filename + self.__statusFiles + '.txt', 'w')
            f1.close()
            f2.close()
            f3.close()
            if not self.__existsFile(self.__picAndLogDir + self.__generalLog + '.txt'):
                f4 = open(self.__picAndLogDir + self.__generalLog + '.txt', 'w')
                f4.close()
        else:
            raise NoProbesMonitorizedException('There is no probes to monitorize')

    def __existsFile(self, filename):
        if path.isfile(filename):
            return True
        else:
            return False

#------------------------------------------------------------------------------ 
# add information to log files
#------------------------------------------------------------------------------ 
    def addInformation(self, dataSent):
        self.__infoCounter = int(dataSent.data.split(' / ')[0])
        self.__alertCounter = int(dataSent.data.split(' / ')[1]) #.split()[0]
        name = self.__getProbeKeyFromInfoReceived(dataSent)
        #print 'Timeval added, name: ', name
        status = rrdtool.update(self.__databaseDir + name + '.rrd', 'N:%s:%s' % (self.getAlertCounter(), self.getInfoCounter()))
        if status:
            raise AddDataToDatabaseException('Add information to database exception: %s' % rrdtool.error)
        if dataSent.alertState:
            self.__addAlertToLog(dataSent)
        else:
            self.__addTimevalInfoToLog(dataSent)

    def __addTimevalInfoToLog(self, dataSent):
        probeName = self.__getProbeKeyFromInfoReceived(dataSent)
        fileDir = self.__picAndLogDir + probeName + self.__infoFiles + '.txt'
        f = open(fileDir, 'a')
        info = dataSent.timestamp + '.- Info from ' + str(dataSent.probeId)
        info += ' (AlertState: ' + str(dataSent.alertState) + '): ' + dataSent.data + '\n'
        f.write(info)
        f.close()

    def __addAlertToLog(self, brokenRule):
        probeName = self.__getProbeKeyFromInfoReceived(brokenRule)
        fileDir = self.__picAndLogDir + probeName + self.__alertsFiles + '.txt'
        f = open(fileDir, 'a')
        info = brokenRule.timestamp + '.- Alert from ' + str(brokenRule.probeId)
        info += ' (AlertState: ' + str(brokenRule.alertState) + '): ' + brokenRule.data + '\n'
        f.write(info)
        f.close()

#------------------------------------------------------------------------------ 
    def addInfoToGeneralLog(self, info):
        fileDir = self.__picAndLogDir + self.__generalLog + '.txt'
        f = open(fileDir, 'a')
        f.write(asctime() + '.- ' + info + '\n')
        f.close()

#------------------------------------------------------------------------------ 
    def setProbeStatus(self, res, proxy): # res = probeId o None, p = probeProxy
        status = ''
        probeName = self.__getProbeKeyFromProbeProxy(proxy)
        if res == None:
            status = 'Down'
        else:
            if res.alertState == False:
                status = 'Ok'
            elif res.alertState == True:
                status = 'Alert'
        fileDir = self.__picAndLogDir + probeName + self.__statusFiles + '.txt'
        f = open(fileDir, 'w')
        f.write(status)
        f.close()
        return self.__saveProbeStatus(probeName, status)

    def __saveProbeStatus(self, probe, status):
        if not self.__probesStatus.has_key(probe):
            self.__probesStatus.update({probe:status})
        else:
            if self.__probesStatus[probe] != status:
                self.__probesStatus[probe] = status
                msg = probe + ' has changed its state to: ' + status
                self.addInfoToGeneralLog(msg)
                return msg

#------------------------------------------------------------------------------ 
    def __getProbeKeyFromInfoReceived(self, infoReceived):
        probes = self.getProbesMonitorized()
        pos = -1
        counter = 0
        for dics in probes.itervalues():
            if dics['probeId'] == infoReceived.probeId:
                pos = counter
            counter += 1
        if pos != -1:
            return probes.keys()[pos]
        else:
            raise UnknownProbeKeyException('InfoReceived: ' + str(infoReceived) + '\nDICS: ' + str(probes))

    def __getProbeKeyFromProbeProxy(self, proxy):
        probes = self.getProbesMonitorized()
        pos = -1
        counter = 0
        for dics in probes.itervalues():
            if dics['proxy'] == proxy:
                pos = counter
            counter += 1
        if pos != -1:
            return probes.keys()[pos]
        else:
            raise UnknownProbeKeyException('There was some problems in the probe key searching')

#------------------------------------------------------------------------------ 
# creating graphs
#------------------------------------------------------------------------------ 
    def createInformationGraph(self):
        for name in self.getProbesMonitorized().iterkeys():
            status = rrdtool.graph(self.__picAndLogDir + name + self.__infoFiles + ".png",
                      "--start", "-" + str(systemTimes['rrdPeriodGraph']) + "min",
                      "--vertical-label=Timeval data",
                      "DEF:info=" + self.__databaseDir + name + ".rrd" + ":information:LAST",
                      "CDEF:realinfo=info,1,*",
                      "AREA:realinfo#398F1BFF:Data from " + name + " since " + str(systemTimes['rrdPeriodGraph']) + " minutes ago")
            if not status:
                raise CreateDataGraphException('Create information graph exception: %s' % rrdtool.error)

    def createAlertsGraph(self):
        for name in self.getProbesMonitorized().iterkeys():
            status = rrdtool.graph(self.__picAndLogDir + name + self.__alertsFiles + ".png",
                      "--start", "-" + str(systemTimes['rrdPeriodGraph']) + "min",
                      "--vertical-label=Alerts",
                      "DEF:alert=" + self.__databaseDir + name + ".rrd" + ":alerts:LAST",
                      "CDEF:realalert=alert,1,*",
                      "LINE:realalert#FF1900FF:Alerts on " + name + " since " + str(systemTimes['rrdPeriodGraph']) + " minutes ago")
            if not status:
                raise CreateDataGraphException('Create alert graph exception: %s' % rrdtool.error)



