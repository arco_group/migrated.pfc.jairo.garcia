#!/usr/bin/python
# -*- mode:python; coding:utf-8; tab-width:4 -*-

from Exceptions import ArgumentParsingException
from Exceptions import OptionParsingError, OptionParsingExit

from dindesConf import nodeConfFile

import Ice

from LibvirtManager import LibvirtManager
from TemplateManager import TemplateManager
from RouterShellMain import RouterShellMain

import subprocess, sys
from optparse import OptionParser, OptionGroup


#===============================================================================
# DindesSystem Class
#===============================================================================
class DindesSystem(Ice.Application):
    def __init__(self):
        self.__mMonitorNode = ManageMonitorNode()
        self.__mVirtualMachines = ManageVirtualMachines()
        self.__mRouterShell = ManageRouterShell()
        self.__mIcegrid = ManageIcegrid()
        self.__mDindesWeb = ManageWebServer()
        self.__ic = None

#------------------------------------------------------------------------------ 
# getters
#------------------------------------------------------------------------------
    def getMonitorNodeManager(self):
        return self.__mMonitorNode

    def getManageVMManager(self):
        return self.__mVirtualMachines

    def getRouterShellManager(self):
        return self.__mRouterShell

    def getIcegridManager(self):
        return self.__mIcegrid

    def getWebServerManager(self):
        return self.__mDindesWeb

#------------------------------------------------------------------------------ 
# run
#------------------------------------------------------------------------------
    def run(self, args):
        self.__ic = self.communicator()

        options = self.analyzeArguments(args[1:])

        if options != None:
            aQuery = ArgumentQuery(options)
            optionList = aQuery.getOptionString()

            if aQuery.isStartMonitor(optionList):
                self.__mMonitorNode.start()

            elif aQuery.isLaunchIcepatch(optionList):
                self.__mIcegrid.launchIcepatch()

            elif aQuery.isLaunchIcegrid(optionList):
                self.__mIcegrid.launchIcegridadmin()

            elif aQuery.isCreateTemplate(optionList):
                self.__mIcegrid.create()

            elif aQuery.isMVOptions(optionList):
                self.__mVirtualMachines.start(options)

            elif aQuery.isRSOptions(optionList):
                self.__mRouterShell.start(options, self.__ic)

            elif aQuery.isWebOption(optionList):
                self.__mDindesWeb.start()

            else:
                self.analyzeArguments(['-h'])

        else:
            print 'Router proxies: %s' % self.__mRouterShell.getRouterProxies(self.__ic)

        return 0


    def analyzeArguments(self, arguments):
        result = {}
        parser = _MyOptionParser("Usage: %prog [options]")

        group = OptionGroup(parser, "Manage virtual machines")
        group.add_option("-s", "--start_vm", dest="startVMName", default=[], action='append', help="start a new virtual machine", metavar="VM_NAME")
        group.add_option("-S", "--start_all", dest="startAllVM", default=False , action="store_true", help="start all virtual machines")
        group.add_option("-p", "--stop_vm", dest="stopVMName", default=[], action='append', help="stop a running virtual machine", metavar="VM_NAME")
        group.add_option("-P", "--stop_all", dest="stopAllVM", default=False, action="store_true", help="stop all virtual machines")
        group.add_option("-l", "--list_not_running", dest="listNotRunning", default=False, action="store_true", help="list all not running virtual machines")
        group.add_option("-L", "--list_running", dest="listRunning", default=False, action="store_true", help="list all running virtual machines")
        parser.add_option_group(group)

        group = OptionGroup(parser, "Manage monitor node options")
        group.add_option("-M", "--start_monitor", dest="startMonitor", action="store_true", default=False, help="start monitor node and registry")
        parser.add_option_group(group)

        group = OptionGroup(parser, "Manage Dindes web server")
        group.add_option("-R", "--start_runserver", dest="startRunserver", action="store_true", default=False, help="start Dindes web server")
        parser.add_option_group(group)

        group = OptionGroup(parser, "Manage icegrid options")
        group.add_option("-C", "--create_template", dest="createTemplate", action="store_true", default=False, help="create a template for icegridadmin")
        group.add_option("-H", "--launch_icepatch", dest="startIcepatch", action="store_true", default=False, help="launch icepatch2calc")
        group.add_option("-I", "--launch_icegrid_admin", dest="startIcegrid", action="store_true", default=False, help="deploy application and launch system with template")
        #group.add_option("-U", "--update_registry", dest="updateRegistry", action="store_true", default=False, help="update registry and deploy application again")
        parser.add_option_group(group)

        group = OptionGroup(parser, "Manage router shell options")
        group.add_option("-r", "--router_proxy", dest="routerProxy", default= -1, type='int', metavar="PROXY_NUMBER", help="select the number of a router proxy")
        group.add_option("-d", "--delete_actions", dest="delete", action="store_true", default=False, help="delete all actions sent to a router")
        group.add_option("-c", "--sent_actions", dest="sent", action="store_true", default=False, help="show all actions sent to a router")
        group.add_option("-n", "--router_name", dest="name", action="store_true", default=False, help="show router name")
        group.add_option("-g", "--ping_router", dest="ping", action="store_true", default=False, help="make a ping to a router")
        group.add_option("-i", "--router_ifaces", dest="ifaces", action="store_true", default=False, help="show all router ifaces")
        parser.add_option_group(group)

        group = OptionGroup(parser, "Manage router action to send")
        group.add_option("-t", "--transport_proto", dest="transportProto", default='', metavar='PROTO_NAME', help="select a transport protocol")
        group.add_option("-e", "--direction", dest="direction", default='', metavar='DIR_NAME', help="select a direction: dst or src")
        group.add_option("-a", "--ip_address", dest="ipAddress", default='', metavar='IP_ADDRESS', help="select an ip")
        group.add_option("-o", "--port", dest="port", default=0, type='int', metavar='PORT_NUMBER', help="select a port number")
        group.add_option("-y", "--type_action", dest="typeAction", default='', metavar='ACTION', help="select a type of action: allow, reject or drop")
        parser.add_option_group(group)

        try:
            if len(arguments) == 0:
                parser.print_help()
            else:
                (result, args) = parser.parse_args(arguments)
                return (result.__dict__)
        except OptionParsingError, e:
            raise ArgumentParsingException('Parsing error with arguments: %s' % e.msg)
        except OptionParsingExit, e:
            pass

#=============================================================================== 
# _MyOptionParser class
#===============================================================================
class _MyOptionParser(OptionParser):
    def error(self, msg):
        raise OptionParsingError(msg)

    def exit(self, status=0, msg=None):
        raise OptionParsingExit(status, msg)



#===============================================================================
# argumentsQuery
#===============================================================================
class ArgumentQuery():
    def __init__(self, args):
        self.__args = args
        self.__optionString = ['0', '0', '0', '0', '0', '0', '0']
        self.createOptionString()

    def getOptionString(self):
        res = ''
        for i in self.__optionString:
            res += i
        return res

    def createOptionString(self):
        if self.__args['startMonitor']:
            self.__optionString[0] = '1'

        if self.__args['startIcepatch']:
            self.__optionString[1] = '1'
        if self.__args['startIcegrid']:
            self.__optionString[2] = '1'
        if self.__args['createTemplate']:
            self.__optionString[3] = '1'

        if len(self.__args['startVMName']) > 0 or self.__args['startAllVM'] or len(self.__args['stopVMName']) > 0:
            self.__optionString[4] = '1'
        if self.__args['stopAllVM'] or self.__args['listNotRunning'] or self.__args['listRunning']:
            self.__optionString[4] = '1'

        if self.__args['routerProxy'] > -1 or self.__args['delete'] or self.__args['sent']:
            self.__optionString[5] = '1'
        if self.__args['name'] or self.__args['ping'] or self.__args['ifaces']:
            self.__optionString[5] = '1'
        if self.__args['transportProto'] != '' or self.__args['direction'] != '':
            self.__optionString[5] = '1'
        if self.__args['ipAddress'] != '' or self.__args['port'] > 0 or self.__args['typeAction'] != '':
            self.__optionString[5] = '1'

        if self.__args['startRunserver']:
            self.__optionString[6] = '1'


#------------------------------------------------------------------------------ 
# private query methods
#------------------------------------------------------------------------------ 
    def isStartMonitor(self, options):
        if int(options, 2) == 64:
            return True
        else:
            return False

    def isLaunchIcepatch(self, options):
        if int(options, 2) == 32:
            return True
        else:
            return False

    def isLaunchIcegrid(self, options):
        if int(options, 2) == 16:
            return True
        else:
            return False

    def isCreateTemplate(self, options):
        if int(options, 2) == 8:
            return True
        else:
            return False

    def isMVOptions(self, options):
        if int(options, 2) == 4:
            return True
        else:
            return False

    def isRSOptions(self, options):
        if int(options, 2) == 2:
            return True
        else:
            return False

    def isWebOption(self, options):
        if int(options, 2) == 1:
            return True
        else:
            return False



#===============================================================================
# Manage Monitor Node
#===============================================================================
class ManageMonitorNode():
    def __init__(self):
        pass

    def start(self):
        print 'Starting registry and monitor node...'
        startMonitor = 'icegridnode --Ice.Config=' + nodeConfFile['monitorNode']
        try:
            subprocess.call(startMonitor.split())
        except KeyboardInterrupt:
            pass
        print 'Ended!'



#===============================================================================
# Manage Virtual Machines
#===============================================================================
class ManageVirtualMachines():
    def __init__(self):
        self.__lManager = LibvirtManager()

    def getLibvirtManager(self):
        return self.__lManager

    def start(self, args):
        options = self.__selectArgsForVM(args)
        self.__lManager.run(options)

    def __selectArgsForVM(self, args):
        vmArgs = {}
        vmArgs.update({'startVMName':args['startVMName']})
        vmArgs.update({'startAllVM':args['startAllVM']})
        vmArgs.update({'stopVMName':args['stopVMName']})
        vmArgs.update({'stopAllVM':args['stopAllVM']})
        vmArgs.update({'listNotRunning':args['listNotRunning']})
        vmArgs.update({'listRunning':args['listRunning']})
        return vmArgs



#===============================================================================
# Manage Routershell
#===============================================================================
class ManageRouterShell():
    def __init__(self):
        self.__rShell = RouterShellMain()

    def getRouterShellInstance(self):
        return self.__rShell

    def start(self, args, iceInstance):
        options = self.__selectArgsForRouterShell(args)
        self.__rShell.startRouterShell(options, iceInstance)

    def __selectArgsForRouterShell(self, args):
        rsArgs = {}
        rsArgs.update({'routerProxy':args['routerProxy']})
        rsArgs.update({'delete':args['delete']})
        rsArgs.update({'sent':args['sent']})
        rsArgs.update({'name':args['name']})
        rsArgs.update({'ping':args['ping']})
        rsArgs.update({'ifaces':args['ifaces']})
        rsArgs.update({'transportProto':args['transportProto']})
        rsArgs.update({'direction':args['direction']})
        rsArgs.update({'ipAddress':args['ipAddress']})
        rsArgs.update({'port':args['port']})
        rsArgs.update({'typeAction':args['typeAction']})
        return rsArgs

    def getRouterProxies(self, iceInstance):
        return self.__rShell.getRouterProxies(iceInstance)



#===============================================================================
# Icegrid
#===============================================================================
class ManageIcegrid():
    def __init__(self):
        self.__tManager = TemplateManager()

    def create(self):
        print 'Creating DindesTemplate.xml...'
        self.__tManager.createTemplate()
        print 'Done!'

    def launchIcepatch(self):
        print 'Starting icepatch2calc...'
        startIcepatch = 'icepatch2calc .'
        try:
            subprocess.call(startIcepatch.split())
        except KeyboardInterrupt:
            pass
        print 'Done!'

    def launchIcegridadmin(self):
        print 'Starting icegridadmin...'
        iceCfgFile = nodeConfFile['monitorNode']
        templateFile = nodeConfFile['template']
        try:
            subprocess.call(["icegridadmin", "--Ice.Config=" + iceCfgFile, "-e", "application add '" + templateFile + "'"])
        except KeyboardInterrupt:
            pass
        print 'Done!'



#===============================================================================
# webserver
#===============================================================================
class ManageWebServer():
    def __init__(self):
        pass

    def start(self):
        print 'Starting web server...'
        runServer = 'python ./web/manage.py runserver'
        try:
            subprocess.call(runServer.split())
        except KeyboardInterrupt:
            pass
        print 'Ended!'



#===============================================================================
# DindesScript Main
#===============================================================================
if __name__ == '__main__':
    dindes = DindesSystem()
    dindes.main(sys.argv, nodeConfFile['locator'])



