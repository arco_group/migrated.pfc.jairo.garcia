#!/usr/bin/python
# -*- mode:python; coding:utf-8; tab-width:4 -*-

from dindesConf import slices, systemTimes
from Exceptions import NoProbeSubscriptionException, UnsubscribeMonitorException

import Ice
Ice.loadSlice(slices['dindesSliceLocal'])
import DInDeS

from threading import Thread, Event

from MonitorFacade import MonitorFacade



#===============================================================================
# MonitorView class
#===============================================================================
class Monitor(DInDeS.Monitor):
    def __init__(self):
        self.__mFacade = MonitorFacade()
        self.__notSubscribedProbes = []
        self.__tStopPing = None
        self.__tStopSubscribe = None

#------------------------------------------------------------------------------ 
# getters
#------------------------------------------------------------------------------
    def getMonitorFacade(self):
        return self.__mFacade

#------------------------------------------------------------------------------ 
# alert and timevalInformation methods
#------------------------------------------------------------------------------ 
    def alert(self, brokenRule, current=None):
        print 'Alert from %s (%s): %s' % (brokenRule.methodName, brokenRule.probeId, brokenRule.data)
        self.__mFacade.addAlert(brokenRule)
        self.__mFacade.createGraphs()

    def timevalInformation(self, dataSended, current=None):
        print 'Info received from %s (%s): %s' % (dataSended.methodName, dataSended.probeId, dataSended.data)
        self.__mFacade.addInformation(dataSended)
        self.__mFacade.createGraphs()

#------------------------------------------------------------------------------ 
# monitor controller class methods
#------------------------------------------------------------------------------
    def configureMonitor(self, proxiesArgs, iceInstance):
        for p in proxiesArgs:
            error = False

            try: #nos suscribimos a ella
                self.__mFacade.subscribeMonitorMethodsOnProbe(self, p, iceInstance)
            except Exception, e:
                print 'Error while trying to subscribe to a probe (%s)' % p
                error = True
                self.__notSubscribedProbes.append(p)
                #return 1

            if not error:
                try: #creamos una nueva base de datos
                    self.__mFacade.createDataBase()
                except NoProbeSubscriptionException, e:
                    print 'NoProbeSubscriptionException: ', e
                    return 1

                try: #creamos los ficheros de log
                    self.__mFacade.createLogFiles()
                except Exception, e:
                    print 'Error creating log files. %s' % e
                    return 1

                try: #creamos los gráficos
                    self.__mFacade.createGraphs()
                except Exception, e:
                    print 'Error creating graphs. %s' % e
                    return 1

        self.startPingProbes(iceInstance)
        return 0

    def startPingProbes(self, iceInstance):
        self.__tStop = Event()
        t = Thread(target=self.__pingProbes)
        t.start()
        iceInstance.waitForShutdown()
        self.__tStop.set()

    def __pingProbes(self):
        while(not self.__tStop.is_set()):
            self.__mFacade.pingProbes()
            if self.__notSubscribedProbes:
                self.__mFacade.tryToSubscribe(self, self.__notSubscribedProbes)
            self.__tStop.wait(systemTimes['pingProbesTimeout'])

    def stopMonitor(self):
        try: #nos desuscribimos de todas las sondas que monitorizamos
            self.__mFacade.unsubscribeMonitorMethodsOnProbe()
        except UnsubscribeMonitorException, e:
            print 'UnsubscribeMonitorException: ', e




