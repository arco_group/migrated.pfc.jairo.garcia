#!/usr/bin/python
# -*- mode:python; coding:utf-8; tab-width:4 -*-

from Exceptions import NoInputArgumentsIntroducedException, ArgumentParsingException
from Exceptions import ArgumentParsingExitException
from Exceptions import OptionParsingError, OptionParsingExit

import Ice

import sys
from optparse import OptionParser

from Router import Router

#===============================================================================
# ArgumentManager
#===============================================================================
class ArgumentManager():
    def analyzeArguments(self, args):
        if len(args) > 0:
            arguments = self.__parseArguments(args)['routerName']
            return arguments
        else:
            raise NoInputArgumentsIntroducedException('There are no typed arguments')

    def __parseArguments(self, arguments):
        result = {}
        parser = _MyOptionParser()
        parser.add_option("-n", "--name" , dest="routerName" , default='' , help="Choose a name", metavar="ROUTER_NAME")
        try:
            (result, args) = parser.parse_args(arguments)
            return (result.__dict__)
        except OptionParsingError, e:
            raise ArgumentParsingException('Parsing error with arguments: %s' % e.msg)
        except OptionParsingExit, e:
            raise ArgumentParsingExitException('Parser exited with result code %s' % e.status)



#=============================================================================== 
# _MyOptionParser class
#===============================================================================
class _MyOptionParser(OptionParser):
    def error(self, msg):
        raise OptionParsingError(msg)

    def exit(self, status=0, msg=None):
        raise OptionParsingExit(status, msg)



#===============================================================================
# RouterMain class
#===============================================================================
class RouterMain(Ice.Application):
    def __init__(self):
        self.__aManager = ArgumentManager()

    def run(self, argv):
        self.shutdownOnInterrupt()
        ic = self.communicator()
        try:
            routername = self.__aManager.analyzeArguments(argv[1:])
        except NoInputArgumentsIntroducedException, e:
            print 'RouterMain: ', e
            return 0

        router = Router(routername)
        try:
            router.configureRouter(ic)
        except Exception, e:
            print 'MainError: %s' % e
            return 0
        print 'Working...'
        print 'Press Ctrl+C for stopping.'
        ic.waitForShutdown()
        print 'Stopped!'
        return 0

#===============================================================================
# main
#===============================================================================
if __name__ == "__main__":
    RouterMain().main(sys.argv)



