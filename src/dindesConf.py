#!/usr/bin/python
# -*- mode:python; coding:utf-8; tab-width:4 -*-


#===============================================================================
# Especificamos si queremos depurar el código mediante eclipse o no. En función
# de ésto, configuraremos la ruta de las demás variables.
#===============================================================================
debugEclipse = False

icegridMode = True

mailReady = True

#===============================================================================
# Configuración de los datos del administrador del sistema
#===============================================================================
adminInfo = {'name': 'Jairo García',
             'mail': 'garcianieves.jairo@gmail.com',
             }

#===============================================================================
# Configuración del correo desde donde se mandarán los emails indicando el estado
# de alerta
#===============================================================================
mailConf = {'mail' : 'dindesnotificationsystem@gmail.com',
            'pass' : 'dindespfc',
            'server' : 'smtp.gmail.com',
            'port': 587,
            'ready' : mailReady,
            }

#===============================================================================
# Configuración de los diferentes tiempos del sistema
#===============================================================================
systemTimes = {'rrdDatabaseStep' : 60, # segs, cada cuánto se almacenan datos 
               'rrdPeriodGraph' : 30, # mins, desde cuándo se muestra los datos 
               'rrdMaxValue' : 4400, #elements, valor máximo que se almacena
               'pingProbesTimeout' : 10, # segs, cada cuánto se manda un ping
               'pluginStopperTimeout' : 1, # segs, cada cuánto se comprueba si se para la sonda
               'stateAlertTimeout' : 120, # segs, tiempo durante en el que se indica el estado de alerta
               'monitorWaitingTime' : 15, # segs, tiempo que espera el monitor para que las sondas 
                                         # estén listas para recibir peticiones de subscripción
               }

#===============================================================================
# Configuración de la política de seguridad en las reglas en los routers (uso de iptables)
#===============================================================================
iptablesReady = icegridMode
policies = {'passive' : 'iptables -P FORWARD DROP',
            'permissive' : 'iptables -P FORWARD ACCEPT',
            }

ipTablesConf = {'ready' : iptablesReady,
                'policy' : policies['permissive'],
                }

#===============================================================================
# Configuración de la plantilla de IceGrid. Tipo de elementos: Sonda
#===============================================================================
probes = [{'id':'0', 'activation':'always', 'node':'WorkStationRex', 'plugin':'sniffNetwork', 'iface':'eth0', 'timestamp':'10', 'rule':'icmp', 'promisc':'False'},
          {'id':'1', 'activation':'always', 'node':'WebServerWolf', 'plugin':'packetsReceived', 'iface':'eth0', 'timestamp':'10', 'rule':'9999999', 'promisc':'False'},
          #{'id':'2', 'activation':'manual' , 'node':'MailServerMantis', 'plugin':'sniffNetwork', 'iface':'eth0', 'timestamp':'10', 'rule':'tcp and port 80', 'promisc':'False'},
          ]

#===============================================================================
# Configuración de la plantilla de IceGrid. Tipo de elementos: Routers.
#===============================================================================
routers = [{'id':'0', 'activation':'always', 'node':'RouterOcelot'},
           {'id':'1', 'activation':'always', 'node':'RouterRaven'},
           ]

#===============================================================================
# Configuración de la plantilla de IceGrid. Tipo de elemento: Monitor.
#===============================================================================
monitor = {'activation':'always', 'node':'Monitor',
           }

#===============================================================================
# Configuración de los path de los diferentes archivos, así como de los proxies
# dependiendo de si queremos ejecutar o depurar. 
#===============================================================================
filesDeployedDir = ''
ipIcegridQuery = '127.0.0.1'

if debugEclipse:
    path = '../'
else:
    path = './'
    if icegridMode:
        filesDeployedDir = 'db/distrib/Dindes/'
        ipIcegridQuery = '192.168.122.1'

#===============================================================================
# Configuración de los diferentes proxys utilizados en el sistema
#===============================================================================
systemProxies = {'topicManager' : 'Dindes.IceStorm/TopicManager:default -p 10001',
                 'icegridQuery' : 'IceGrid/Query:default -h ' + ipIcegridQuery + ' -p 20001',
                 }

topicInfo = {'topicManagerName' : 'ProbeTopicManager',
             }

#===============================================================================
# Configuración de los diferentes adaptadores del sistema
#===============================================================================
systemAdapters = {'subscriptionEndpoint': 'subscribe',
                  'monitorAdapter' : 'monitor',
                  'routerEndpoint' : 'manageRouter',
                  }

#===============================================================================
# Configuración del fichero Slice en el se especifica las interfaces de cada entidad
#===============================================================================
slices = {'dindesSliceDeployed' : path + filesDeployedDir + 'slices/dindes.ice',
          'dindesSliceLocal' : path + 'slices/dindes.ice',
          }

#===============================================================================
# Configuración de los directorios en donde se guardan las bases de datos y los 
# ficheros generados
#===============================================================================
directories = {'databaseDir' : path + 'rrdtoolDatabase/',
               'picAndLogDir' : path + 'web/web/static/info/',
               }

#===============================================================================
# Localización de los ficheros de configuración de los nodos IceGrid
#===============================================================================
nodeConfFile = {'monitorNode': path + 'icegridConf/monitor.cfg',
               'locator': path + 'icegridConf/locator.cfg',
               'template':path + 'icegridConf/DindesTemplate.xml',
               }

#===============================================================================
# Configuración de los ficheros en donde tomaremos la información de datos recibidos
#===============================================================================
networkFiles = {'devFile' : '/proc/net/dev',
                'netstatFile' : '/proc/net/netstat',
                }

#=============================================================================== 
# Configuración de los ficheros fake con datos previamente establecidos
#===============================================================================
testsFiles = {'devFile' : '../tests/auxFiles/auxNetDev.txt',
              'netstatFile' : '../tests/auxFiles/auxNetNetstat.txt',
              }



