#!/usr/bin/python
# -*- mode:python; coding:utf-8; tab-width:4 -*-

from dindesConf import slices, systemTimes
from Exceptions import ErrorCreatingRuleException, RuleAlreadyExistsException

import Ice
Ice.loadSlice(slices['dindesSliceDeployed'])
import DInDeS

from scapy.all import sniff, conf
from pcapy import open_live, PcapError
from time import time, asctime
from threading import Thread, Event



#===============================================================================
# Sniffing Network Plugin
#===============================================================================
class SniffingNetwork(object):
    def __init__(self, alertMethod, notifMethod):
        self.__alertMethod = alertMethod
        self.__notifMethod = notifMethod
        self.__sReportMethods = SniffingReportMethod()
        self.__sPacketAnalysis = SniffingPacketAnalysis()
        self.__probeId = ''
        self.__methodLoaded = None
        self.__iface = ''
        self.__timeval = -1
        self.__rules = []
        self.__promisc = False
        self.__nextNotification = 0.0
        self.__tStop = Event()
        self.__nAlertPackets = 0
        self.__nDataPackets = 0
        self.__lastAlertStateTime = 0
        self.__inAlertState = False

#------------------------------------------------------------------------------ 
# getters
#------------------------------------------------------------------------------
    def getProbeId(self):
        return self.__probeId

    def getMethodLoaded(self):
        return self.__methodLoaded

    def getNetworkIface(self):
        return self.__iface

    def getTimeval(self):
        return self.__timeval

    def getAlertRules(self):
        return self.__rules

    def getPromiscMode(self):
        return self.__promisc

    def getNextNotification(self):
        return self.__nextNotification

    def getNumberAlerts(self):
        return self.__nAlertPackets

    def getNumberData(self):
        return self.__nDataPackets

    def getIsSyntaxCorrect(self, info):
        return self.__isSyntaxCorrect(info)
#------------------------------------------------------------------------------
    def loadMethod(self, methodName):
        self.__loadMethod(methodName)

    def notifyPacket(self, packet):
        self.__notifyPacket(packet)

    def getCheckInformation(self):
        return self.__checkInformation()

#------------------------------------------------------------------------------ 
    def getSniffingReportMethods(self):
        return self.__sReportMethods

#------------------------------------------------------------------------------ 
# setters
#------------------------------------------------------------------------------ 
    def setPromiscMode(self, mode):
        self.__promisc = mode

    def setNumberAlerts(self, nPackets):
        self.__nAlertPackets = nPackets

    def setNumberData(self, nData):
        self.__nDataPackets = nData

#------------------------------------------------------------------------------ 
# class methods
#------------------------------------------------------------------------------
    def configure(self, probeName='', plugin='', iface='lo', timeval=1, rule=[], promisc=False, iceInstance=None, topicProxy=None):
        self.__setProbeName(probeName)
        self.__loadMethod(plugin)
        self.__setNetworkIface(iface)
        self.__setTimeval(timeval)
        self.__addAlertRule(rule)
        self.__setPromiscMode(promisc)
        self.__ice = iceInstance

    def startToRun(self):
        self.__tStop = Event()
        t = Thread(target=self.__execute)
        t.start()
        self.__ice.waitForShutdown()
        self.stopRunning()

    def stopRunning(self):
        self.__tStop.set()

    def __execute(self):
        self.__methodLoaded()

#------------------------------------------------------------------------------ 
# private configure methods
#------------------------------------------------------------------------------
    def __setProbeName(self, probeName):
        self.__probeId = probeName

    def __loadMethod(self, methodName):
        self.__methodLoaded = getattr(self, "_" + methodName)

    def __setNetworkIface(self, iface):
        self.__iface = iface

    def __setTimeval(self, timeval):
        self.__timeval = timeval

    def __addAlertRule(self, inputRules):
        for r in inputRules:
            if self.__ruleAlreadyExists(r):
                raise RuleAlreadyExistsException('This rule already exists: ' + r)
            elif self.__isSyntaxCorrect(r):
                newRule = self.__sReportMethods.getAlertRuleFromString(r, str(self.__methodLoaded.func_name).strip('_'), self.__probeId)
                self.__rules.append(newRule)
            else:
                raise ErrorCreatingRuleException('Error with this rule: ' + r)

    def __setPromiscMode(self, mode):
        promisc = 0
        if str(mode) == 'True':
            promisc = 1
        self.__promisc = mode
        conf.sniff_promisc = promisc

#------------------------------------------------------------------------------ 
    def __ruleAlreadyExists(self, inputRule):
        for rule in self.getAlertRules():
            if rule.data == inputRule:
                return True
            else:
                return False

    def __isSyntaxCorrect(self, inputRule):
        result = ''
        p = open_live('lo', 200, 0, 100)
        try:
            p.setfilter(inputRule)
            result = True
        except PcapError:
            result = False
        return result

#------------------------------------------------------------------------------ 
# private execute methods
#------------------------------------------------------------------------------
    def _sniffNetwork(self):
        self.__nextNotification = time() + self.getTimeval()
        # /usr/share/pyshared/scapy/sendrecv.py
        sniff(iface=self.getNetworkIface(), count=0, prn=lambda x: self.__notifyPacket(x),
              stopperTimeout=systemTimes['pluginStopperTimeout'], stopper=self.__checkInformation)

    def __checkInformation(self):
        if self.getNextNotification() < time():
            ratio = "%d / %d" % (self.getNumberData(), self.getNumberAlerts())
            methodName = str(self.__methodLoaded.func_name).strip('_')
            report = self.__sReportMethods.createReport(ratio, self.__inAlertState, methodName , self.__probeId)
            self.__notifMethod(report)
            self.__nextNotification += self.getTimeval()

        if self.__lastAlertStateTime + systemTimes['stateAlertTimeout'] < time() and self.__inAlertState:
                self.__inAlertState = False

        return self.__tStop.is_set()

    def __notifyPacket(self, newPacket):
        for r in self.getAlertRules():
            if self.__sPacketAnalysis.isForbidden(r, newPacket):
                print 'Alert!: ', newPacket.summary()
                self.setNumberAlerts(self.getNumberAlerts() + 1)

                if not self.__inAlertState:
                    self.__lastAlertStateTime = time()
                    self.__inAlertState = True
                    brokenRule = self.__sReportMethods.createBrokenRuleReport(r, newPacket.summary())
                    self.__alertMethod(brokenRule)

        self.setNumberData(self.getNumberData() + 1)



#===============================================================================
# sniffing report method
#===============================================================================
class SniffingReportMethod():
    def __init__(self):
        pass

#------------------------------------------------------------------------------ 
# report methods
#------------------------------------------------------------------------------
    def getAlertRuleFromString(self, inputRule, methodLoaded, probeId):
        #print 'INPUT RULE: ', inputRule
        a = DInDeS.AlertRule()
        a.methodName = self.__getMethodNameFromString(methodLoaded)

        a.networkProto = self.__getNetworkProtoFromString(inputRule)
        a.transportProto = self.__getTransportProtoFromString(inputRule)
        a.direction = self.__getPacketDirectionFromString(inputRule)
        a.ipAddress = self.__getIpAddressFromString(inputRule)
        a.port = self.__getPortFromString(inputRule)

        a.data = inputRule
        a.timestamp = '' #se completa cuando se rompe la regla
        a.probeId = probeId

        return a

    def createBrokenRuleReport(self, alertRule, packetSummary):
        a = DInDeS.AlertRule()
        a.methodName = alertRule.methodName
        a.networkProto = alertRule.networkProto
        a.transportProto = alertRule.transportProto
        a.direction = alertRule.direction
        a.ipAddress = alertRule.ipAddress
        a.port = alertRule.port

        a.data = alertRule.data + ' (' + packetSummary + ')'
        a.timestamp = str(asctime())
        a.probeId = alertRule.probeId
        return a

    def createReport(self, ratio, alertState, methodLoaded, probeId):
        r = DInDeS.TimevalData()
        r.methodName = self.__getMethodNameFromString(methodLoaded) #'sniffNetwork'
        r.data = ratio
        r.alertState = alertState
        r.timestamp = str(asctime())
        r.probeId = probeId
        return r

#------------------------------------------------------------------------------ 
    def __getMethodNameFromString(self, methodName):
        s = DInDeS.MethodLoaded
        methods = [s.bytesTransmitted, s.bytesReceived, s.packetsTransmitted, s.packetsReceived,
                   s.errorsTransmitted, s.errorsReceived, s.multicastTransmitted, s.multicastReceived,
                   s.broadcastTransmitted, s.broadcastReceived, s.sniffNetwork]
        for i in methods:
            if str(i) == methodName:
                return i
        return s.noMethodLoaded

    def __getNetworkProtoFromString(self, inputRule):
        s = DInDeS.NetworkProtocol
        networkProtos = [s.arp, s.ip, s.ip6]
        for data in inputRule.split():
            if data in str(networkProtos):
                for proto in networkProtos:
                    if str(proto) == data:
                        return proto
        return s.noNetworkProtocol

    def __getTransportProtoFromString(self, inputRule):
        s = DInDeS.TransportProtocol
        transportProtos = [s.icmp, s.tcp, s.udp]
        for data in inputRule.split():
            if data in str(transportProtos):
                for proto in transportProtos:
                    if str(proto) == data:
                        return proto
        return s.noTransportProtocol

    def __getPacketDirectionFromString(self, inputRule):
        s = DInDeS.PacketDirection
        directions = [s.src, s.dst]
        for data in inputRule.split():
            if data in str(directions):
                for direction in directions:
                    if str(direction) == data:
                        return direction
        return s.noPacketDirection

    def __getIpAddressFromString(self, inputRule):
        isIP = False
        for data in inputRule.split():
            if isIP:
                return data
                isIP = False
            if data == 'host' or data == 'net':
                isIP = True
        return ''

    def __getPortFromString(self, inputRule):
        isPortNumber = False
        for data in inputRule.split():
            if isPortNumber:
                return int(data)
                isPortNumber = False
            if data == 'port':
                isPortNumber = True
        return 0



#===============================================================================
# sniffing packet analysis
#===============================================================================
class SniffingPacketAnalysis():
    def __init__(self):
        pass

#------------------------------------------------------------------------------ 
# packet analysis
#------------------------------------------------------------------------------ 
    def isForbidden(self, rule, packet):
        fields = self.__getFieldsToCheck(rule)

        self.__checkNetworkProtoField(fields, rule, packet)
        self.__checkIpAddressField(fields, rule, packet)
        self.__checkTransportProtoField(fields, rule, packet)
        self.__checkPortField(fields, rule, packet)

        for res in fields.values():
            if res == '':       #si hay algún campo que no está en alerta
                return False
        return True             #si los campos que pueden estar en alerta lo están

#------------------------------------------------------------------------------ 
    def __getFieldsToCheck(self, rule):
        fields = {'networkProto':'', 'ipAddress':'', 'transportProto':'', 'port':''}
        #vemos los campos que no tenemos que tener en cuenta porque no lo especificamos en el AlertRule
        if rule.networkProto == DInDeS.NetworkProtocol.noNetworkProtocol:
            fields['networkProto'] = None
        if rule.ipAddress == '':
            fields['ipAddress'] = None
        if rule.transportProto == DInDeS.TransportProtocol.noTransportProtocol:
            fields['transportProto'] = None
        if rule.port == 0:
            fields['port'] = None

        return fields

    def __checkNetworkProtoField(self, fields, rule, packet):
        #comprobamos si es ipv6
        if rule.networkProto != DInDeS.NetworkProtocol.noNetworkProtocol and \
        str(rule.networkProto) == 'ip6' and \
        packet.sprintf("%version%") == '6':
            fields['networkProto'] = 'alert'

    def __checkIpAddressField(self, fields, rule, packet):
        if rule.direction == DInDeS.PacketDirection.noPacketDirection and \
        (rule.ipAddress == packet.sprintf("%IP.src%") or rule.ipAddress == packet.sprintf("%IP.dst%")):
            fields['ipAddress'] = 'alert'
        if rule.direction == DInDeS.PacketDirection.src and rule.ipAddress == packet.sprintf("%IP.src%"):
            fields['ipAddress'] = 'alert'
        if rule.direction == DInDeS.PacketDirection.dst and rule.ipAddress == packet.sprintf("%IP.dst%"):
            fields['ipAddress'] = 'alert'

    def __checkTransportProtoField(self, fields, rule, packet):
        if rule.transportProto != DInDeS.TransportProtocol.noTransportProtocol and \
        str(rule.transportProto) == packet.sprintf("%IP.proto%"):
            fields['transportProto'] = 'alert'

    def __checkPortField(self, fields, rule, packet):
        #si no hemos indicado en la AlertRule el puerto, no comprobamos nada
        if fields['port'] != None:
            if rule.direction == DInDeS.PacketDirection.noPacketDirection  and \
            (str(rule.port) == packet.sprintf("%IP.sport%") or str(rule.port) == packet.sprintf("%IP.dport%")):
                fields['port'] = 'alert'
            if rule.direction == DInDeS.PacketDirection.src and str(rule.port) == packet.sprintf("%IP.sport%"):
                fields['port'] = 'alert'
            if rule.direction == DInDeS.PacketDirection.dst and str(rule.port) == packet.sprintf("%IP.dport%"):
                fields['port'] = 'alert'



