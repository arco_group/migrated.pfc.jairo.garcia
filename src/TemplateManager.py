#!/usr/bin/python
# -*- mode:python; coding:utf-8; tab-width:4 -*-

import xml.etree.cElementTree as ET
from dindesConf import probes, routers, monitor

from os import getcwd


#===============================================================================
# Template manager
#===============================================================================
class TemplateManager():
    def __init__(self):
        self.file = None

    def createTemplate(self):
        root = ET.Element("icegrid")

        application = ET.SubElement(root, "application")
        application.set("name", "Dindes")

        distrib = ET.SubElement(application, "distrib")
        distrib.set("icepatch", "${application}.IcePatch2/server")

        self.addServiceTemplate(application)
        self.addIcepatchTemplate(application)
        self.addIcestormTemplate(application)

        self.addMonitorTemplate(application)
        self.addProbeTemplate(application)
        self.addRouterTemplate(application)

        self.addMonitorNode(application, monitor)
        self.addRouterNode(application, routers)
        self.addProbeNode(application, probes)


        tree = ET.ElementTree(root)
        tree.write("./icegridConf/DindesTemplate.xml")



#------------------------------------------------------------------------------ 
# class methods
#------------------------------------------------------------------------------ 
    def addServiceTemplate(self, app):
        serviceTemp = ET.SubElement(app, "service-template")
        serviceTemp.set("id", "IceStorm")
        parameter1 = ET.SubElement(serviceTemp, "parameter")
        parameter1.set("name", "instance-name")
        parameter1.set("default", "${application}.IceStorm")
        parameter2 = ET.SubElement(serviceTemp, "parameter")
        parameter2.set("name", "topic-manager-endpoints")
        parameter2.set("default", "default")
        parameter3 = ET.SubElement(serviceTemp, "parameter")
        parameter3.set("name", "publish-endpoints")
        parameter3.set("default", "default")
        parameter4 = ET.SubElement(serviceTemp, "parameter")
        parameter4.set("name", "flush-timeout")
        parameter4.set("default", "1000")

        service = ET.SubElement(serviceTemp, "service")
        service.set("name", "${instance-name}")
        service.set("entry", "IceStormService,33:createIceStorm")

        properties = ET.SubElement(service, "properties")
        property1 = ET.SubElement(properties, "property")
        property1.set("name", "${service}.InstanceName")
        property1.set("value", "${instance-name}")
        property2 = ET.SubElement(properties, "property")
        property2.set("name", "${service}.Flush.Timeout")
        property2.set("value", "${flush-timeout}")

        adapter1 = ET.SubElement(service, "adapter")
        adapter1.set("name", "${service}.Publish")
        adapter1.set("endpoints", "${publish-endpoints}")
        adapter1.set("id", "${instance-name}.Publish")
        adapter2 = ET.SubElement(service, "adapter")
        adapter2.set("name", "${service}.TopicManager")
        adapter2.set("endpoints", "${topic-manager-endpoints}")
        adapter2.set("id", "${instance-name}.TopicManager")
        obj = ET.SubElement(adapter2, "object")
        obj.set("identity", "${instance-name}/TopicManager")
        obj.set("type", "::IceStorm::TopicManager")

        dbenv = ET.SubElement(service, "dbenv")
        dbenv.set("name", "${service}")

    def addIcepatchTemplate(self, app):
        serverTemp = ET.SubElement(app, "server-template")
        serverTemp.set("id", "IcePatch2")

        parameter1 = ET.SubElement(serverTemp, "parameter")
        parameter1.set("name", "instance-name")
        parameter1.set("default", "${application}.IcePatch2")
        parameter2 = ET.SubElement(serverTemp, "parameter")
        parameter2.set("name", "endpoints")
        parameter2.set("default", "default")
        parameter3 = ET.SubElement(serverTemp, "parameter")
        parameter3.set("name", "directory")

        server = ET.SubElement(serverTemp, "server")
        server.set("id", "${instance-name}")
        server.set("activation", "on-demand")
        server.set("application-distrib", "false")
        server.set("exe", "icepatch2server")

        properties = ET.SubElement(server, "properties")
        property1 = ET.SubElement(properties, "property")
        property1.set("name", "IcePatch2.InstanceName")
        property1.set("value", "${instance-name}")
        property2 = ET.SubElement(properties, "property")
        property2.set("name", "IcePatch2.Directory")
        property2.set("value", "${directory}")

        adapter = ET.SubElement(server, "adapter")
        adapter.set("name", "IcePatch2")
        adapter.set("endpoints", "${endpoints}")
        adapter.set("id", "${server}.IcePatch2")
        obj = ET.SubElement(adapter, "object")
        obj.set("identity", "${instance-name}/server")
        obj.set("type", "::IcePatch2::FileServer")

    def addIcestormTemplate(self, app):
        serverTemp = ET.SubElement(app, "server-template")
        serverTemp.set("id", "IceStorm")

        parameter1 = ET.SubElement(serverTemp, "parameter")
        parameter1.set("name", "run_as")
        parameter1.set("default", "root")
        parameter2 = ET.SubElement(serverTemp, "parameter")
        parameter2.set("name", "instance-name")
        parameter2.set("default", "${node}.IceStorm")
        parameter3 = ET.SubElement(serverTemp, "parameter")
        parameter3.set("name", "topic-manager-endpoints")
        parameter3.set("default", "default")
        parameter4 = ET.SubElement(serverTemp, "parameter")
        parameter4.set("name", "publish-endpoints")
        parameter4.set("default", "default")
        parameter5 = ET.SubElement(serverTemp, "parameter")
        parameter5.set("name", "flush-timeout")
        parameter5.set("default", "1000")

        icebox = ET.SubElement(serverTemp, "icebox")
        icebox.set("id", "${instance-name}")
        icebox.set("activation", "on-demand")
        icebox.set("exe", "icebox")

        serviceIns = ET.SubElement(icebox, "service-instance")
        serviceIns.set("template", "IceStorm")
        serviceIns.set("instance-name", "${instance-name}")
        serviceIns.set("topic-manager-endpoints", "${topic-manager-endpoints}")
        serviceIns.set("publish-endpoints", "${publish-endpoints}")
        serviceIns.set("flush-timeout", "${flush-timeout}")

    def addMonitorTemplate(self, app):
        serverTemp = ET.SubElement(app, "server-template")
        serverTemp.set("id", "MonitorTemplate")

        parameter1 = ET.SubElement(serverTemp, "parameter")
        parameter1.set("name", "exepath")
        parameter1.set("default", "${application.distrib}/MonitorMain.py")

        parameter2 = ET.SubElement(serverTemp, "parameter")
        parameter2.set("name", "activation_mode")
        parameter2.set("default", "always")

        server = ET.SubElement(serverTemp, "server")
        server.set("id", "Monitor")
        server.set("activation", "${activation_mode}")
        server.set("exe", "${exepath}")

    def addProbeTemplate(self, app):
        serverTemp = ET.SubElement(app, "server-template")
        serverTemp.set("id", "ProbeTemplate")

        parameter0 = ET.SubElement(serverTemp, "parameter")
        parameter0.set("name", "index")
        parameter1 = ET.SubElement(serverTemp, "parameter")
        parameter1.set("name", "activation_mode")
        parameter1.set("default", "always")
        parameter2 = ET.SubElement(serverTemp, "parameter")
        parameter2.set("name", "probeName")
        parameter3 = ET.SubElement(serverTemp, "parameter")
        parameter3.set("name", "run_as")
        parameter3.set("default", "root")
        parameter4 = ET.SubElement(serverTemp, "parameter")
        parameter4.set("name", "plugin")
        parameter5 = ET.SubElement(serverTemp, "parameter")
        parameter5.set("name", "iface")
        parameter6 = ET.SubElement(serverTemp, "parameter")
        parameter6.set("name", "timestamp")
        parameter6.set("default", "10")
        parameter7 = ET.SubElement(serverTemp, "parameter")
        parameter7.set("name", "alertrule")
        parameter7.set("default", "")
        parameter8 = ET.SubElement(serverTemp, "parameter")
        parameter8.set("name", "exepath")
        parameter8.set("default", "${application.distrib}/ProbeMain.py")
        parameter9 = ET.SubElement(serverTemp, "parameter")
        parameter9.set("name", "promisc")
        parameter10 = ET.SubElement(serverTemp, "parameter")
        parameter10.set("name", "iceStorm-topicProxy")
        parameter10.set("default", "${node}.IceStorm/TopicManager")

        server = ET.SubElement(serverTemp, "server")
        server.set("id", "${node}")
        server.set("activation", "${activation_mode}")
        server.set("exe", "${exepath}")
        server.set("user", "${run_as}")

        option = ET.SubElement(server, "option")
        option.text = "-n"
        option = ET.SubElement(server, "option")
        option.text = "${probeName}"
        option = ET.SubElement(server, "option")
        option.text = "-p"
        option = ET.SubElement(server, "option")
        option.text = "${plugin}"
        option = ET.SubElement(server, "option")
        option.text = "-i"
        option = ET.SubElement(server, "option")
        option.text = "${iface}"
        option = ET.SubElement(server, "option")
        option.text = "-t"
        option = ET.SubElement(server, "option")
        option.text = "${timestamp}"
        option = ET.SubElement(server, "option")
        option.text = "-r"
        option = ET.SubElement(server, "option")
        option.text = "${alertrule}"
        option = ET.SubElement(server, "option")
        option.text = "-m"
        option = ET.SubElement(server, "option")
        option.text = "${promisc}"
        option = ET.SubElement(server, "option")
        option.text = "-s"
        option = ET.SubElement(server, "option")
        option.text = "${iceStorm-topicProxy}"

        properties = ET.SubElement(server, "properties")

        adapter = ET.SubElement(server, "adapter")
        adapter.set("name", "subscribe")
        adapter.set("endpoints", "default")
        adapter.set("id", "${server}.subscribe")

        obj = ET.SubElement(adapter, "object")
        obj.set("identity", "${node}")
        obj.set("type", "::DInDeS::ProbeQuery")

    def addRouterTemplate(self, app):
        serverTemp = ET.SubElement(app, "server-template")
        serverTemp.set("id", "RouterTemplate")

        parameter0 = ET.SubElement(serverTemp, "parameter")
        parameter0.set("name", "index")
        parameter1 = ET.SubElement(serverTemp, "parameter")
        parameter1.set("name", "activation_mode")
        parameter1.set("default", "always")
        parameter2 = ET.SubElement(serverTemp, "parameter")
        parameter2.set("name", "routerName")
        parameter2.set("default", "${node}")
        parameter3 = ET.SubElement(serverTemp, "parameter")
        parameter3.set("name", "run_as")
        parameter3.set("default", "root")
        parameter4 = ET.SubElement(serverTemp, "parameter")
        parameter4.set("name", "exepath")
        parameter4.set("default", "${application.distrib}/RouterMain.py")

        server = ET.SubElement(serverTemp, "server")
        server.set("id", "${node}")
        server.set("activation", "${activation_mode}")
        server.set("exe", "${exepath}")
        server.set("user", "${run_as}")

        option = ET.SubElement(server, "option")
        option.text = "-n"
        option = ET.SubElement(server, "option")
        option.text = "${routerName}"

        properties = ET.SubElement(server, "properties")

        adapter = ET.SubElement(server, "adapter")
        adapter.set("name", "manageRouter")
        adapter.set("endpoints", "default")
        adapter.set("id", "${server}.manageRouter")

        obj = ET.SubElement(adapter, "object")
        obj.set("identity", "${node}")
        obj.set("type", "::DInDeS::RouterQuery")

    def addMonitorNode(self, app, m):
        serverTemp = ET.SubElement(app, "node")
        serverTemp.set("name", "Monitor")

        serverInst1 = ET.SubElement(serverTemp, "server-instance")
        serverInst1.set("template", "IcePatch2")
        serverInst1.set("directory", getcwd())

        serverInst2 = ET.SubElement(serverTemp, "server-instance")
        serverInst2.set("template", "MonitorTemplate")
        serverInst2.set("activation_mode", m['activation'])

    def addRouterNode(self, app, routers):
        for r in routers:
            serverTemp = ET.SubElement(app, "node")
            serverTemp.set("name", r['node'])

            serverInst = ET.SubElement(serverTemp, "server-instance")
            serverInst.set("template", "RouterTemplate")
            serverInst.set("activation_mode", r['activation'])
            serverInst.set("index", r['id'])
            serverInst.set("routerName", "${node}")
            serverInst.set("run_as", "root")

    def addProbeNode(self, app, probes):
        for p in probes:
            serverTemp = ET.SubElement(app, "node")
            serverTemp.set("name", p['node'])

            serverInst1 = ET.SubElement(serverTemp, "server-instance")
            serverInst1.set("template", "ProbeTemplate")
            serverInst1.set("activation_mode", p['activation'])
            serverInst1.set("index", p['id'])
            serverInst1.set("probeName", "${node}")
            serverInst1.set("run_as", "root")
            serverInst1.set("plugin", p['plugin'])
            serverInst1.set("iface", p['iface'])
            serverInst1.set("timestamp", p['timestamp'])
            serverInst1.set("alertrule", p['rule'])
            serverInst1.set("promisc", p['promisc'])

            serverInst2 = ET.SubElement(serverTemp, "server-instance")
            serverInst2.set("template", "IceStorm")
            serverInst2.set("run_as", "root")
