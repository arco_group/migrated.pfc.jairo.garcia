#!/usr/bin/python
# -*- mode:python; coding:utf-8; tab-width:4 -*-

from dindesConf import slices, networkFiles, systemTimes
from Exceptions import ErrorCreatingRuleException

import Ice
Ice.loadSlice(slices['dindesSliceDeployed'])
import DInDeS

from time import asctime, time
from threading import Thread, Event



#===============================================================================
# Monitoring System Plugin
#===============================================================================
class MonitoringSystem(object):
    def __init__(self, alertMethod, notifMethod):
        self.__alertMethod = alertMethod
        self.__notifMethod = notifMethod
        self.__probeId = ''
        self.__methodLoaded = None
        self.__iface = ''
        self.__timeval = -1
        self.__ruleRule = -1
        self.__file = ''
        self.__ice = None
        self.__tStop = Event()
        self.__nextNotification = 0.0
        self.__alertCounter = 0
        self.__dataCounter = 0
        self.__lastAlertStateTime = 0
        self.__inAlertState = False

#------------------------------------------------------------------------------ 
# getters
#------------------------------------------------------------------------------
    def getAlertMethod(self):
        return self.__alertMethod

    def getNotifMethod(self):
        return self.__notifMethod

    def getProbeId(self):
        return self.__probeId

    def getMethodLoaded(self):
        return self.__methodLoaded

    def getNetworkIface(self):
        return self.__iface

    def getTimeval(self):
        return self.__timeval

    def getAlertRule(self):
        return self.__ruleRule

    def getFile(self):
        return self.__file

    def getNumberAlerts(self):
        return self.__alertCounter

    def getNumberData(self):
        return self.__dataCounter

#------------------------------------------------------------------------------ 
    def loadMethod(self, methodName):
        self.__loadMethod(methodName)

#------------------------------------------------------------------------------ 
# setters
#------------------------------------------------------------------------------
    def setFile(self, fileDir):
        self.__file = fileDir

    def setNumberAlerts(self, nAlerts):
        self.__alertCounter = nAlerts

    def setNumberData(self, nData):
        self.__dataCounter = nData

#------------------------------------------------------------------------------ 
# class methods
#------------------------------------------------------------------------------
    def configure(self, probeName='', plugin='', iface='lo', timeval=1, rule=[], promisc=False, iceInstance=None, topicProxy=None):
        self.__setProbeName(probeName)
        self.__loadMethod(plugin)
        self.__setNetworkIface(iface)
        self.__setTimeval(timeval)
        self.__setAlertRule(rule)
        self.__ice = iceInstance

    def startToRun(self):
        self.__tStop = Event()
        t = Thread(target=self.__execute)
        t.start()
        self.__ice.waitForShutdown()
        self.stopRunning()

    def stopRunning(self):
        self.__tStop.set()

    def __execute(self):
        self.__nextNotification = time() + self.getTimeval()
        while(not self.__tStop.is_set()):
            result = self.__methodLoaded()
            self.setNumberData(result)

            if self.getAlertRule() != -1 and self.getNumberData() > self.getAlertRule() and not self.__inAlertState:
                self.__alertMethod(self.__createBrokenRuleReport(self.getNumberData()))
                self.__lastAlertStateTime = time()
                self.__inAlertState = True

            if self.__nextNotification < time():
                if self.getNumberData() < self.getAlertRule():
                    self.setNumberAlerts(0)
                    ratio = "%d / %d" % (self.getNumberData(), self.getAlertRule())
                else:
                    self.setNumberAlerts(self.getNumberData() - self.getAlertRule())
                    ratio = "%d / %d" % (self.getAlertRule(), self.getNumberAlerts())
                self.__notifMethod(self.__createReport(ratio))
                self.__nextNotification += self.getTimeval()

            if self.__lastAlertStateTime + systemTimes['stateAlertTimeout'] < time() and self.__inAlertState:
                self.__inAlertState = False

            self.__tStop.wait(systemTimes['pluginStopperTimeout'])

#------------------------------------------------------------------------------ 
# private configure methods
#------------------------------------------------------------------------------
    def __setProbeName(self, probeName):
        self.__probeId = probeName

    def __loadMethod(self, methodName):
        self.__methodLoaded = getattr(self, "_" + methodName)
        self.__setFileByMethodName(methodName)

    def __setNetworkIface(self, iface):
        self.__iface = iface

    def __setTimeval(self, timeval):
            self.__timeval = timeval

    def __setAlertRule(self, inputRule):
        if len(inputRule) > 0:
            if str(inputRule[0]).isdigit():
                self.__ruleRule = int(inputRule[0])
            else:
                raise ErrorCreatingRuleException('Error with this rule: ' + str(inputRule[0]))

#------------------------------------------------------------------------------
    def __setFileByMethodName(self, methodName):
        devId = ['bytesTransmitted', 'bytesReceived', 'packetsTransmitted', 'packetsReceived', 'errorsTransmitted', 'errorsReceived']
        netstatId = ['multicastTransmitted', 'multicastReceived', 'broadcastTransmitted', 'broadcastReceived']
        if methodName in devId:
            self.setFile(networkFiles['devFile'])
        elif methodName in netstatId:
            self.setFile(networkFiles['netstatFile'])

#------------------------------------------------------------------------------ 
# private report methods
#------------------------------------------------------------------------------
    def __createBrokenRuleReport(self, data):
        report = DInDeS.AlertRule()
        report.methodName = self.__getMethodNameFromString(str(self.getMethodLoaded().func_name).strip('_'))
        report.data = str(data)
        report.timestamp = str(asctime())
        report.probeId = self.getProbeId()
        return report

    def __createReport(self, data):
        report = DInDeS.TimevalData()
        report.methodName = self.__getMethodNameFromString(str(self.getMethodLoaded().func_name).strip('_'))
        report.data = data
        report.alertState = self.__inAlertState
        report.timestamp = str(asctime())
        report.probeId = self.getProbeId()
        return report

    def __getMethodNameFromString(self, methodName):
        s = DInDeS.MethodLoaded
        methods = [s.bytesTransmitted, s.bytesReceived, s.packetsTransmitted, s.packetsReceived,
                   s.errorsTransmitted, s.errorsReceived, s.multicastTransmitted, s.multicastReceived,
                   s.broadcastTransmitted, s.broadcastReceived, s.sniffNetwork]
        for i in methods:
            if str(i) == methodName:
                return i
        return s.noMethodLoaded

#------------------------------------------------------------------------------ 
# private execute methods
#------------------------------------------------------------------------------
    def _bytesTransmitted(self):
        result = -1
        for line in open(self.getFile(), 'r'): # '/proc/net/dev'
            if self.getNetworkIface() in line:
                data = line.split(' %s:' % self.getNetworkIface())[1].split()
                #print 'T_bytes %d' % int(data[8])
                result = int(data[8])
        return result

    def _bytesReceived(self):
        result = -1
        for line in open(self.getFile(), 'r'): # '/proc/net/dev'
            if self.getNetworkIface() in line:
                data = line.split(' %s:' % self.getNetworkIface())[1].split()
                #print 'R_bytes %d' % int(data[0])
                result = int(data[0])
        return result

#------------------------------------------------------------------------------ 
    def _packetsTransmitted(self):
        result = -1
        for line in open(self.getFile(), 'r'): # '/proc/net/dev'
            if self.getNetworkIface() in line:
                data = line.split(' %s:' % self.getNetworkIface())[1].split()
                #print 'T_packets %d' % int(data[9])
                result = int(data[9])
        return result

    def _packetsReceived(self):
        result = -1
        for line in open(self.getFile(), 'r'): # '/proc/net/dev'
            if self.getNetworkIface() in line:
                data = line.split(' %s:' % self.getNetworkIface())[1].split()
                #print 'R_packets %d' % int(data[1])
                result = int(data[1])
        return result

#------------------------------------------------------------------------------ 
    def _errorsTransmitted(self):
        result = -1
        for line in open(self.getFile(), 'r'): # '/proc/net/dev'
            if self.getNetworkIface() in line:
                data = line.split(' %s:' % self.getNetworkIface())[1].split()
                #print 'T_errors %d' % int(data[10])
                result = int(data[10])
        return result

    def _errorsReceived(self):
        result = -1
        for line in open(self.getFile(), 'r'): # '/proc/net/dev'
            if self.getNetworkIface() in line:
                data = line.split(' %s:' % self.getNetworkIface())[1].split()
                #print 'R_errors %d' % int(data[2])
                result = int(data[2])
        return result

#------------------------------------------------------------------------------ 
    def _multicastTransmitted(self):
        result = -1
        lineCounter = 1
        for line in open(self.getFile(), 'r'): # '/proc/net/netstat'
            if not lineCounter == 4:
                lineCounter += 1
            else:
                data = line.split('IpExt:')[1].split()
                #print 'T_multicast %d' % int(data[3])
                result = int(data[3])
        return result

    def _multicastReceived(self):
        result = -1
        lineCounter = 1
        for line in open(self.getFile(), 'r'): # '/proc/net/netstat'
            if not lineCounter == 4:
                lineCounter += 1
            else:
                data = line.split('IpExt:')[1].split()
                #print 'R_multicast %d' % int(data[2])
                result = int(data[2])
        return result

#------------------------------------------------------------------------------ 
    def _broadcastTransmitted(self):
        result = -1
        lineCounter = 1
        for line in open(self.getFile(), 'r'): # '/proc/net/netstat'
            if not lineCounter == 4:
                lineCounter += 1
            else:
                data = line.split('IpExt:')[1].split()
                #print 'T_broadcast: %d' % int(data[5])
                result = int(data[5])
        return result

    def _broadcastReceived(self):
        result = -1
        lineCounter = 1
        for line in open(self.getFile(), 'r'): # '/proc/net/netstat'
            if not lineCounter == 4:
                lineCounter += 1
            else:
                data = line.split('IpExt:')[1].split()
                #print 'R_broadcast: %d' % int(data[4])
                result = int(data[4])
        return result
