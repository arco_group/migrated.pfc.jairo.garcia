#!/usr/bin/python
# -*- mode:python; coding:utf-8; tab-width:4 -*-

from dindesConf import topicInfo

from Exceptions import ArgumentParsingException, ArgumentParsingExitException
from Exceptions import UnknownPluginMethodNameException, UnknownPluginClassNameException
from Exceptions import PluginClassNotLoadedException, NoInputArgumentsIntroducedException

from ProbeFacade import ProbeFacade



#===============================================================================
# Probe class
#===============================================================================
class Probe():
    def __init__(self):
        self.__pFacade = ProbeFacade()
        self.__probeId = None

#------------------------------------------------------------------------------ 
# getters
#------------------------------------------------------------------------------
    def getProbeFacade(self):
        return self.__pFacade

    def getProbeId(self):
        return self.__probeId

    def setProbeId(self, probeId):
        self.__probeId = probeId

#------------------------------------------------------------------------------
# probe configure class methods 
#------------------------------------------------------------------------------
    def configureProbe(self, args, iceInstance):
        try: #establecemos los argumentos iniciales de la sonda
            self.setProbeId(args['probeName'])
            self.__pFacade.setInputArguments(args, iceInstance)
        except ArgumentParsingException, e:
            print 'ArgumentParsingException: ', e
            return 1
        except ArgumentParsingExitException, e:
            print 'ArgumentParsingExitException: ', e
            return 1
        except NoInputArgumentsIntroducedException, e:
            print 'NoInputArgumentsIntroducedException: ', e
            return 1

        try: #cargamos el método seleccionado
            self.__pFacade.loadClass()
        except UnknownPluginClassNameException, e:
            print 'UnknownPluginClassNameException: ', e
            return 1
        except UnknownPluginMethodNameException, e:
            print 'UnknownPluginMethodNameException: ', e
            return 1

        try: #creamos los proxys de la Sonda
            self.__pFacade.createProxyEndpoints()
        except Exception, e:
            print 'Error while creating probe proxy. %s' % e
            return 1

        try: # creamos el topic manager en el que pondremos los datos
            self.__pFacade.createTopicManager(topicInfo['topicManagerName']) #  + datetime.now().strftime('%s%f')
        except Exception, e:
            print 'Error while creating topic manager. %s' % e
            return 1
        return 0

    def executeProbe(self):
        try: # ejecutamos el método que hemos cargado previamente
            self.__pFacade.executeMethod()
        except PluginClassNotLoadedException, e:
            print 'PluginClassHasNotBeenLoadedException: ', e
            return 1
        except Exception, e:
            print 'Error with the probe executing. %s' % e
            return 1
        return 0



