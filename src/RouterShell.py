#!/usr/bin/python
# -*- mode:python; coding:utf-8; tab-width:4 -*-

from dindesConf import slices
from Exceptions import IncorrectActionException

import Ice
Ice.loadSlice(slices['dindesSliceLocal'])
import DInDeS

from time import asctime

from RouterShellModel import RouterShellModel



#===============================================================================
# RouterShell classs
#===============================================================================
class RouterShell():
    def __init__(self):
        self.__rShellModel = RouterShellModel()
        self.__actionManager = ActionManager()
        self.__actions = []

#------------------------------------------------------------------------------ 
# getters
#------------------------------------------------------------------------------
    def getRouterShellModel(self):
        return self.__rShellModel

    def getActions(self):
        return self.__actions

#------------------------------------------------------------------------------ 
# action methods
#------------------------------------------------------------------------------ 
    def createNewAction(self, options):
        try:
            act = self.__actionManager.convertActionToMakeAction(options)
            self.addActionToSend(act)
        except IncorrectActionException:
            raise IncorrectActionException('Incorrect action to do exception')

    def addActionToSend(self, action):
        self.__actions.append(action)

    def sendActions(self, routerProxy):
        router = DInDeS.RouterQueryPrx.checkedCast(routerProxy)
        for action in self.__actions:
            router.setAction(action)
            self.__rShellModel.addInfoToLog(action, routerProxy)
        res = router.applyActions()
        if res:
            self.__rShellModel.addInfoToLog('Actions applied.', routerProxy)
        else:
            self.__rShellModel.addInfoToLog('Actions not applied.', routerProxy)
        self.__actions = []

    def deleteSentActions(self, routerProxy):
        router = DInDeS.RouterQueryPrx.checkedCast(routerProxy)
        res = router.deleteActions()
        if res:
            self.__rShellModel.addInfoToLog('Deleting actions done.', routerProxy)
        else:
            self.__rShellModel.addInfoToLog('Deleting actions don\'t done.', routerProxy)

#------------------------------------------------------------------------------ 
    def getSentActions(self, routerProxy):
        router = DInDeS.RouterQueryPrx.checkedCast(routerProxy)
        sentActions = router.getActionsConfigured()
        self.__rShellModel.addInfoToLog('Getting sent actions done.', routerProxy)
        return sentActions

    def pingRouter(self, routerProxy):
        router = DInDeS.RouterQueryPrx.checkedCast(routerProxy)
        router.ping()
        self.__rShellModel.addInfoToLog('Ping done.', routerProxy)

    def getRouterName(self, routerProxy):
        router = DInDeS.RouterQueryPrx.checkedCast(routerProxy)
        name = router.getName()
        self.__rShellModel.addInfoToLog('Getting router name done.', routerProxy)
        return name

    def getRouterIfaces(self, routerProxy):
        router = DInDeS.RouterQueryPrx.checkedCast(routerProxy)
        ifaces = router.getIfaces()
        self.__rShellModel.addInfoToLog('Getting router interfaces done.', routerProxy)
        return ifaces



#===============================================================================
# action manager
#===============================================================================
class ActionManager():
#------------------------------------------------------------------------------ 
# class methods
#------------------------------------------------------------------------------ 
    def convertActionToMakeAction(self, action):
        if self.__isCorrect(action):
            return self.__makeActionFromDict(action)
        else:
            raise IncorrectActionException('Incorrect action to do exception')

#------------------------------------------------------------------------------ 
# private methods
#------------------------------------------------------------------------------ 
    def __isCorrect(self, action):
        result = True
        if not(action['transportProto'] == 'tcp' or action['transportProto'] == 'udp' or action['transportProto'] == 'icmp' or action['transportProto'] == ''):
            result = False
        elif not (action['direction'] == 'src' or action['direction'] == 'dst' or action['direction'] == ''):
            result = False
        elif action['port'] > 65535 or action['port'] < 0:
            result = False
        elif not (action['typeAction'] == 'allow' or action['typeAction'] == 'drop' or action['typeAction'] == 'reject' or action['typeAction'] == ''):
            result = False
        return result

    def __makeActionFromDict(self, action):
        a = DInDeS.MakeAction()
        a.transportProto = self.__getActionProtocolFromString(action['transportProto'])
        a.direction = self.__getActionDirectionFromString(action['direction'])
        a.ipAddress = action['ipAddress']
        a.port = action['port']
        a.typeAction = self.__getTypeActionFromString(action['typeAction'])
        a.timestamp = asctime()
        return a

#------------------------------------------------------------------------------ 
    def __getActionProtocolFromString(self, protoString):
        s = DInDeS.TransportProtocol
        protocols = [s.tcp, s.udp, s.icmp]
        for proto in protocols:
            if str(proto) == protoString:
                return proto
        return s.noTransportProtocol

    def __getActionDirectionFromString(self, directionString):
        s = DInDeS.PacketDirection
        directions = [s.src, s.dst]
        for direction in directions:
            if str(direction) == directionString:
                return direction
        return s.noPacketDirection

    def __getTypeActionFromString(self, portString):
        s = DInDeS.Action
        ports = [s.allow, s.drop, s.reject]
        for port in ports:
            if str(port) == portString:
                return port
        return s.noAction



