#!/bin/bash

UUID=$1 
vm_name=$2
user="jairo"
pass="prueba"


echo -e "\n\t4.1.- Mounting some filesystem..."
mount -t proc none /proc
mount -t sysfs none /sys

echo -e "\n\t4.2.- Installing linux-image..."
aptitude update
aptitude install -y linux-image-2.6-486

echo -e "\n\t4.3.- Installing grub..."
aptitude install -y extlinux
extlinux-update

#echo -e "\n\t4.3.- Installing openssh-server..."
#aptitude install -y openssh-server

echo -e "\n\t4.4.- Installing zeroc-ice packages..."
aptitude install -y zeroc-ice

echo -e "\n\t4.5.- Configuring some grub params..."
sed -i -e "s#^EXTLINUX_PARAMETERS.*#EXTLINUX_PARAMETERS=\"ro quiet root=UUID=$UUID console=ttyS0\"#" /etc/default/extlinux
extlinux-update
sed -i -e "s/#\(.*ttyS0.*\)/\1/" /etc/inittab

echo -e "\n\t4.6.- Configuring filesystem..."
echo "proc       /proc proc defaults                   0 0" >  /etc/fstab
echo "UUID=$UUID /     ext2 defaults,errors=remount-ro 0 1" >> /etc/fstab

echo -e "\n\t4.7.- Configuring user params and hostname..."
echo $vm_name > /etc/hostname
useradd $user
echo "$user:$pass" | chpasswd 
echo "root:$pass" | chpasswd
#echo $pass | passwd "$user" --stdin
#passwd

echo -e "\n\t4.8.- Installing boot loader..."
extlinux --install boot/extlinux

echo -e "\n\t4.9.- Unmounting mounted filesystems..."
umount -l /proc
umount -l /sys

