ending_ip=$1
mac_vm=$2

echo "auto lo"                       > /etc/network/interfaces
echo "iface lo inet loopback"        >> /etc/network/interfaces

echo "auto eth0"                     >> /etc/network/interfaces
echo "iface eth0 inet static"        >> /etc/network/interfaces
echo "address 192.168.2.$ending_ip"  >> /etc/network/interfaces
echo "netmask 255.255.255.0"         >> /etc/network/interfaces
echo "broadcast 192.168.2.255"       >> /etc/network/interfaces
echo "network 192.168.2.0"           >> /etc/network/interfaces

echo "up route add -net 192.168.4.0 netmask 255.255.255.0 gw 192.168.2.1" >> /etc/network/interfaces

echo "SUBSYSTEM==\"net\", ACTION==\"add\", DRIVERS==\"?*\", ATTR{address}==\"$mac_vm\", ATTR{dev_id}==\"0x0\", ATTR{type}==\"1\", KERNEL==\"eth*\", NAME=\"eth0\"" > /etc/udev/rules.d/70-persistent-net.rules

/etc/init.d/networking restart
