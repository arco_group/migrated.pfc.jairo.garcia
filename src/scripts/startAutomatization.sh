#!/bin/bash

################################### VARIABLES ##################################
data_url="http://ftp.es.debian.org/debian"
debootstrap_config_file="debootstrapConfig.sh"
network1_config_file="network1Config.sh"
network2_config_file="network2Config.sh"
network_router_config_file="networkRouterConfig.sh"

hd_dir="`pwd`/hd_dir"
hd_size=1300

mount_dir="`pwd`/mnt_dir"

vm_names="vm"
number_vm_network1=1
number_vm_network2=1
ending_ip=45
mac_vm="52:54:00:0a:76"

##################################### STEPS ####################################
echo -e "\n1.- Creating hard disk for first virtual machine..."
mkdir $hd_dir
dd if=/dev/zero of=$hd_dir/$vm_names.img bs=1M count=$hd_size
/sbin/mke2fs -F $hd_dir/$vm_names.img


echo -e "\n2.- Installing required packages on system..."
aptitude install -y debootstrap virt-manager

echo -e "\n2.1.- Configuring libvirt.conf..."
sed -i "s/unix_sock_rw_perms = \"0770\"/unix_sock_rw_perms = \"0777\"/" /etc/libvirt/libvirtd.conf
/etc/init.d/libvirt-bin restart


echo -e "\n3.- Starting debootstrap for getting data from $data_url..."
mkdir $mount_dir
mount $hd_dir/$vm_names.img $mount_dir -o loop
debootstrap --arch=i386 sid $mount_dir $data_url


echo -e "\n4.- Configuring debootstrap downloaded data..."
UUID=`/sbin/blkid -s UUID -o value $hd_dir/$vm_names.img`
echo "UUID: $UUID"

cp "`pwd`/$debootstrap_config_file" "$mount_dir/"

chroot $mount_dir /bin/bash -c "./$debootstrap_config_file $UUID $vm_names;exit"
umount -l $mount_dir


echo -e "\n5.- Starting default network..."
virsh net-start default


### NETWORK 1 ###
contador=0
while [ $contador -lt $number_vm_network1 ]; do

    echo -e "\n6.- Creating hard disk for virtual machine$contador-net1..."
    cp $hd_dir/$vm_names.img $hd_dir/$vm_names-$contador-net1.img


    echo -e "\n7.- Configuring debootstrap networking of $vm_names-$contador-net1..."
    mount $hd_dir/$vm_names-$contador-net1.img $mount_dir -o loop

    cp "`pwd`/$network1_config_file" "$mount_dir/"

    complete_mac="$mac_vm:$ending_ip" #eth0
    chroot $mount_dir /bin/bash -c "./$network1_config_file $ending_ip $complete_mac;exit"
    umount -l $mount_dir


    echo -e "\n8.- Installing first virtual machine on libvirt..."
    virt-install --connect qemu:///system -n $vm_names-$contador-net1 -r 256 --vcpus=1 \
    -w network=default,mac=$complete_mac --os-type linux --os-variant debiansqueeze \
    --hvm --noautoconsole --disk path=$hd_dir/$vm_names-$contador-net1.img,size=$hd_size --import

    virsh destroy $vm_names-$contador-net1

    contador=$(( $contador + 1 ))
    ending_ip=$(( $ending_ip + 1 ))
done


### NETWORK 2 ###
contador=0
while [ $contador -lt $number_vm_network2 ]; do

    echo -e "\n6.- Creating hard disk for virtual machine$contador--net2..."
    cp $hd_dir/$vm_names.img $hd_dir/$vm_names-$contador-net2.img


    echo -e "\n7.- Configuring debootstrap networking of $vm_names-$contador-net2..."
    mount $hd_dir/$vm_names-$contador-net2.img $mount_dir -o loop

    cp "`pwd`/$network2_config_file" "$mount_dir/"

    complete_mac="$mac_vm:$ending_ip" #eth0
    chroot $mount_dir /bin/bash -c "./$network2_config_file $ending_ip $complete_mac;exit"
    umount -l $mount_dir


    echo -e "\n8.- Installing first virtual machine on libvirt..."
    virt-install --connect qemu:///system -n $vm_names-$contador-net2 -r 256 --vcpus=1 \
    -w network=default,mac=$complete_mac --os-type linux --os-variant debiansqueeze \
    --hvm --noautoconsole --disk path=$hd_dir/$vm_names-$contador-net2.img,size=$hd_size --import

    virsh destroy $vm_names-$contador-net2

    contador=$(( $contador + 1 ))
    ending_ip=$(( $ending_ip + 1 ))
done


### ROUTER ###
echo -e "\n6.- Creating hard disk for virtual machine$contador-r1..."
cp $hd_dir/$vm_names.img $hd_dir/$vm_names-$contador-r1.img


echo -e "\n7.- Configuring debootstrap networking of $vm_names-$contador..."
mount $hd_dir/$vm_names-$contador-r1.img $mount_dir -o loop

cp "`pwd`/$network_router_config_file" "$mount_dir/"

complete_mac1="$mac_vm:$ending_ip" #eth0
ending_ip=$(( $ending_ip + 1 ))
complete_mac2="$mac_vm:$ending_ip" #eth1

chroot $mount_dir /bin/bash -c "./$network_router_config_file $complete_mac1 $complete_mac2;exit"
umount -l $mount_dir


echo -e "\n8.- Installing first virtual machine on libvirt..."
virt-install --connect qemu:///system -n $vm_names-router -r 256 --vcpus=1 \
-w network=default,mac="$complete_mac1" -w network=default,mac="$complete_mac2" --os-type linux --os-variant debiansqueeze \
--hvm --noautoconsole --disk path=$hd_dir/$vm_names-$contador-r1.img,size=$hd_size --import

virsh destroy $vm_names-router


echo -e "\nAll done!"


