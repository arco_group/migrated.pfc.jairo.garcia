mac_vm0=$1
mac_vm1=$2

echo "auto lo"                       > /etc/network/interfaces
echo "iface lo inet loopback"        >> /etc/network/interfaces

echo "auto eth0"                     >> /etc/network/interfaces
echo "iface eth0 inet static"        >> /etc/network/interfaces
echo "address 192.168.4.1"         >> /etc/network/interfaces
echo "netmask 255.255.255.0"         >> /etc/network/interfaces
echo "broadcast 192.168.4.255"     >> /etc/network/interfaces
echo "network 192.168.4.0"         >> /etc/network/interfaces

echo "auto eth1"                     >> /etc/network/interfaces
echo "iface eth1 inet static"        >> /etc/network/interfaces
echo "address 192.168.2.1"           >> /etc/network/interfaces
echo "netmask 255.255.255.0"         >> /etc/network/interfaces
echo "broadcast 192.168.2.255"       >> /etc/network/interfaces
echo "network 192.168.2.0"           >> /etc/network/interfaces

echo "SUBSYSTEM==\"net\", ACTION==\"add\", DRIVERS==\"?*\", ATTR{address}==\"$mac_vm0\", ATTR{dev_id}==\"0x0\", ATTR{type}==\"1\", KERNEL==\"eth*\", NAME=\"eth0\"" > /etc/udev/rules.d/70-persistent-net.rules

echo "SUBSYSTEM==\"net\", ACTION==\"add\", DRIVERS==\"?*\", ATTR{address}==\"$mac_vm1\", ATTR{dev_id}==\"0x0\", ATTR{type}==\"1\", KERNEL==\"eth*\", NAME=\"eth1\"" >> /etc/udev/rules.d/70-persistent-net.rules

mount -t proc proc /proc
echo 1 > /proc/sys/net/ipv4/ip_forward
sed -i "s/#net.ipv4.ip_forward=1/net.ipv4.ip_forward=1/" /etc/sysctl.conf

/etc/init.d/networking restart

