#!/usr/bin/python
# -*- mode:python; coding:utf-8; tab-width:4 -*-

from dindesConf import slices, systemProxies, systemAdapters #, systemTimes
from Exceptions import UnknownPluginClassNameException, PluginClassNotLoadedException
from Exceptions import UnknownPluginMethodNameException

import Ice, IceStorm
Ice.loadSlice(slices['dindesSliceDeployed'])
import DInDeS

from MonitoringSystem import MonitoringSystem
from SniffingNetwork import SniffingNetwork



#===============================================================================
# ProbeFacade class
#===============================================================================
class ProbeFacade(DInDeS.ProbeQuery):
    def __init__(self):
        self.__inputDataManager = InputDataManager()
        self.__subscriptionManager = SubscriptionManager(self.__inputDataManager)
        self.__pluginManager = PluginManager(self.__subscriptionManager, self.__inputDataManager)

#------------------------------------------------------------------------------ 
# manager getters
#------------------------------------------------------------------------------
    def getInputDataManager(self):
        return self.__inputDataManager

    def getSubscriptionManager(self):
        return self.__subscriptionManager

    def getPluginManager(self):
        return self.__pluginManager

#------------------------------------------------------------------------------ 
# inputDataManager
#------------------------------------------------------------------------------
    def getInputArguments(self):
        return self.__inputDataManager.getArguments()

    def setInputArguments(self, args, iceInstance):
        self.__inputDataManager.setArguments(args)
        self.__inputDataManager.setIceInstance(iceInstance)

#------------------------------------------------------------------------------ 
# subcriptionManager
#------------------------------------------------------------------------------
    def getSubscriptionProxy(self):
        return self.__subscriptionManager.getSubscriptionProxy()

    def createProxyEndpoints(self):
        self.__subscriptionManager.createProxyEndpoints(self)

    def createTopicManager(self, name):
        self.__subscriptionManager.createTopicManager(name)

    def subscribeMonitor(self, monitorProxy, current=None):
        return self.__subscriptionManager.subscribeMonitor(monitorProxy)

    def unsubscribeMonitor(self, monitorProxy, current=None):
        self.__subscriptionManager.unsubscribeMonitor(monitorProxy)

    def ping(self, current=None):
        return self.__subscriptionManager.ping()

#------------------------------------------------------------------------------ 
# pluginManager
#------------------------------------------------------------------------------
    def loadClass(self):
        self.__pluginManager.loadClass()

    def executeMethod(self):
        self.__pluginManager.executeMethod()

    def getNumberOfAlerts(self, current=None):
        return self.__pluginManager.getNumberOfAlerts()

    def getNumberOfData(self, current=None):
        return self.__pluginManager.getNumberOfData()



#===============================================================================
# InputDataManager class
#===============================================================================
class InputDataManager():
    def __init__(self):
        self.__arguments = {}
        self.__iceInstance = None

#------------------------------------------------------------------------------ 
# getters & setters
#------------------------------------------------------------------------------
    def getArguments(self):
        return self.__arguments

    def setArguments(self, args):
        self.__arguments = args

    def getIceInstance(self):
        return self.__iceInstance

    def setIceInstance(self, iceInstance):
        self.__iceInstance = iceInstance



#===============================================================================
# SubscriptionManager class
#===============================================================================
class SubscriptionManager():
    def __init__(self, imanager):
        self.__imanager = imanager
        self.__topic = None
        self.__publisher = None
        self.__subscriptionProxy = None
        #self.__actuatorProxy = None
        self.__alertState = False
        self.__alertCounter = 0

#------------------------------------------------------------------------------ 
# getters
#------------------------------------------------------------------------------
    def getTopic(self):
        return self.__topic

    def getPublisher(self):
        return self.__publisher

    def getSubscriptionProxy(self):
        return self.__subscriptionProxy

#------------------------------------------------------------------------------ 
# class methods
#------------------------------------------------------------------------------
# ice methods
#------------------------------------------------------------------------------ 
    def createProxyEndpoints(self, probeInstance):
        iceInstance = self.__imanager.getIceInstance()
        probeAdapter = iceInstance.createObjectAdapterWithEndpoints(systemAdapters['subscriptionEndpoint'], 'tcp')
        pIdentity = iceInstance.stringToIdentity(self.__imanager.getArguments()['probeName'])
        self.__subscriptionProxy = probeAdapter.add(probeInstance, pIdentity)
        print 'Subscription Proxy: ', self.__subscriptionProxy
        probeAdapter.activate()

    def createTopicManager(self, topicName):
        iceInstance = self.__imanager.getIceInstance()
        topicProxyTyped = self.__imanager.getArguments()['topicProxy']
        if not topicProxyTyped:
            topicProxyTyped = systemProxies['topicManager']
        proxy = iceInstance.stringToProxy(topicProxyTyped)
        topicMgr = IceStorm.TopicManagerPrx.checkedCast(proxy)
        try:
            self.__topic = topicMgr.create(topicName)
        except IceStorm.TopicExists:
            self.__topic = topicMgr.retrieve(topicName)
        publisher = self.getTopic().getPublisher().ice_oneway()
        self.__publisher = DInDeS.MonitorPrx.uncheckedCast(publisher)
#------------------------------------------------------------------------------ 
# subcription methods
#------------------------------------------------------------------------------ 
    def subscribeMonitor(self, monitorProxy):
        print 'New monitor subscribed!'
        try:
            self.__topic.subscribeAndGetPublisher(None, monitorProxy)
            return self.__getProbeInfo()
        except Exception, e:
            #return self.__getProbeInfo()
            pass

    def __getProbeInfo(self):
        p = DInDeS.ProbeInfo()
        p.probeId = self.__imanager.getArguments()['probeName']
        p.alertState = self.__alertState
        return p

    def unsubscribeMonitor(self, monitorProxy):
        print 'Unsubscribing monitor...'
        self.__topic.unsubscribe(monitorProxy)
        print 'Monitor unsubscribed'
#------------------------------------------------------------------------------ 
# ping probe
#------------------------------------------------------------------------------ 
    def ping(self):
        print 'Ping!'
        return self.__getProbeInfo()
#------------------------------------------------------------------------------ 
# sending information to Topic Manager
#------------------------------------------------------------------------------ 
    def sendAlert(self, brokenRule):
        print 'ALERT SENT, from ', self.__imanager.getArguments()['probeName']
        self.__publisher.alert(brokenRule)

    def sendInformation(self, timevalData):
        #print 'INFO SENT, from ', self.__imanager.getArguments()['probeName']
        self.__alertState = timevalData.alertState #self.__alreadyInAlertState()
        self.__publisher.timevalInformation(timevalData)



#===============================================================================
# PluginManager class
#===============================================================================
class PluginManager():
    def __init__(self, subsInstance, inputInstance):
        self.__sManager = subsInstance
        self.__iManager = inputInstance
        self.__classSelected = None
        self.__probeClasses = {}
        self.__registerPlugin('sniffing', SniffingNetwork)
        self.__registerPlugin('monitoring', MonitoringSystem)

#------------------------------------------------------------------------------ 
# getters
#------------------------------------------------------------------------------ 
    def getClassSelected(self):
        return self.__classSelected

    def getNumberOfAlerts(self):
        return self.__classSelected.getNumberAlerts()

    def getNumberOfData(self):
        return self.__classSelected.getNumberData()

#------------------------------------------------------------------------------ 
# class methods
#------------------------------------------------------------------------------
    def __registerPlugin(self, plugId, plugClass):
        self.__probeClasses[plugId] = plugClass

    def loadClass(self):
        methodName = self.__iManager.getArguments()['plugin']
        plugKey = self.__getPluginIdByMethodSelected(methodName)
        try:
            self.__classSelected = self.__probeClasses[plugKey](self.__sManager.sendAlert, self.__sManager.sendInformation)
        except KeyError:
            raise UnknownPluginClassNameException('Unknown plugin name')

    def __getPluginIdByMethodSelected(self, method):
        if getattr(MonitoringSystem(None, None), '_' + method, False):
            return 'monitoring'
        elif getattr(SniffingNetwork(None, None), '_' + method, False):
            return 'sniffing'
        else:
            raise UnknownPluginMethodNameException('Unknown method name: ' + method)

    def executeMethod(self):
        try:
            configureArgs = self.__iManager.getArguments()
            configureArgs.update({'iceInstance' : self.__iManager.getIceInstance()})
            self.__classSelected.configure(**configureArgs)
        except AttributeError:
            raise PluginClassNotLoadedException('Plugins class has not been loaded')
        self.__classSelected.startToRun()



