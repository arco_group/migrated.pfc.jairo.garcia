from django.shortcuts import render_to_response, HttpResponseRedirect
import os
from re import match
#from django import forms

def makeWeb(request):
    files = os.listdir('./web/web/static/info/')
    probeList = __getProbeList(files)
    generalLog = __getGeneralLog()
    cssFile = 'css/style.css'

    html = render_to_response('web.html',
                              {'probeElements':probeList,
                               'subscribeCounter':len(probeList),
                               'cssName':cssFile,
                               'generalLog':generalLog,
                               }
                              )
    return html


def __getGeneralLog():
    log = 'info/' + 'GeneralLog.txt'
    if __readFile(log) == '':
        log = ''
    return log


def __readFile(fileDir):
    try:
        f = open('./web/web/static/' + fileDir, 'r')
        res = f.read()
        f.close()
    except:
        res = ''
    return res


def __getProbeList(fileNames):
    probeList = []
    probeNames = []
    for name in fileNames:
        if match('Probe([0-9]+)Info.txt', name):
            probeNames.append(name.split('Info.txt')[0])
    probeNames.sort()
    for probe in probeNames:
        pi = ProbeInformation(probe)
        probeList.append(pi)
    return probeList


class ProbeInformation():
    def __init__(self, name):
        self.dir = 'info/'
        self.Name = name
        self.AlertGraph = self.dir + self.Name + 'Alerts.png'
        self.InfoGraph = self.dir + self.Name + 'Info.png'
        self.AlertLog = self.dir + self.Name + 'Alerts.txt'
        self.InfoLog = self.dir + self.Name + 'Info.txt'
        self.Status = self.readFile('./web/web/static/' + self.dir + self.Name + 'Status.txt')

    def readFile(self, fileDir):
        f = open(fileDir, 'r')
        res = f.read()
        f.close()
        return res.split('\n')[0]

'''
IMP_CHOICES = (
    ('1', 'imp 1'),
    ('2', 'imp 2'),
    ('3', 'imp 3'),
    ('4', 'imp 4'),
)

class UploadFileForm(forms.Form):
    imp = forms.ChoiceField(choices=IMP_CHOICES)



def contact(request):
    if request.method == 'POST':
        return HttpResponseRedirect('/gracias/')

'''
