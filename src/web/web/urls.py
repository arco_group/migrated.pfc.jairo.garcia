from django.conf.urls import patterns, include, url
from django.contrib import admin
from web.settings import MEDIA_ROOT

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^pfcweb/$', 'principal.views.makeWeb'),
    url(r'^pfcweb/(?P<path>.*)$', 'django.views.static.serve', {'document_root': MEDIA_ROOT},),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
    # Examples:
    # url(r'^$', 'web.views.home', name='home'),
    # url(r'^web/', include('web.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
