#!/usr/bin/python
# -*- mode:python; coding:utf-8; tab-width:4 -*-

from dindesConf import slices, systemAdapters, networkFiles, ipTablesConf
import Ice
Ice.loadSlice(slices['dindesSliceDeployed'])
import DInDeS

import socket, fcntl, struct, subprocess



#===============================================================================
# Iptables manager
#===============================================================================
# iptables -t--> tabla--> tipo_operación--> cadena--> regla_con_parámetros--> ACCION
# iptables -t    filter       -A            INPUT     -p tcp -dport 23        -j DROP
class IptablesManager():
    def __init__(self):
        if ipTablesConf['ready']:
            initIptable = ipTablesConf['policy']
            self.applyAction(initIptable)
        else:
            pass

    def run(self, makeAction):
        rule = self.configureAction(makeAction)
        self.applyAction(rule)

    def configureAction(self, makeAction):
        rule = 'iptables -t filter -A FORWARD '
        rule += self.__getProto(makeAction)
        rule += self.__getIP(makeAction)
        rule += self.__getPort(makeAction)
        rule += self.__getTypeAction(makeAction)
        return rule

    def applyAction(self, rule):
        if ipTablesConf['ready']:
            subprocess.call(rule.split())
        else:
            pass

    def deleteActions(self):
        if ipTablesConf['ready']:
            iptables = 'iptables -F'
            subprocess.call(iptables.split())
        else:
            pass

#------------------------------------------------------------------------------ 
# private methods
#------------------------------------------------------------------------------ 
    def __getProto(self, action):
        proto = ''
        if action.transportProto == DInDeS.TransportProtocol.tcp:
            proto = '-p %s ' % str(DInDeS.TransportProtocol.tcp)
        elif action.transportProto == DInDeS.TransportProtocol.udp:
            proto = '-p %s ' % DInDeS.TransportProtocol.udp
        elif action.transportProto == DInDeS.TransportProtocol.icmp:
            proto = '-p %s ' % DInDeS.TransportProtocol.icmp
        return proto

    def __getIP(self, action):
        ip = ''
        if action.direction == DInDeS.PacketDirection.src:
            ip = '-s %s ' % action.ipAddress
        elif action.direction == DInDeS.PacketDirection.dst:
            ip = '-d %s ' % action.ipAddress
        return ip

    def __getPort(self, action):
        port = ''
        if action.port != 0:
            if action.direction == DInDeS.PacketDirection.src:
                port = '--sport %s ' % str(action.port)
            elif action.direction == DInDeS.PacketDirection.dst:
                port = '--dport %s ' % str(action.port)
        return port

    def __getTypeAction(self, action):
        typeAction = ''
        if action.typeAction == DInDeS.Action.allow:
            typeAction = '-j %s ' % str(DInDeS.Action.allow).upper()
        elif action.typeAction == DInDeS.Action.drop:
            typeAction = '-j %s ' % str(DInDeS.Action.drop).upper()
        elif action.typeAction == DInDeS.Action.reject:
            typeAction = '-j %s ' % str(DInDeS.Action.reject).upper()
        return typeAction



#===============================================================================
# Router class
#===============================================================================
class Router(DInDeS.RouterQuery):
    def __init__(self, routerName):
        self.__routerName = routerName
        self.__iptablesManager = IptablesManager()
        self.__routerProxy = None
        self.__actionsToApply = []

#------------------------------------------------------------------------------ 
# getter & setters
#------------------------------------------------------------------------------
    def getIpTablesManager(self):
        return self.__iptablesManager

    def getRouterProxy(self):
        return self.__routerProxy

    def getActionsToApply(self):
        return self.__actionsToApply

#------------------------------------------------------------------------------ 
# configure router
#------------------------------------------------------------------------------ 
    def configureRouter(self, iceInstance):
        routerAdapter = iceInstance.createObjectAdapterWithEndpoints(systemAdapters['routerEndpoint'], 'tcp')
        self.__routerProxy = routerAdapter.add(self, iceInstance.stringToIdentity(self.__routerName))
        routerAdapter.activate()
        print 'Router Proxy: ', str(self.__routerProxy)

#------------------------------------------------------------------------------ 
# action methods
#------------------------------------------------------------------------------ 
    def setAction(self, action, current=None):
        print 'New action received: ', action
        self.__actionsToApply.append(action)

    def getActionsConfigured(self, current=None):
        sentActions = self.__actionsToApply
        print 'Getting actions sent.'
        return sentActions

    def deleteActions(self, current=None):
        res = True
        self.__actionsToApply = []
        try:
            self.__iptablesManager.deleteActions()
            print 'Actions deleted.'
        except:
            res = False
        return res

    def applyActions(self, current=None):
        res = True
        try:
            for action in self.__actionsToApply:
                self.__iptablesManager.run(action)
            print 'Actions applied.'
        except:
            res = False
        return res

#------------------------------------------------------------------------------ 
# ping router
#------------------------------------------------------------------------------
    def ping(self, current=None):
        print 'Ping!'
        return self.__getRouterInfo()

#------------------------------------------------------------------------------ 
# router info
#------------------------------------------------------------------------------
    def getName(self, current=None):
        print 'Getting name.'
        return str(self.__routerName)

    def getIfaces(self, current=None):
        ifaces = {}
        for line in open(networkFiles['devFile'], 'r'):
            if len(line.split(':')) == 2:
                ifaceName = line.split()[0][:-1]
                ifaces[ifaceName] = self.__getIpAddress(ifaceName)
        print 'Getting network ifaces.'
        return ifaces

    def __getRouterInfo(self):
        p = DInDeS.RouterInfo()
        p.routerName = str(self.__routerName)
        p.ifaces = self.getIfaces()
        p.sentActions = self.__actionsToApply
        return p

    # http://code.activestate.com/recipes/439094/
    def __getIpAddress(self, ifaceName):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        try:
            ip = socket.inet_ntoa(fcntl.ioctl(s.fileno(), 0x8915, struct.pack('256s', ifaceName[:15]))[20:24])
        except:
            ip = ''
        return ip



