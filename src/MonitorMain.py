#!/usr/bin/python
# -*- mode:python; coding:utf-8; tab-width:4 -*-

from dindesConf import systemProxies, systemTimes
from Exceptions import ArgumentParsingException

import Ice, IceGrid

import sys, time

from Monitor import Monitor



#===============================================================================
# MonitorMain class
#===============================================================================
class MonitorMain(Ice.Application):
    def __init__(self):
        self.__monitor = Monitor()

#------------------------------------------------------------------------------ 
# start monitor method
#------------------------------------------------------------------------------ 
    def run(self, argv):
        self.shutdownOnInterrupt()
        ic = self.communicator()

        try:
            proxies = self.getProbeProxies(ic)
        except ArgumentParsingException:
            return 0
        try:
            time.sleep(systemTimes['monitorWaitingTime'])
            self.__monitor.configureMonitor(proxies, ic)
        except Exception, e:
            print 'Main Configuration Error: %s' % e
            return 0

        print 'Working...'
        print 'Press Ctrl+C for stopping.'
        ic.waitForShutdown()
        print 'Stopping...'
        self.__monitor.stopMonitor()
        print 'Stopped!'
        return 0

    def getProbeProxies(self, iceInstance):
        try:
            query = IceGrid.QueryPrx.checkedCast(iceInstance.stringToProxy(systemProxies['icegridQuery']))
            proxies = query.findAllObjectsByType('::DInDeS::ProbeQuery')
            print 'PROXIES: ', proxies
        except Exception:
            raise ArgumentParsingException('Parsing error with arguments. Error while searching probe queries')
        return proxies

#===============================================================================
# main
#===============================================================================
if __name__ == "__main__":
    MonitorMain().main(sys.argv)



