#!/usr/bin/python
# -*- mode:python; coding:utf-8; tab-width:4 -*-


#===============================================================================
# Exception with arguments
#===============================================================================
class NoInputArgumentsIntroducedException(Exception):
    def __init__(self, info):
        self.info = info
    def __str__(self):
        return repr(self.info)

class IncorrectOptionException(Exception):
    def __init__(self, info):
        self.info = info
    def __str__(self):
        return repr(self.info)

class OptionParsingError(RuntimeError):
    def __init__(self, msg):
        self.msg = msg

class OptionParsingExit(Exception):
    def __init__(self, status, msg):
        self.msg = msg
        self.status = status

class ArgumentParsingException(Exception):
    def __init__(self, info):
        self.info = info
    def __str__(self):
        return repr(self.info)

class ArgumentParsingExitException(Exception):
    def __init__(self, info):
        self.info = info
    def __str__(self):
        return repr(self.info)

class UnknownPromiscModeException(Exception):
    def __init__(self, info):
        self.info = info
    def __str__(self):
        return repr(self.info)

class UnknownRouterArgumentCombinationException(Exception):
    def __init__(self, info):
        self.info = info
    def __str__(self):
        return repr(self.info)


#===============================================================================
# Exception in load plugins  
#===============================================================================
class UnknownPluginClassNameException(Exception):
    def __init__(self, info):
        self.info = info
    def __str__(self):
        return repr(self.info)


class PluginClassNotLoadedException(Exception):
    def __init__(self, info):
        self.info = info
    def __str__(self):
        return repr(self.info)

class UnknownPluginMethodNameException(Exception):
    def __init__(self, info):
        self.info = info
    def __str__(self):
        return repr(self.info)


#===============================================================================
# Exception in configure plugins
#===============================================================================
class IncorrectPluginConfigurationException(Exception):
    def __init__(self, info):
        self.info = info
    def __str__(self):
        return repr(self.info)

class UnknownNetworkIfaceException(Exception):
    def __init__(self, info):
        self.info = info
    def __str__(self):
        return repr(self.info)

class NegativeTimevalSelectedException(Exception):
    def __init__(self, info):
        self.info = info
    def __str__(self):
        return repr(self.info)


#===============================================================================
# Exception in addRule
#===============================================================================
class ErrorCreatingRuleException(Exception):
    def __init__(self, info):
        self.info = info
    def __str__(self):
        return repr(self.info)

class RuleAlreadyExistsException(Exception):
    def __init__(self, info):
        self.info = info
    def __str__(self):
        return repr(self.info)


#===============================================================================
# Exception with data base
#===============================================================================
class CreateDatabaseException(Exception):
    def __init__(self, info):
        self.info = info
    def __str__(self):
        return repr(self.info)

class AddDataToDatabaseException(Exception):
    def __init__(self, info):
        self.info = info
    def __str__(self):
        return repr(self.info)

class CreateDataGraphException(Exception):
    def __init__(self, info):
        self.info = info
    def __str__(self):
        return repr(self.info)

class UnknownProbeKeyException(Exception):
    def __init__(self, info):
        self.info = info
    def __str__(self):
        return repr(self.info)


#===============================================================================
# Exception with subscriptions
#===============================================================================
class SubscribeMonitorException(Exception):
    def __init__(self, info):
        self.info = info
    def __str__(self):
        return repr(self.info)

class NoProbeSubscriptionException(Exception):
    def __init__(self, info):
        self.info = info
    def __str__(self):
        return repr(self.info)

class UnsubscribeMonitorException(Exception):
    def __init__(self, info):
        self.info = info
    def __str__(self):
        return repr(self.info)


#===============================================================================
# Exception with actions
#===============================================================================
class IncorrectActionException(Exception):
    def __init__(self, info):
        self.info = info
    def __str__(self):
        return repr(self.info)


#===============================================================================
# Exceptions with MonitorModel
#===============================================================================
class NoProbesMonitorizedException(Exception):
    def __init__(self, info):
        self.info = info
    def __str__(self):
        return repr(self.info)

'''
#===============================================================================
# Colors in error messages
#===============================================================================

class ColorMessage:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'

    def disable(self):
        self.HEADER = ''
        self.OKBLUE = ''
        self.OKGREEN = ''
        self.WARNING = '' 
        self.FAIL = ''
        self.ENDC = ''

'''
