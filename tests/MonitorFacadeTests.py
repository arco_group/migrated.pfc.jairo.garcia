#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

from src.dindesConf import slices, directories
from src.Exceptions import NoProbeSubscriptionException, UnsubscribeMonitorException
from src.Exceptions import SubscribeMonitorException

import Ice
Ice.loadSlice(slices['dindesSliceLocal'])
import DInDeS

from unittest import TestCase
from mock import Mock
from os import path
from time import asctime

from src.MonitorFacade import MonitorFacade
from src.Probe import Probe
from src.Monitor import Monitor



#===============================================================================
# MockMonitor class
#===============================================================================
class MockMonitor(Monitor):
    alert = Mock()
    timevalInformation = Mock()



#===============================================================================
# MonitorFacade class
#===============================================================================
class MonitorFacadeTests(TestCase):
    def setUp(self):
        self.iceInstance = Ice.initialize()
        self.databaseDir = directories['databaseDir']
        self.picAndLogDir = directories['picAndLogDir']

        self.alert = DInDeS.AlertRule()
        self.alert.methodName = DInDeS.MethodLoaded.sniffNetwork
        self.alert.data = 'tcp and port 2345'
        self.alert.timestamp = str(asctime())
        self.alert.probeId = 'Probe1ID'

        self.data = DInDeS.TimevalData()
        self.data.methodName = DInDeS.MethodLoaded.bytesTransmitted
        self.data.data = "12341 / 999999"
        self.data.alertState = False
        self.data.timestamp = str(asctime())
        self.data.probeId = 'Probe2ID'

        self.mFacade = MonitorFacade()


    def tearDown(self):
        if len(self.mFacade.getProbesMonitorized()) > 0:
            self.mFacade.unsubscribeMonitorMethodsOnProbe()
        if self.existsFile(self.picAndLogDir + 'GeneralLog.txt'):
            self.deleteContentFile(self.picAndLogDir + 'GeneralLog.txt')

    def existsFile(self, filename):
        if path.isfile(filename):
            return True
        else:
            return False

    def deleteContentFile(self, filedir):
        f = open(filedir, 'w')
        f.write("")
        f.close()

    def readFile(self, filedir):
        f = open(filedir, 'r')
        info = f.readline()
        #info = info.split('.-')[1]
        f.close()
        return info


#------------------------------------------------------------------------------ 
# Monitor subscription manager
#------------------------------------------------------------------------------
    # Simulamos la subscripción de un monitor en una sonda. Para ello configuramos 
    # una sonda básica y le mandamos el monitor para que nos inscriba a ella.
    def testSubscribeMethods(self):
        #given
        arguments = {'probeName': 'Sonda101', 'plugin':'bytesTransmitted', 'iface':'wlan0', 'timeval':'432', 'rule':[], 'promisc':False, 'topicProxy':''}
        probe = Probe()
        probe.configureProbe(arguments, self.iceInstance)
        probeProxy = probe.getProbeFacade().getSubscriptionProxy()

        monitor = MockMonitor()
        #when
        self.mFacade.subscribeMonitorMethodsOnProbe(monitor, probeProxy, self.iceInstance)
        #then
        self.assertEquals(len(self.mFacade.getSubscriptionManager().getProbesMonitorized()), 1)

    # Simulamos la subscripción de un monitor en una sonda. Para ello primero configuramos
    # una sonda básica
    def testUnsubscribeMethods(self):
        #given
        arguments = {'probeName': 'Sonda2', 'plugin':'bytesTransmitted', 'iface':'wlan0', 'timeval':'432', 'rule':[], 'promisc':False, 'topicProxy':''}
        probe = Probe()
        probe.configureProbe(arguments, self.iceInstance)
        probeProxy = probe.getProbeFacade().getSubscriptionProxy()
        p = probe.getProbeFacade()
        p.getSubscriptionManager().getTopic().unsubscribe = Mock()

        monitor = MockMonitor()
        self.mFacade.subscribeMonitorMethodsOnProbe(monitor, probeProxy, self.iceInstance)
        result = self.mFacade.getProbesMonitorized()
        #when
        self.mFacade.unsubscribeMonitorMethodsOnProbe()
        #then
        self.assertEquals(len(result), 1)
        self.assertTrue(p.getSubscriptionManager().getTopic().unsubscribe.called)

    def testUnsubscribeMethodsBeforeSubscription(self):
        #given
        self.mFacade.getInputDataManager().setIceInstance(self.iceInstance)
        result = self.mFacade.getProbesMonitorized()
        #when
        methodFail = self.mFacade.unsubscribeMonitorMethodsOnProbe
        #then
        self.assertEquals(len(result), 0)
        self.assertRaises(UnsubscribeMonitorException, methodFail)


#------------------------------------------------------------------------------ 
# Monitor information received manager
#------------------------------------------------------------------------------
    # Nos subscribimos a una sonda y le mandamos un mensaje de ping para comprobar
    # que la conexión está correctamente establecida.
    def testProbePing(self):
        #given
        arguments = {'probeName': 'Sonda303', 'plugin':'bytesTransmitted', 'iface':'wlan0', 'timeval':'432', 'rule':[], 'promisc':False, 'topicProxy':''}
        probe = Probe()
        probe.configureProbe(arguments, self.iceInstance)
        probeProxy = probe.getProbeFacade().getSubscriptionProxy()
        probe.getProbeFacade().getSubscriptionManager().ping = Mock(return_value=DInDeS.ProbeInfo())

        self.mFacade.setIceInstance(self.iceInstance)
        mModel = self.mFacade.getInformationManager().getMonitorModel()
        mModel.setProbeStatus = Mock()
        self.mFacade.subscribeMonitorMethodsOnProbe(MockMonitor(), probeProxy, self.iceInstance)
        #when
        self.mFacade.pingProbes()
        #then
        self.assertTrue(probe.getProbeFacade().getSubscriptionManager().ping.called)
        self.assertTrue(mModel.setProbeStatus.called)

    # Intentamos hacer ping a una sonda a la que no estamos subscritos. Comprobamos
    # que nos lanza un error.
    def testProbePingFail(self):
        #given
        arguments = {'probeName': 'Sonda4', 'plugin':'bytesTransmitted', 'iface':'wlan0', 'timeval':'432', 'rule':[], 'promisc':False, 'topicProxy':''}
        probe = Probe()
        probe.configureProbe(arguments, self.iceInstance)
        probe.getProbeFacade().getSubscriptionManager().ping = Mock(return_value=DInDeS.ProbeInfo())

        probeProxy = ''
        self.mFacade.setIceInstance(self.iceInstance)
        mModel = self.mFacade.getInformationManager().getMonitorModel()
        mModel.setProbeStatus = Mock()
        #when
        methodFail = self.mFacade.subscribeMonitorMethodsOnProbe
        self.mFacade.pingProbes()
        #then
        self.assertRaises(SubscribeMonitorException, methodFail, MockMonitor(), probeProxy, self.iceInstance)
        self.assertFalse(probe.getProbeFacade().getSubscriptionManager().ping.called)

#------------------------------------------------------------------------------ 
    # Simulamos que nos hemos subscrito a una sonda y tenemos sus datos almacenados.
    # Creamos correctamente la base de datos para esa sonda.
    def testCreateDataBase(self):
        #given
        probesMonitorized = {'Probe1' : {'probeId':'Probe1ID', 'proxy':'ABC-123'}}
        #when
        self.mFacade.getInformationManager().createDataBase(probesMonitorized)
        #then
        result1 = self.mFacade.getInformationManager().getMonitorModel().getProbesMonitorized()
        self.assertEquals(len(result1), 1)

    # Intentamos ejecutar el método de crear base de datos nueva para una sonda, 
    # pero sin habernos subscrito a una de ellas. Se lanza un error.
    def testCreateDataBaseWithNoProbesMonitorized(self):
        #given
        mFacade = MonitorFacade()
        #when
        methodFail = mFacade.createDataBase
        #then
        self.assertRaises(NoProbeSubscriptionException, methodFail)

    # Simulamos que recibimos una nueva alerta y la introducimos en el fichero de
    # alertas recibidas para una sonda. Vemos como se ha introducido correctamente.
    def testAddNewAlert(self):
        #given
        probesMonitorized = {'Probe1' : {'probeId':'Probe1ID', 'proxy':'ABC-123'}}
        self.mFacade.getInformationManager().createDataBase(probesMonitorized)
        self.mFacade.createLogFiles()
        #when
        self.mFacade.addAlert(self.alert)
        self.mFacade.createGraphs()
        #then
        self.assertTrue(self.existsFile(self.picAndLogDir + 'GeneralLog.txt'))
        info = self.readFile(self.picAndLogDir + 'GeneralLog.txt')
        alertString = 'New broken rule from ' + str(self.alert.probeId)
        alertString += ': ' + self.alert.data + '\n'
        self.assertEquals(info.split('.- ')[1], alertString)

    # Simulamos que hemos recibido una nueva información temporal y la intentamos 
    # introducir al fichero de información temporal de una sonda. Todo Ok. 
    def testAddNewInformation(self):
        #given
        probesMonitorized = {'Probe2' : {'probeId':'Probe2ID', 'proxy':'ABC-123'}}
        self.mFacade.getInformationManager().createDataBase(probesMonitorized)
        self.mFacade.createLogFiles()
        #when
        self.mFacade.addInformation(self.data)
        self.mFacade.createGraphs()
        #then
        self.assertTrue(self.existsFile(self.picAndLogDir + 'Probe2Info.txt'))
        info = self.readFile(self.picAndLogDir + 'Probe2Info.txt')
        infoString = self.data.timestamp + '.- Info from ' + str(self.data.probeId)
        infoString += ' (AlertState: ' + str(self.data.alertState) + '): ' + str(self.data.data) + '\n'
        self.assertEquals(info, infoString)

    # Se simula que nos hemos subscrito a una sonda para monitorizarla y le indicamos
    # que queremos crear los gráficos que vayan mostrando lo que ha pasado tiempo atrás. 
    def testCreateAlertsGraph(self):
        #given
        probesMonitorized = {'Probe3' : {'probeId':'Probe3ID', 'proxy':'ABC-123'}}
        self.mFacade.getInformationManager().createDataBase(probesMonitorized)
        self.mFacade.getInformationManager().getMonitorModel().createInformationGraph
        self.mFacade.getInformationManager().getMonitorModel().createAlertsGraph
        #when
        self.mFacade.createGraphs()
        #then
        self.assertTrue(self.existsFile(self.picAndLogDir + 'Probe3Alerts.png'))
        self.assertTrue(self.existsFile(self.picAndLogDir + 'Probe3Info.png'))

    # Se simula se quiere introducir una información en el log general del sistema
    def testAddNewInfoToGeneralLog(self):
        #given
        msg = 'This is a message\n'
        probesMonitorized = {'Probe4' : {'probeId':'Probe4ID', 'proxy':'ABC-123'}}
        self.mFacade.getInformationManager().createDataBase(probesMonitorized)
        self.mFacade.createLogFiles()
        #when
        self.mFacade.addInfoToGeneralLog(msg)
        #then 
        self.assertTrue(self.existsFile(self.picAndLogDir + 'GeneralLog.txt'))
        info = self.readFile(self.picAndLogDir + 'GeneralLog.txt')
        self.assertEquals(info.split('.- ')[1], msg)

#------------------------------------------------------------------------------ 
# mail system
#------------------------------------------------------------------------------
    # Se quiere simular que se ha recibido una alerta y se manda un email al administrador
    # del sistema para indicarle de la incidencia
    def testSendMailWithSniffingNetworkIncident(self):
        #given
        self.mFacade.getMailNotificationManager().brokenRuleSendMail = Mock()
        probesMonitorized = {'Probe1' : {'probeId':'Probe1ID', 'proxy':'ABC-123'}}
        self.mFacade.getInformationManager().createDataBase(probesMonitorized)
        self.mFacade.createLogFiles()
        #when
        self.mFacade.addAlert(self.alert)
        #then
        self.assertTrue(self.mFacade.getMailNotificationManager().brokenRuleSendMail.called)

    # Se quiere simular que se ha caido una sonda que se estaba controlando y se 
    # comprueba que se manda un email confirmándolo. Se manda un ping a una sonda
    # guardada y se manda el mail
    def testSendMailWithProbeProblems(self):
        #given 
        self.mFacade.getMailNotificationManager().probeStatusChangedSendMail = Mock()
        self.mFacade.getInformationManager().getMonitorModel().setProbeStatus = Mock()
        newProbeInfo = {'probeId':'Probe1ID', 'proxy':'ABC-123'}
        self.mFacade.getSubscriptionManager().setProbesMonitorized(newProbeInfo)
        #when
        self.mFacade.pingProbes()
        #then
        self.assertTrue(self.mFacade.getInformationManager().getMonitorModel().setProbeStatus.called)
        self.assertTrue(self.mFacade.getMailNotificationManager().probeStatusChangedSendMail.called)





