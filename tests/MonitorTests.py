#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

import Ice

from unittest import TestCase
from mock import Mock

from src.Monitor import Monitor
from src.Probe import Probe



#===============================================================================
# MonitorTests
#===============================================================================
class MonitorTests(TestCase):
    def setUp(self):
        self.iceInstance = Ice.initialize()
        self.monitor = Monitor()

    def tearDown(self):
        if len(self.monitor.getMonitorFacade().getProbesMonitorized()) > 0:
            self.monitor.stopMonitor()

#------------------------------------------------------------------------------ 
# Configuration
#------------------------------------------------------------------------------ 
    # Introducimos los argumentos (proxy de la sonda) y vemos si se han ejecutado 
    # todos los métodos de configuración correctamente, devolviendo 0 en tal caso.
    def testMonitorizeNewProbe(self):
        #given
        proxyList = ['proxy1']
        self.monitor.getMonitorFacade().subscribeMonitorMethodsOnProbe = Mock()
        self.monitor.getMonitorFacade().createDataBase = Mock()
        self.monitor.getMonitorFacade().createLogFiles = Mock()
        self.monitor.getMonitorFacade().createGraphs = Mock()
        self.monitor.startPingProbes = Mock()
        #when
        self.monitor.configureMonitor(proxyList, self.iceInstance)
        #then
        self.assertTrue(self.monitor.getMonitorFacade().subscribeMonitorMethodsOnProbe.called)
        self.assertTrue(self.monitor.getMonitorFacade().createDataBase.called)
        self.assertTrue(self.monitor.getMonitorFacade().createLogFiles.called)
        self.assertTrue(self.monitor.getMonitorFacade().createGraphs.called)

    # Introducimos los argumentos correctos, pero no nos suscribimos a una sonda.
    # Vemos que a la hora de crear la base de datos, produce un error y devuelve
    # 1 confirmándolo.
    def testMonitorizedNewProbeWithCreatingDatabaseException(self):
        #given
        proxyList = ['proxy1']
        self.monitor.getMonitorFacade().subscribeMonitorMethodsOnProbe = Mock()
        self.monitor.getMonitorFacade().createLogFiles = Mock()
        self.monitor.startPingProbes = Mock()
        #when
        self.monitor.configureMonitor(proxyList, self.iceInstance)
        #then
        self.assertTrue(self.monitor.getMonitorFacade().subscribeMonitorMethodsOnProbe.called)
        self.assertFalse(self.monitor.getMonitorFacade().createLogFiles.called)

    # Introducimos los argumentos correctos para poder monitorizar varias sondas.
    # Para ello, introducimos los proxys de cada una de las sondas que queremos 
    # monitorizar
    def testMonitorizeServeralProbes(self):
        #given
        options = {'probeName': 'Sonda1-1', 'plugin':'bytesReceived', 'iface':'eth0', 'timeval':'2', 'rule':['udp'], 'promisc':False, 'topicProxy':''}
        p1 = Probe()
        p1.configureProbe(options, self.iceInstance)
        proxy1 = p1.getProbeFacade().getSubscriptionProxy()

        options = {'probeName': 'Sonda1-2', 'plugin':'bytesReceived', 'iface':'eth0', 'timeval':'2', 'rule':['udp'], 'promisc':False, 'topicProxy':''}
        p2 = Probe()
        p2.configureProbe(options, Ice.initialize())
        proxy2 = p2.getProbeFacade().getSubscriptionProxy()

        probeProxies = [proxy1, proxy2]
        self.monitor.startPingProbes = Mock()
        #when
        self.monitor.configureMonitor(probeProxies, self.iceInstance)
        #then
        self.assertEquals(len(self.monitor.getMonitorFacade().getProbesMonitorized()), 2)
        self.assertTrue(self.monitor.startPingProbes.called)

#------------------------------------------------------------------------------ 
# Stopping Monitor
#------------------------------------------------------------------------------
    # Configuramos una nueva monitorización a una sonda y posteriormente nos desuscribimos
    def testStoppingMonitor(self):
        #given
        options = {'probeName': 'Sonda2', 'plugin':'bytesReceived', 'iface':'eth0', 'timeval':'2', 'rule':['udp'], 'promisc':False, 'topicProxy':''}
        probe = Probe()
        probe.configureProbe(options, self.iceInstance)
        proxy = probe.getProbeFacade().getSubscriptionProxy()

        probeProxy = [proxy]
        self.monitor.startPingProbes = Mock()
        self.monitor.configureMonitor(probeProxy, self.iceInstance)
        result1 = self.monitor.getMonitorFacade().getProbesMonitorized()
        #when
        self.monitor.stopMonitor()
        result2 = self.monitor.getMonitorFacade().getProbesMonitorized()
        #then 
        self.assertEquals(len(result1), 1)
        self.assertEquals(len(result2), 0)

    # Configuramos varias sondas a monitorizar. Nos subscribimos a ellas y luego nos 
    # desuscribimos 
    def testStoppingMonitorFromSeveralProbes(self):
        #given
        options = {'probeName': 'Sonda3-1', 'plugin':'bytesReceived', 'iface':'eth0', 'timeval':'2', 'rule':['udp'], 'promisc':False, 'topicProxy':''}
        p1 = Probe()
        p1.configureProbe(options, self.iceInstance)
        pProxy1 = p1.getProbeFacade().getSubscriptionProxy()

        options = {'probeName': 'Sonda3-2', 'plugin':'bytesReceived', 'iface':'eth0', 'timeval':'2', 'rule':['udp'], 'promisc':False, 'topicProxy':''}
        p2 = Probe()
        p2.configureProbe(options, Ice.initialize())
        pProxy2 = p2.getProbeFacade().getSubscriptionProxy()

        proxyList = [pProxy1, pProxy2]
        self.monitor.startPingProbes = Mock()
        self.monitor.configureMonitor(proxyList, self.iceInstance)
        result = self.monitor.getMonitorFacade().getProbesMonitorized()
        #when
        self.monitor.stopMonitor()
        #then
        self.assertEquals(len(result), 2)



