#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

from src.dindesConf import slices
from src.Exceptions import UnknownPluginMethodNameException, PluginClassNotLoadedException

import Ice
Ice.loadSlice(slices['dindesSliceLocal'])
import DInDeS

from unittest import TestCase
from mock import Mock

from src.ProbeFacade import ProbeFacade
from src.Monitor import Monitor
from src.MonitoringSystem import MonitoringSystem
from src.SniffingNetwork import SniffingNetwork



#===============================================================================
# MockMonitor class
#===============================================================================
class MockMonitor(Monitor):
    alert = Mock()
    timevalInformation = Mock()



#===============================================================================
# ProbeFacadeTests class 
#===============================================================================
class ProbeFacadeTests(TestCase):
    def setUp(self):
        self.alert = DInDeS.AlertRule()
        self.data = DInDeS.TimevalData()
        self.iceInstance = Ice.initialize()
        self.pFacade = ProbeFacade()

    def tearDown(self):
        pass

#------------------------------------------------------------------------------ 
# Probe subscription manager
#------------------------------------------------------------------------------
    # Se configura la sonda y se carga el método deseado. Se crea el proxy para
    # que los monitores se puedan subscribir. Se comprueba que se crean.
    def testCreateSubscriptionProxy(self):
        #given
        options = {'probeName': 'Sonda1', 'plugin':'bytesTransmitted', 'iface':'wlan0', 'timeval':'2', 'rule':['tcp'], 'promisc':False, 'topicProxy':''}
        self.pFacade.setInputArguments(options, self.iceInstance)
        self.pFacade.loadClass()
        #when 
        resProxy1 = self.pFacade.getSubscriptionManager().getSubscriptionProxy()
        self.pFacade.createProxyEndpoints()
        resProxy2 = self.pFacade.getSubscriptionManager().getSubscriptionProxy()
        #then
        self.assertEquals(resProxy1, None)
        self.assertNotEquals(resProxy2, None)

    # Se configura la sonda y se carga el método deseado. Se crea el topic manager
    # para que la sonda pueda mandar los datos. Se comprueba que se crea.
    def testCreateTopicManager(self):
        #given
        topicName = 'topicProbeTest1'
        options = {'probeName': 'Sonda2', 'plugin':'bytesTransmitted', 'iface':'wlan0', 'timeval':'2', 'rule':['tcp'], 'promisc':False, 'topicProxy':''}
        self.pFacade.setInputArguments(options, self.iceInstance)
        #when
        resTopic1 = self.pFacade.getSubscriptionManager().getTopic()
        self.pFacade.createTopicManager(topicName)
        resTopic2 = self.pFacade.getSubscriptionManager().getTopic()
        #then
        self.assertEquals(resTopic1, None)
        self.assertNotEquals(resTopic2, None)

    # Se configura la sonda y se carga el método deseado. Se dos topic managers
    # con el mismo nombre. Se comprueba que primero se crea uno, y el segundo se 
    # recupera el primero que se ha creado.
    def testCreateTopicManagerTwoTimesWithSameName(self):
        #given
        topicName = 'topicProbeTest2'
        options = {'probeName': 'Sonda3', 'plugin':'bytesTransmitted', 'iface':'wlan0', 'timeval':'2', 'rule':['tcp'], 'promisc':False, 'topicProxy':''}
        self.pFacade.setInputArguments(options, self.iceInstance)
        self.pFacade.createTopicManager(topicName)
        resTopic1 = self.pFacade.getSubscriptionManager().getTopic()
        #when
        self.pFacade.createTopicManager(topicName)
        resTopic2 = self.pFacade.getSubscriptionManager().getTopic()
        #then
        self.assertNotEquals(resTopic1, None)
        self.assertEquals(resTopic2, resTopic1)

    # Se configura la sonda y se comprueba que se manda una AlertRule rota a los 
    # métodos de alerta.
    def testSendAlert(self):
        #given
        topicName = 'sendAlertTopic'
        servant = MockMonitor()
        options = {'probeName': 'Sonda4', 'plugin':'bytesReceived', 'iface':'eth0', 'timeval':'2', 'rule':['udp'], 'promisc':False, 'topicProxy':''}
        self.pFacade.setInputArguments(options, self.iceInstance)
        self.pFacade.createTopicManager(topicName)
        topic = self.pFacade.getSubscriptionManager().getTopic()
        topic.subscribeAndGetPublisher = Mock()
        self.pFacade.getSubscriptionManager().getPublisher().alert = Mock(side_effect=servant.alert)
        #when
        self.pFacade.getSubscriptionManager().sendAlert(self.alert)
        #then
        self.assertTrue(servant.alert.called)

    # Se configura la sonda y se comprueba que se manda una información temporal
    # a los métodos de información.
    def testSendInformation(self):
        #given
        topicName = 'sendNotificationTopic'
        servant = MockMonitor()
        options = {'probeName': 'Sonda5', 'plugin':'bytesReceived', 'iface':'eth0', 'timeval':'2', 'rule':['udp'], 'promisc':False, 'topicProxy':''}
        self.pFacade.setInputArguments(options, self.iceInstance)
        self.pFacade.createTopicManager(topicName)
        topic = self.pFacade.getSubscriptionManager().getTopic()
        topic.subscribeAndGetPublisher = Mock()
        self.pFacade.getSubscriptionManager().getPublisher().timevalInformation = Mock(side_effect=servant.timevalInformation)
        #when
        self.pFacade.getSubscriptionManager().sendInformation(self.data)
        #then
        self.assertTrue(servant.timevalInformation.called)

    # Subscribe un monitor a una sonda y posteriormente la desubscribimos correctamente
    def testUnsubscribeMonitorFromAProbe(self):
        #given
        topicName = 'unsubscribeTopic'
        servant = MockMonitor()
        options = {'probeName': 'Sonda6', 'plugin':'bytesReceived', 'iface':'eth0', 'timeval':'2', 'rule':['udp'], 'promisc':False, 'topicProxy':''}
        self.pFacade.setInputArguments(options, self.iceInstance)
        self.pFacade.createTopicManager(topicName)
        t = self.pFacade.getSubscriptionManager().getTopic()
        t.subscribeAndGetPublisher = Mock()
        self.pFacade.subscribeMonitor(servant)
        #when
        t.unsubscribe = Mock()
        self.pFacade.unsubscribeMonitor(servant)
        #then
        self.assertTrue(t.subscribeAndGetPublisher.called)
        self.assertTrue(t.unsubscribe.called)

#------------------------------------------------------------------------------ 
# Probe plugin manager, load class
#------------------------------------------------------------------------------
    # Inicializamos la sonda indicándole una serie de argumentos correctamente.
    # Comprobamos que se carga el plugin.
    def testLoadSniffingPlugin(self):
        #given 
        options = {'probeName': 'Sonda7', 'plugin':'sniffNetwork', 'iface':'wlan0', 'timeval':'123', 'rule':[], 'promisc':False, 'topicProxy':''}
        self.pFacade.setInputArguments(options, self.iceInstance)
        #when
        result1 = self.pFacade.getPluginManager().getClassSelected()
        self.pFacade.getPluginManager().loadClass()
        #then
        result2 = self.pFacade.getPluginManager().getClassSelected()
        self.assertEquals(result1, None)
        self.assertTrue(isinstance(result2, SniffingNetwork))

    # Inicializamos la sonda con el nombre del método a cargar erróneo. Se lanza
    # un error y se comprueba que no se ha cargado nada.
    def testLoadPluginWithError(self):
        #given
        options = {'probeName': 'Sonda8', 'plugin':'bla', 'iface':'wlan0', 'timeval':'2', 'rule':[], 'promisc':False, 'topicProxy':''}
        self.pFacade.setInputArguments(options, self.iceInstance)
        #when
        result1 = self.pFacade.getPluginManager().getClassSelected()
        methodFail = self.pFacade.loadClass
        #then
        self.assertRaises(UnknownPluginMethodNameException, methodFail)
        result2 = self.pFacade.getPluginManager().getClassSelected()
        self.assertEquals(result1, None)
        self.assertEquals(result2, None)

#------------------------------------------------------------------------------ 
    # Inicializamos la sonda con los argumentos correctos, y queriendo cargar el 
    # método bytesTransmitted del plugin MonitoringSystem. Comprobamos que se carga 
    # correctamente.
    def testLoadBytesTransmitted(self):
        #given
        options = {'probeName': 'Sonda9', 'plugin':'bytesTransmitted', 'iface':'wlan0', 'timeval':'432', 'rule':[], 'promisc':False, 'topicProxy':''}
        self.pFacade.setInputArguments(options, self.iceInstance)
        #when
        result1 = self.pFacade.getPluginManager().getClassSelected()
        self.pFacade.loadClass()
        #then
        result2 = self.pFacade.getPluginManager().getClassSelected()
        self.assertEqual(result1, None)
        self.assertTrue(isinstance(result2, MonitoringSystem))

    # Inicializamos la sonda con los argumentos correctos, y queriendo cargar el 
    # método bytesReceived del plugin MonitoringSystem. Comprobamos que se carga 
    # correctamente.
    def testLoadBytesReceived(self):
        #given
        options = {'probeName': 'Sonda10', 'plugin':'bytesReceived', 'iface':'wlan0', 'timeval':'432', 'rule':[], 'promisc':False, 'topicProxy':''}
        self.pFacade.setInputArguments(options, self.iceInstance)
        #when
        result1 = self.pFacade.getPluginManager().getClassSelected()
        self.pFacade.loadClass()
        #then
        result2 = self.pFacade.getPluginManager().getClassSelected()
        self.assertEqual(result1, None)
        self.assertTrue(isinstance(result2, MonitoringSystem))

#------------------------------------------------------------------------------ 
    # Inicializamos la sonda con los argumentos correctos, y queriendo cargar el 
    # método packetsTransmitted del plugin MonitoringSystem. Comprobamos que se carga 
    # correctamente.
    def testLoadPacketsTransmitted(self):
        #given
        options = {'probeName': 'Sonda11', 'plugin':'packetsTransmitted', 'iface':'wlan0', 'timeval':'432', 'rule':[], 'promisc':False, 'topicProxy':''}
        self.pFacade.setInputArguments(options, self.iceInstance)
        #when
        result1 = self.pFacade.getPluginManager().getClassSelected()
        self.pFacade.loadClass()
        #then
        result2 = self.pFacade.getPluginManager().getClassSelected()
        self.assertEqual(result1, None)
        self.assertTrue(isinstance(result2, MonitoringSystem))

    # Inicializamos la sonda con los argumentos correctos, y queriendo cargar el 
    # método packetsReceived del plugin MonitoringSystem. Comprobamos que se carga 
    # correctamente.
    def testLoadPacketsReceived(self):
        #given
        options = {'probeName': 'Sonda12', 'plugin':'packetsReceived', 'iface':'wlan0', 'timeval':'432', 'rule':[], 'promisc':False, 'topicProxy':''}
        self.pFacade.setInputArguments(options, self.iceInstance)
        #when
        result1 = self.pFacade.getPluginManager().getClassSelected()
        self.pFacade.loadClass()
        #then
        result2 = self.pFacade.getPluginManager().getClassSelected()
        self.assertEqual(result1, None)
        self.assertTrue(isinstance(result2, MonitoringSystem))

#------------------------------------------------------------------------------ 
    # Inicializamos la sonda con los argumentos correctos, y queriendo cargar el 
    # método errorsTransmitted del plugin MonitoringSystem. Comprobamos que se carga 
    # correctamente.
    def testLoadErrorsTransmitted(self):
        #given
        options = {'probeName': 'Sonda13', 'plugin':'errorsTransmitted', 'iface':'wlan0', 'timeval':'432', 'rule':[], 'promisc':False, 'topicProxy':''}
        self.pFacade.setInputArguments(options, self.iceInstance)
        #when
        result1 = self.pFacade.getPluginManager().getClassSelected()
        self.pFacade.loadClass()
        #then
        result2 = self.pFacade.getPluginManager().getClassSelected()
        self.assertEqual(result1, None)
        self.assertTrue(isinstance(result2, MonitoringSystem))

    # Inicializamos la sonda con los argumentos correctos, y queriendo cargar el 
    # método errorsReceived del plugin MonitoringSystem. Comprobamos que se carga 
    # correctamente.
    def testLoadErrorsReceived(self):
        #given
        options = {'probeName': 'Sonda14', 'plugin':'errorsReceived', 'iface':'wlan0', 'timeval':'432', 'rule':[], 'promisc':False, 'topicProxy':''}
        self.pFacade.setInputArguments(options, self.iceInstance)
        #when
        result1 = self.pFacade.getPluginManager().getClassSelected()
        self.pFacade.loadClass()
        #then
        result2 = self.pFacade.getPluginManager().getClassSelected()
        self.assertEqual(result1, None)
        self.assertTrue(isinstance(result2, MonitoringSystem))

#------------------------------------------------------------------------------ 
    # Inicializamos la sonda con los argumentos correctos, y queriendo cargar el 
    # método multicastTransmitted del plugin MonitoringSystem. Comprobamos que se carga 
    # correctamente.
    def testLoadMulticastTransmitted(self):
        #given
        options = {'probeName': 'Sonda15', 'plugin':'multicastTransmitted', 'iface':'wlan0', 'timeval':'432', 'rule':[], 'promisc':False, 'topicProxy':''}
        self.pFacade.setInputArguments(options, self.iceInstance)
        #when
        result1 = self.pFacade.getPluginManager().getClassSelected()
        self.pFacade.loadClass()
        #then
        result2 = self.pFacade.getPluginManager().getClassSelected()
        self.assertEqual(result1, None)
        self.assertTrue(isinstance(result2, MonitoringSystem))

    # Inicializamos la sonda con los argumentos correctos, y queriendo cargar el 
    # método multicastReceived del plugin MonitoringSystem. Comprobamos que se carga 
    # correctamente.
    def testLoadMulticastReceived(self):
        #given
        options = {'probeName': 'Sonda16', 'plugin':'multicastReceived', 'iface':'wlan0', 'timeval':'432', 'rule':[], 'promisc':False, 'topicProxy':''}
        self.pFacade.setInputArguments(options, self.iceInstance)
        #when
        result1 = self.pFacade.getPluginManager().getClassSelected()
        self.pFacade.loadClass()
        #then
        result2 = self.pFacade.getPluginManager().getClassSelected()
        self.assertEqual(result1, None)
        self.assertTrue(isinstance(result2, MonitoringSystem))
#------------------------------------------------------------------------------ 
    # Inicializamos la sonda con los argumentos correctos, y queriendo cargar el 
    # método broadcastTransmitted del plugin MonitoringSystem. Comprobamos que se carga 
    # correctamente.
    def testLoadBroadcastTransmitted(self):
        #given
        options = {'probeName': 'Sonda17', 'plugin':'broadcastTransmitted', 'iface':'wlan0', 'timeval':'432', 'rule':[], 'promisc':False, 'topicProxy':''}
        self.pFacade.setInputArguments(options, self.iceInstance)
        #when
        result1 = self.pFacade.getPluginManager().getClassSelected()
        self.pFacade.loadClass()
        #then
        result2 = self.pFacade.getPluginManager().getClassSelected()
        self.assertEqual(result1, None)
        self.assertTrue(isinstance(result2, MonitoringSystem))

    # Inicializamos la sonda con los argumentos correctos, y queriendo cargar el 
    # método broadcastReceived del plugin MonitoringSystem. Comprobamos que se carga 
    # correctamente.
    def testLoadBroadcastReceived(self):
        #given
        options = {'probeName': 'Sonda18', 'plugin':'broadcastReceived', 'iface':'wlan0', 'timeval':'432', 'rule':[], 'promisc':False, 'topicProxy':''}
        self.pFacade.setInputArguments(options, self.iceInstance)
        #when
        result1 = self.pFacade.getPluginManager().getClassSelected()
        self.pFacade.loadClass()
        #then
        result2 = self.pFacade.getPluginManager().getClassSelected()
        self.assertEqual(result1, None)
        self.assertTrue(isinstance(result2, MonitoringSystem))

#------------------------------------------------------------------------------ 
# Probe plugin manager, load methods
#------------------------------------------------------------------------------
    # Le indicamos los argumentos con los que queremos configurar la sonda, y comprobamos
    # que se ha cargado correctamente el método (plugin sniffNetwork) que queremos 
    # ejecutar.
    def testLoadSniffingMethod(self):
        #given
        options = {'probeName': 'Sonda19', 'plugin':'sniffNetwork', 'iface':'wlan0', 'timeval':'123', 'rule':[], 'promisc':False, 'topicProxy':''}
        self.pFacade.setInputArguments(options, self.iceInstance)
        self.pFacade.loadClass()
        #when
        result1 = self.pFacade.getPluginManager().getClassSelected().getMethodLoaded()
        self.pFacade.getPluginManager().getClassSelected().loadMethod(options['plugin'])
        result2 = self.pFacade.getPluginManager().getClassSelected().getMethodLoaded()
        #then
        self.assertEquals(result1, None)
        self.assertEquals(result2.__name__, '_' + options['plugin'])

    # Le indicamos los argumentos para configurar la sonda y se comprueba que se
    # ha cargado el método correcto (del plugin MonitoringSystem) que queremos 
    # ejecutar.
    def testLoadMonitoringMethod(self):
        #given
        options = {'probeName': 'Sonda20', 'plugin':'packetsReceived', 'iface':'wlan0', 'timeval':'60', 'rule':[], 'promisc':False, 'topicProxy':''}
        self.pFacade.setInputArguments(options, self.iceInstance)
        self.pFacade.loadClass()
        #when
        result1 = self.pFacade.getPluginManager().getClassSelected().getMethodLoaded()
        self.pFacade.getPluginManager().getClassSelected().loadMethod(options['plugin'])
        result2 = self.pFacade.getPluginManager().getClassSelected().getMethodLoaded()
        #then
        self.assertEquals(result1, None)
        self.assertEquals(result2.__name__, '_' + options['plugin'])

#------------------------------------------------------------------------------ 
# Probe plugin manager, executeMethods
#------------------------------------------------------------------------------
    # Configuramos la sonda con los argumentos correctos y comprobamos si se ejecuta
    # el método a lanzar (plugin SniffingNetwork). Ejecución con espera (pulsar enter)
    def testExecuteSniffingLoadedMethod(self):
        #given
        options = {'probeName': 'Sonda21', 'plugin':'sniffNetwork', 'iface':'wlan0', 'timeval':'2', 'rule':['icmp'], 'promisc':False, 'topicProxy':''}
        self.pFacade.setInputArguments(options, self.iceInstance)
        self.pFacade.loadClass()
        #when
        self.pFacade.getPluginManager().getClassSelected().startToRun = Mock()
        self.pFacade.executeMethod()
        #then
        self.assertTrue(self.pFacade.getPluginManager().getClassSelected().startToRun.called)

    # Configuramos la sonda con los argumentos correctos y comprobamos que se ejecuta
    # el método a lanzar (plugin MonitoringSystem). Ejecución con espera (pulsar enter)
    def testExecuteMonitoringLoadedMethod(self):
        #given
        options = {'probeName': 'Sonda22', 'plugin':'bytesTransmitted', 'iface':'wlan0', 'timeval':'2', 'rule':['1000'], 'promisc':False, 'topicProxy':''}
        self.pFacade.setInputArguments(options, self.iceInstance)
        self.pFacade.loadClass()
        #when
        self.pFacade.getPluginManager().getClassSelected().startToRun = Mock()
        self.pFacade.executeMethod()
        #then
        self.assertTrue(self.pFacade.getPluginManager().getClassSelected().startToRun.called)

    # Se configura la sonda correctamente, pero se intenta ejecutar el método deseado
    # sin cargarlo previamente. Se lanza un error
    def testTryExecuteSniffingWithoutLoadMethods(self):
        #given
        options = {'probeName': 'Sonda23', 'plugin':'bytesTransmitted', 'iface':'wlan0', 'timeval':'2', 'rule':['icmp'], 'promisc':False, 'topicProxy':''}
        self.pFacade.setInputArguments(options, self.iceInstance)
        #when
        methodFail = self.pFacade.executeMethod
        #then
        self.assertRaises(PluginClassNotLoadedException, methodFail)
        self.assertEquals(self.pFacade.getPluginManager().getClassSelected(), None)



