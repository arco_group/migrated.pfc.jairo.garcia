#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

from src.dindesConf import testsFiles
from src.Exceptions import ErrorCreatingRuleException

from unittest import TestCase
from mock import Mock

from src.MonitoringSystem import MonitoringSystem



#===============================================================================
# MonitoringSystemTests class
#===============================================================================
class MonitoringSystemTests(TestCase):
    def setUp(self):
        self.devFileFake = testsFiles['devFile']
        self.netstatFileFake = testsFiles['netstatFile']
        self.alertMethod = Mock()
        self.notifMethod = Mock()
        self.plug = MonitoringSystem(self.alertMethod, self.notifMethod)

    def tearDown(self):
        pass

#------------------------------------------------------------------------------ 
# Methods
#------------------------------------------------------------------------------ 
# Load monitoring method
#------------------------------------------------------------------------------
    # Dado el diccionario con los argumentos pasados por la sonda, cargamos el 
    # método deseado.
    def testLoadSniffMethod(self):
        #given
        method = 'errorsReceived'
        options = {'plugin' : method}
        result1 = self.plug.getMethodLoaded()
        #when
        self.plug.configure(**options)
        #then
        self.assertEquals(result1, None)
        self.assertEquals(self.plug.getMethodLoaded().__name__, '_' + method)

#------------------------------------------------------------------------------ 
# Selecting network iface
#------------------------------------------------------------------------------
    # Se pasa el diccionario con las opciones para configurar el plugin. Se pasa un
    # nombre de interfaz correcto, y se comprueba que es correcto
    def testSelectingNetworkIface(self):
        #given
        options = {'plugin':'multicastTransmitted', 'iface':'wlan0'}
        result1 = self.plug.getNetworkIface()
        #when
        self.plug.configure(**options)
        #then
        self.assertEquals(result1, '')
        self.assertEquals(self.plug.getNetworkIface(), 'wlan0')

#------------------------------------------------------------------------------ 
# Selecting Alert Rule
#------------------------------------------------------------------------------
    # Se configura el plugin de monitoreo con una regla de alerta. En este caso
    # las reglas de alertas serán el número máximo de datos que capture en el monitoreo.
    # Se establece correctamente.
    def testSettingAlertRule(self):
        #given
        options = {'plugin':'bytesTransmitted', 'iface':'wlan0', 'timeval':10, 'rule':['123123']}
        #when
        self.plug.configure(**options)
        #then
        self.assertEquals(self.plug.getAlertRule(), 123123)

    # Se quiere establecer 2 reglas de alerta para el plugin. Pero sólo se tiene
    # en cuenta la primera que se introduzca. Se establece correctamente.
    def testSettingSeveralAlertRules(self):
        #given
        options = {'plugin':'packetsTransmitted', 'iface':'lo', 'timeval':10, 'rule':['123000', '345432']}
        #when
        self.plug.configure(**options)
        #then 
        self.assertEquals(self.plug.getAlertRule(), 123000)

    # Se quiere establecer una regla de alerta incorrecta. Se lanza un error a la
    # hora de crear la regla. Se mantiene el valor -1 inicial
    def testSettingAlertRuleException(self):
        #given
        options = {'plugin':'bytesTransmitted', 'iface':'wlan0', 'timeval':10, 'rule':['1231asd23']}
        #when
        methodFail = self.plug.configure
        #then
        self.assertRaises(ErrorCreatingRuleException, methodFail, **options)
        self.assertEquals(self.plug.getAlertRule(), -1)

#------------------------------------------------------------------------------ 
# Methods
#------------------------------------------------------------------------------ 
    # Se prueba que el método de captura de bytes transmitidos obtiene valores,
    # en el fichero correcto.
    def testGetBytesTransmittedWithNonFakeFile(self):
        #given
        options = {'plugin':'bytesTransmitted', 'iface':'lo', 'timeval':10}
        self.plug.configure(**options)
        methodLoaded = self.plug.getMethodLoaded()
        self.plug._execute = Mock(side_effect=(methodLoaded))
        #when
        result = self.plug._execute()
        #then
        self.assert_(result > 0)

    # Se prueba que se capture los valores correctos de bytes transmitidos, de un 
    # fichero fake, con los valores pactados. El resultado es el correcto.
    def testGetBytesTransmitted(self):
        #given
        options = {'plugin':'bytesTransmitted', 'iface':'wlan0', 'timeval':10}
        self.plug.configure(**options)
        self.plug.setFile(self.devFileFake)
        methodLoaded = self.plug.getMethodLoaded()
        self.plug._execute = Mock(side_effect=(methodLoaded))
        #when
        result = self.plug._execute()
        #then
        self.assertEqual(result, 10101)

    # Se prueba que se capture los valores correctos de bytes recibidos, de un 
    # fichero fake, con los valores pactados. El resultado de ejecución es el 
    # correcto
    def testGetBytesReceived(self):
        #given
        options = {'plugin':'bytesReceived', 'iface':'wlan0', 'timeval':10}
        self.plug.configure(**options)
        self.plug.setFile(self.devFileFake)
        methodLoaded = self.plug.getMethodLoaded()
        self.plug._execute = Mock(side_effect=(methodLoaded))
        #when
        result = self.plug._execute()
        #then
        self.assertEqual(result, 12345)

#------------------------------------------------------------------------------ 
    # Se prueba que se capture los valores correctos de paquetes transmitidos, 
    # de un fichero fake, con los valores pactados. El resultado es el correcto.
    def testGetPacketsTransmitted(self):
        #given
        options = {'plugin':'packetsTransmitted', 'iface':'lo', 'timeval':10}
        self.plug.configure(**options)
        self.plug.setFile(self.devFileFake)
        methodLoaded = self.plug.getMethodLoaded()
        self.plug._execute = Mock(side_effect=(methodLoaded))
        #when
        result = self.plug._execute()
        #then
        self.assertEqual(result, 645)

    # Se prueba que se capture los valores correctos que paquetes recibidos, de
    # un fichero fake, con los valores pactados. El resultado es el correcto.
    def testGetPacketsReceived(self):
        #given
        options = {'plugin':'packetsReceived', 'iface':'lo', 'timeval':10}
        self.plug.configure(**options)
        self.plug.setFile(self.devFileFake)
        methodLoaded = self.plug.getMethodLoaded()
        self.plug._execute = Mock(side_effect=(methodLoaded))
        #when
        result = self.plug._execute()
        #then
        self.assertEqual(result, 345)

#------------------------------------------------------------------------------ 
    # Se prueba que se capture los valores correctos de errores transmitidos, 
    # de un fichero fake, con los valores pactados. El resultado es el correcto.
    def testGetErrorsTransmitted(self):
        #given
        options = {'plugin':'errorsTransmitted', 'iface':'eth0', 'timeval':10}
        self.plug.configure(**options)
        self.plug.setFile(self.devFileFake)
        methodLoaded = self.plug.getMethodLoaded()
        self.plug._execute = Mock(side_effect=(methodLoaded))
        #when
        result = self.plug._execute()
        #then
        self.assertEqual(result, 2)

    # Se prueba que se capture los valores correctos de errores recibidos, 
    # de un fichero fake, con los valores pactados. El resultado es el correcto.
    def testGetErrorsReceived(self):
        #given
        options = {'plugin':'errorsReceived', 'iface':'eth0', 'timeval':10}
        self.plug.configure(**options)
        self.plug.setFile(self.devFileFake)
        methodLoaded = self.plug.getMethodLoaded()
        self.plug._execute = Mock(side_effect=(methodLoaded))
        #when
        result = self.plug._execute()
        #then
        self.assertEqual(result, 6)

#------------------------------------------------------------------------------ 
    # Se prueba que se capture los valores correctos de paquetes multicast transmitidos, 
    # de un fichero fake, con los valores pactados. El resultado es el correcto.
    def testGetMulticastTransmitted(self):
        #given
        options = {'plugin':'multicastTransmitted', 'iface':'wlan0', 'timeval':10}
        self.plug.configure(**options)
        self.plug.setFile(self.netstatFileFake)
        methodLoaded = self.plug.getMethodLoaded()
        self.plug._execute = Mock(side_effect=(methodLoaded))
        #when
        result = self.plug._execute()
        #then
        self.assertEqual(result, 20)

    # Se prueba que se capture los valores correctos de paquetes multicast recibidos, 
    # de un fichero fake, con los valores pactados. El resultado es el correcto.
    def testGetMulticastReceived(self):
        #given
        options = {'plugin':'multicastReceived', 'iface':'wlan0', 'timeval':10}
        self.plug.configure(**options)
        self.plug.setFile(self.netstatFileFake)
        methodLoaded = self.plug.getMethodLoaded()
        self.plug._execute = Mock(side_effect=(methodLoaded))
        #when
        result = self.plug._execute()
        #then
        self.assertEqual(result, 10)

#------------------------------------------------------------------------------ 
    # Se prueba que se capture los valores correctos de paquetes broadcast transmitidos, 
    # de un fichero fake, con los valores pactados. El resultado es el correcto. 
    def testGetBroadcastTransmitted(self):
        #given
        options = {'plugin':'broadcastTransmitted', 'iface':'eth0', 'timeval':10}
        self.plug.configure(**options)
        self.plug.setFile(self.netstatFileFake)
        methodLoaded = self.plug.getMethodLoaded()
        self.plug._execute = Mock(side_effect=(methodLoaded))
        #when
        result = self.plug._execute()
        #then
        self.assertEqual(result, 200)

    # Se prueba que se capture los valores correctos de paquetes broadcast recibidos, 
    # de un fichero fake, con los valores pactados. El resultado es el correcto.
    def testGetBroadcastReceived(self):
        #given
        options = {'plugin':'broadcastReceived', 'iface':'eth0', 'timeval':10}
        self.plug.configure(**options)
        self.plug.setFile(self.netstatFileFake)
        methodLoaded = self.plug.getMethodLoaded()
        self.plug._execute = Mock(side_effect=(methodLoaded))
        #when
        result = self.plug._execute()
        #then
        self.assertEqual(result, 100)



