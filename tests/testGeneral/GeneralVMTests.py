#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

from src.dindesConf import slices, systemProxies

import Ice, IceGrid
Ice.loadSlice(slices['dindesSliceLocal']) #
import DInDeS

from unittest import TestCase
from threading import Event
from mock import Mock
from scapy.all import IP, ICMP, TCP, send
from time import asctime

from src.Monitor import Monitor
from src.RouterShell import RouterShell



#===============================================================================
# Configuración de las MV's
# probes = [{'id':'0', 'activation':'always' , 'node':'MailServerMantis', 'plugin':'sniffNetwork', 'iface':'eth0', 'timestamp':'10', 'rule':'icmp', 'promisc':'False'},
#           {'id':'1', 'activation':'always', 'node':'WebServerWolf', 'plugin':'packetsReceived', 'iface':'eth0', 'timestamp':'10', 'rule':'9999999', 'promisc':'False'},
#          ]
# routers = [{'id':'0', 'activation':'always', 'node':'RouterOcelot'},
#           ]
# monitor = {'activation':'manual', 'node':'Monitor'}
#===============================================================================

#===============================================================================
# MockMonitor class
#===============================================================================
class MockMonitor(Monitor):
    alert = Mock()
    timevalInformation = Mock()

#===============================================================================
# GeneralVMTests class
#=============================================================================== 
class GeneralVMTests(TestCase):
    def setUp(self):
        self.ice = Ice.initialize(['--Ice.Config=../tests/testGeneral/testGeneral.cfg'])
        self.ocelotInstance = None
        self.ocelotProxy = None
        self.ravenInstance = None
        self.ravenProxy = None
        self.mantisInstance = None
        self.mantisIP = '192.168.2.101'
        self.wolfInstance = None
        self.wolfIP = '192.168.2.102'
        self.rexInstance = None
        self.rexIP = '192.168.4.101'
        self.monitorIP = '192.168.1.1'
        self.monitorSnake = Monitor()
        self.routerShellSnake = RouterShell()

        self.action = DInDeS.MakeAction()

    def tearDown(self):
        if self.ocelotProxy:
            self.routerShellSnake.deleteSentActions(self.ocelotProxy)
        if self.ravenProxy:
            self.routerShellSnake.deleteSentActions(self.ravenProxy)

    def getElementProxies(self, elementType): # '::DInDeS::ProbeQuery'
        #systemProxies = IceGrid/Query:default -h 127.0.0.1 -p 20001
        try:
            p = self.ice.stringToProxy(systemProxies['icegridQuery'])
            query = IceGrid.QueryPrx.checkedCast(p)
            proxies = query.findAllObjectsByType(elementType)
            #print 'PROXIES: ', proxies
        except Exception, e:
            print 'Error in getProbeProxies: %s' % e
        return proxies

    def getElementById(self, elementId):
        #ocelotProxy = self.getElementById('router1')
        try:
            p = self.ice.stringToProxy(systemProxies['icegridQuery'])
            query = IceGrid.QueryPrx.checkedCast(p)
            proxy = query.findObjectById(self.ice.stringToIdentity(elementId))
            #print 'PROXY: ', proxy
        except Exception, e:
            print 'Error in getElementById: %s' % e
        return proxy

    def getRouterInstance(self, routerProxy):
        return DInDeS.RouterQueryPrx.checkedCast(routerProxy)

    def getProbeInstance(self, probeProxy):
        return DInDeS.ProbeQueryPrx.checkedCast(probeProxy)

    def subscribeToProbe(self, probeProxy):
        mFacade = self.monitorSnake.getMonitorFacade()
        mFacade.subscribeMonitorMethodsOnProbe(self.monitorSnake, probeProxy, self.ice)
        mFacade.createDataBase()
        mFacade.createLogFiles()
        mFacade.createGraphs()

    def sendIcmpPacket(self, srcdir, dstdir):
        packet = IP(src=srcdir, dst=dstdir) / ICMP()
        send (packet)

    def sendWebPacket(self, srcdir, dstdir):
        packet = IP(src=srcdir, dst=dstdir) / TCP(dport=80)
        send (packet)



#==============================================================================
# testStage1: se quiere comprobar que se establece una acción en el router 
# para evitar que se rompa una regla otra vez
#==============================================================================
    def testStage1(self):
        #----------------------------- given ----------------------------------
        self.monitorSnake.alert = Mock()
        event = Event()
        waitTime = 0
        print '\nStarting stage1'
        #1.- encender router ocelot
        print '1.- Getting ocelot router instance...',
        self.ocelotProxy = self.getElementById('RouterOcelot')
        self.routerOcelot = self.getRouterInstance(self.ocelotProxy)
        print 'Done!'

        #2.- encender mailserver mantis
        print '2.- Getting mailserver probe instance...',
        mantisProxy = self.getElementById('MailServerMantis')
        self.mantisInstance = self.getProbeInstance(mantisProxy)
        print 'Done!'

        #3.- se manda un ping al router ocelot para ver si está ok 
        print '3.- Running ping() method on ocelot router...',
        self.routerOcelot.ping()
        print 'Done!'

        #4.- se manda un ping al mailserver mantis para ver si está ok
        print '4.- Running ping() method on mailserver probe...',
        self.mantisInstance.ping()
        print 'Done!'

        #5.- nos subscribimos a la sonda para que nos informe lo que vaya sucediendo
        print '5.- Subscribing to mailserver probe...',
        self.subscribeToProbe(mantisProxy)
        print 'Done!'

        #----------------------------- when -----------------------------------
        #1.- se recibe un paquete que rompe una regla. Se crea una alerta
        # Para ello, se manda un paquete que lo rompa mediante scapy
        result1 = self.monitorSnake.getMonitorFacade().getNumberOfAlerts(mantisProxy)
        print '1.- Sending a icmp packet to mailserver probe...',
        self.sendIcmpPacket(self.monitorIP, self.mantisIP)
        print 'Done!'

        #----------------------------- then -----------------------------------
        #1.- se comprueba que se ha producido una alerta en la sonda
        result2 = self.monitorSnake.getMonitorFacade().getNumberOfAlerts(mantisProxy)
        print '1.- Checking if there is an alert message on mailserver probe...',
        self.assertEquals(result1, 0)
        while result2 == 0:
            result2 = self.monitorSnake.getMonitorFacade().getNumberOfAlerts(mantisProxy)
            waitTime += 2
            print "Waiting..."
            event.wait(2)
        self.assertTrue(result1 < result2)

        #2.- mailserver mantis crea una alerta y la manda al monitorSnake
        print '2.- Checking if mailserver mantis is on alert state...',
        result = self.mantisInstance.ping() # for checking alertState=true
        while not result.alertState:
            result = self.mantisInstance.ping()
            print "Waiting..."
            event.wait(2)
        self.assertTrue(result.alertState)
        print 'Done!'

        #3.- monitorSnake recibe la alerta.
        print '3.- Receiving alert from mailserver probe...',
        self.assertTrue(self.monitorSnake.alert.called)
        print 'Done!'

        #4.- routershellSnake manda una acción al router ocelot para evitar que 
        # produzca otra alerta
        print '4.- Routershell snake send an action to ocelot router to avoid another equal new alert...',
        self.action.transportProto = DInDeS.TransportProtocol.icmp
        self.action.direction = DInDeS.PacketDirection.dst
        self.action.ipAddress = self.mantisIP
        self.action.port = 0
        self.action.typeAction = DInDeS.Action.reject
        self.action.timestamp = asctime()
        self.routerShellSnake.addActionToSend(self.action)
        self.routerShellSnake.sendActions(self.ocelotProxy)
        print 'Done!'

        #5.- router ocelot recives an router action from routershell snake
        print '5.- Router ocelot recives an Router Action...',
        actionslist = self.routerShellSnake.getSentActions(self.ocelotProxy)
        event.wait(2)
        self.assertEquals(len(actionslist), 1)
        print 'Done!'

        #6.- se manda el mismo paquete al mailserver mantis. Llega al router pero no
        # a la sonda
        result1 = self.monitorSnake.getMonitorFacade().getNumberOfAlerts(mantisProxy)
        print '6.- Sending another icmp packet to mailserver to check that firewall works...',
        event.wait(waitTime)
        self.sendIcmpPacket(self.monitorIP, self.mantisIP)
        result2 = self.monitorSnake.getMonitorFacade().getNumberOfAlerts(mantisProxy)
        self.assertEquals(result1, result2)
        print 'Done!'



#===============================================================================
# stage2: se quiere controlar que el servidor web no reciba más trafico que lo
# especificado en la regla de alerta y así controlar el correcto funcionamiento
#===============================================================================
    def testStage2(self):
        #----------------------------- given ----------------------------------
        self.monitorSnake.alert = Mock()
        event = Event()
        waitTime = 0
        print '\nStarting stage2'
        #1.- encender router ocelot
        print '1.- Getting ocelot router instance...',
        self.ocelotProxy = self.getElementById('RouterOcelot')
        self.routerOcelot = self.getRouterInstance(self.ocelotProxy)
        print 'Done!'

        #2.- encender webserver wolf
        print '2.- Getting webserver probe instance...',
        wolfProxy = self.getElementById('WebServerWolf')
        self.wolfInstance = self.getProbeInstance(wolfProxy)
        print 'Done!'

        #3.- se manda un ping al router ocelot para ver si está ok 
        print '3.- Running ping() method on ocelot router...',
        self.routerOcelot.ping()
        print 'Done!'

        #4.- se manda un ping al webserver wolf para ver si está ok
        print '4.- Running ping() method on webserver probe...',
        self.wolfInstance.ping()
        print 'Done!'

        #5.- nos subscribimos a la sonda para que nos informe lo que vaya sucediendo
        print '5.- Subscribing to webserver probe...',
        self.subscribeToProbe(wolfProxy)
        print 'Done!'

        #----------------------------- when -----------------------------------
        #1 .- se recibe un paquete que rompe una regla. Se crea una alerta
        # Para ello, se manda un paquete que lo rompa mediante scapy
        result1 = self.monitorSnake.getMonitorFacade().getNumberOfData(wolfProxy)
        print '1.- Sending 2 icmp packets to webserver probe...',
        self.sendIcmpPacket(self.monitorIP, self.wolfIP)
        self.sendIcmpPacket(self.monitorIP, self.wolfIP)
        print 'Done!'

        #----------------------------- then -----------------------------------
        #1.- se comprueba que se ha contabilizado ambos paquetes
        result2 = self.monitorSnake.getMonitorFacade().getNumberOfData(wolfProxy)
        print '1.- Checking if there are 2 more packets on webserver probe...',
        while result2 < result1 + 2:
            result2 = self.monitorSnake.getMonitorFacade().getNumberOfData(wolfProxy)
            print 'RESULT1: ', result1
            print 'RESULT2: ', result2
            print "Waiting..."
            waitTime += 2
            event.wait(2)
        self.assertTrue(result1 + 2 < result2)
        print 'Done!'

        #2.- suponemos que se ha sobrepasado el número de paquetes recibidos que
        #se han establecido en la sonda
        print '2.- We suppose that we have received more packets than we expected...'
        self.monitorSnake.alert(DInDeS.AlertRule())
        event.wait(2)

        #----------------------------- then -----------------------------------
        #1.- monitorSnake recibe la alerta.
        print '1.- Receiving alert from webserver probe...',
        self.assertTrue(self.monitorSnake.alert.called)
        print 'Done!'

        #2.- routershellSnake manda una acción al router ocelot para evitar que 
        # produzca otra alerta
        print '2.- Routershell snake send an action to ocelot router to avoid another equal new alert...',
        self.action.transportProto = DInDeS.TransportProtocol.icmp
        self.action.direction = DInDeS.PacketDirection.dst
        self.action.ipAddress = self.wolfIP
        self.action.port = 0
        self.action.typeAction = DInDeS.Action.reject
        self.action.timestamp = asctime()
        self.routerShellSnake.addActionToSend(self.action)
        self.routerShellSnake.sendActions(self.ocelotProxy)
        print 'Done!'

        #3.- router ocelot recives an router action from routershell snake
        print '3.- Router ocelot recives an Router Action...',
        actionslist = self.routerShellSnake.getSentActions(self.ocelotProxy)
        event.wait(2)
        self.assertEquals(len(actionslist), 1)
        print 'Done!'

        #4.- se manda un paquete al webserver wolf. Llega al router pero no
        # a la sonda
        result1 = self.monitorSnake.getMonitorFacade().getNumberOfData(wolfProxy)
        print '4.- Sending another packet to webserver to check that firewall works...',
        event.wait(waitTime)
        self.sendIcmpPacket(self.monitorIP, self.wolfIP)
        result2 = self.monitorSnake.getMonitorFacade().getNumberOfData(wolfProxy)
        self.assertEquals(result1, result2)
        print 'Done!'


#===============================================================================
# stage3: se quiere controlar que una estación de trabajo en la red corporativa
# no se conecte a Internet, debido a la política de seguridad implantada
#===============================================================================
    def testStage3(self):
        #----------------------------- given ----------------------------------
        self.monitorSnake.alert = Mock()
        event = Event()
        waitTime = 0
        print '\nStarting stage3'
        #1.- encender router ocelot
        print '1.- Getting ocelot router instance...',
        self.ocelotProxy = self.getElementById('RouterOcelot')
        self.routerOcelot = self.getRouterInstance(self.ocelotProxy)
        print 'Done!'

        #2.- encender router raven
        print '2.- Getting raven router instance...',
        self.ravenProxy = self.getElementById('RouterRaven')
        self.routerRaven = self.getRouterInstance(self.ravenProxy)
        print 'Done!'

        #3.- encender webserver wolf
        print '3.- Getting webserver probe instance...',
        wolfProxy = self.getElementById('WebServerWolf')
        self.wolfInstance = self.getProbeInstance(wolfProxy)
        print 'Done!'

        #4.- encender workstation rex
        print '4.- Getting webserver probe instance...',
        rexProxy = self.getElementById('WorkStationRex')
        self.rexInstance = self.getProbeInstance(rexProxy)
        print 'Done!'

        #5.- se manda un ping al router ocelot para ver si está ok 
        print '5.- Running ping() method on ocelot router...',
        self.routerOcelot.ping()
        print 'Done!'

        #6.- se manda un ping al router raven para ver si está ok 
        print '6.- Running ping() method on ocelot raven...',
        self.routerRaven.ping()
        print 'Done!'

        #7.- se manda un ping al webserver wolf para ver si está ok
        print '7.- Running ping() method on webserver probe...',
        self.wolfInstance.ping()
        print 'Done!'

        #8.- se manda un ping al workstation rex para ver si está ok
        print '8.- Running ping() method on workstation probe...',
        self.rexInstance.ping()
        print 'Done!'

        #9.- nos subscribimos a la sonda para que nos informe lo que vaya sucediendo
        print '9.- Subscribing to workstation probe...',
        self.subscribeToProbe(rexProxy)
        print 'Done!'

        #----------------------------- when -----------------------------------
        #1.- se recibe un paquete que rompe una regla. Se crea una alerta
        # Para ello, se manda un paquete que lo rompa mediante scapy
        result1 = self.monitorSnake.getMonitorFacade().getNumberOfAlerts(rexProxy)
        print '1.- Sending a web packet to workstation probe...',
        self.sendWebPacket(self.monitorIP, self.rexIP)
        print 'Done!'

        #----------------------------- then -----------------------------------
        #1.- se comprueba que se ha producido una alerta en la sonda
        result2 = self.monitorSnake.getMonitorFacade().getNumberOfAlerts(rexProxy)
        print '1.- Checking if there is an alert message on mailserver probe...',
        self.assertEquals(result1, 0)
        while result2 == 0:
            result2 = self.monitorSnake.getMonitorFacade().getNumberOfAlerts(rexProxy)
            waitTime += 2
            print "Waiting..."
            event.wait(2)
        self.assertTrue(result1 < result2)

        #2.- workstation rex crea una alerta y la manda al monitorSnake
        print '2.- Checking if workstation rex is on alert state...',
        result = self.rexInstance.ping() # for checking alertState=true
        while not result.alertState:
            result = self.rexInstance.ping()
            print "Waiting..."
            event.wait(2)
        self.assertTrue(result.alertState)
        print 'Done!'

        #3.- monitorSnake recibe la alerta.
        print '3.- Receiving alert from workstation probe...',
        self.assertTrue(self.monitorSnake.alert.called)
        print 'Done!'

        #4.- routershellSnake manda una acción al router raven para evitar que 
        # produzca otra alerta
        print '4.- Routershell snake send an action to ocelot raven to avoid another equal new alert...',
        self.action.transportProto = DInDeS.TransportProtocol.tcp
        self.action.direction = DInDeS.PacketDirection.dst
        self.action.ipAddress = self.rexIP
        self.action.port = 80
        self.action.typeAction = DInDeS.Action.reject
        self.action.timestamp = asctime()
        self.routerShellSnake.addActionToSend(self.action)
        self.routerShellSnake.sendActions(self.ravenProxy)
        print 'Done!'

        #5.- router raven recive una acción procedente del routershell snake
        print '5.- Router raven recives an Router Action...',
        actionslist = self.routerShellSnake.getSentActions(self.ravenProxy)
        event.wait(2)
        self.assertEquals(len(actionslist), 1)
        print 'Done!'

        #6.- se manda el mismo paquete al workstation rex. Llega al router pero no
        # a la sonda
        result1 = self.monitorSnake.getMonitorFacade().getNumberOfAlerts(rexProxy)
        print '6.- Sending another web packet to workstation to check that firewall works...',
        event.wait(waitTime)
        self.sendWebPacket(self.monitorIP, self.rexIP)
        result2 = self.monitorSnake.getMonitorFacade().getNumberOfAlerts(rexProxy)
        self.assertEquals(result1, result2)
        print 'Done!'

        #7.- se manda el mismo paquete al webserver wolf para comprobar que la 
        #regla establecida en el router raven no le afecta.
        result1 = self.monitorSnake.getMonitorFacade().getNumberOfData(wolfProxy)
        print '7.- Sending another web packet to workstation to check that firewall works...',
        event.wait(waitTime)
        self.sendWebPacket(self.monitorIP, self.wolfIP)
        result2 = self.monitorSnake.getMonitorFacade().getNumberOfData(wolfProxy)
        self.assertTrue(result1 <= result2)
        print 'Done!'
