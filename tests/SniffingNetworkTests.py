#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

from src.dindesConf import slices
from src.Exceptions import ErrorCreatingRuleException, RuleAlreadyExistsException

import Ice
Ice.loadSlice(slices['dindesSliceLocal'])
import DInDeS

from unittest import TestCase
from mock import Mock
from scapy.all import Ether, IP, IPv6, TCP, UDP, ICMP

from src.SniffingNetwork import SniffingNetwork



#===============================================================================
# SniffingNetworkTests class
#===============================================================================
class SniffingNetworkTests(TestCase):
    def setUp(self):
        self.alertMethod = Mock()
        self.notifMethod = Mock()
        self.plug = SniffingNetwork(self.alertMethod, self.notifMethod)

    def tearDown(self):
        pass

#------------------------------------------------------------------------------ 
# Methods
#------------------------------------------------------------------------------ 
# Load sniff method
#------------------------------------------------------------------------------
    # Dado el diccionario con los argumentos pasados por la sonda, cargamos el 
    # método deseado.
    def testLoadSniffMethod(self):
        #given
        method = 'sniffNetwork'
        options = {'plugin' : method}
        result1 = self.plug.getMethodLoaded()
        #when
        self.plug.configure(**options)
        #then
        self.assertEquals(result1, None)
        self.assertEquals(self.plug.getMethodLoaded().__name__, '_' + method)

#------------------------------------------------------------------------------ 
# Selecting network iface
#------------------------------------------------------------------------------
    # Se pasa el diccionario con las opciones para configurar el plugin. Se pasa un
    # nombre de interfaz correcto, y se comprueba que es correcto
    def testSelectingNetworkIface(self):
        #given
        options = {'plugin':'sniffNetwork', 'iface':'wlan0'}
        result1 = self.plug.getNetworkIface()
        #when
        self.plug.configure(**options)
        #then
        self.assertEquals(result1, '')
        self.assertEquals(self.plug.getNetworkIface(), 'wlan0')

#------------------------------------------------------------------------------ 
# Selecting timeval
#------------------------------------------------------------------------------
    # Se pasa el diccionario con las opciones para configura el plugin. Se indica
    # un intervalo de tiempo en segundos para informar de lo que se esté ejecutando 
    def testSelectingTimeInterval(self):
        #given
        options = {'plugin':'sniffNetwork', 'timeval':10}
        result1 = self.plug.getTimeval()
        #when
        self.plug.configure(**options)
        #then
        self.assertEquals(result1, -1)
        self.assertEquals(self.plug.getTimeval(), 10)

#------------------------------------------------------------------------------ 
# Promisc mode
#------------------------------------------------------------------------------
    # Se pasa el diccionario con los datos de configuración, indicando que se 
    # quiere establecer el modo promiscuo en la captura de paquetes
    def testSelectingTrueAsPromiscMode(self):
        #given
        options = {'plugin':'sniffNetwork', 'promisc':True}
        result1 = self.plug.getPromiscMode()
        #when
        self.plug.configure(**options)
        #then
        self.assertFalse(result1)
        self.assertTrue(self.plug.getPromiscMode())

    # Se pasa el diccionario con los datos de configuración, indicando varias veces
    # que se quiere establecer el modo promiscuo en la captura de paquetes.
    def testSelectingTrueAsPromiscModeSeveralTimes(self):
        #given
        options = {'plugin':'sniffNetwork', 'promisc':True}
        result1 = self.plug.getPromiscMode()
        self.plug.configure(**options)
        result2 = self.plug.getPromiscMode()
        #when
        self.plug.configure(**options)
        result3 = self.plug.getPromiscMode()
        #then
        self.assertFalse(result1)
        self.assertTrue(result2)
        self.assertTrue(result3)

    # Se pasa el diccionario con los datos para configuración. Se quiere establecer
    # a falso el modo promiscuo. Es falso por defecto.
    def testSelectingFalseAsPromiscMode(self):
        #given
        options = {'plugin':'sniffNetwork'}
        result1 = self.plug.getPromiscMode()
        #when
        self.plug.configure(**options)
        #then
        self.assertFalse(result1)
        self.assertFalse(self.plug.getPromiscMode())

    # Se elije primero el modo promiscuo en la captura de paquetes, pero posteriormente
    # se cambia y se pone a falso. Todos los cambios se hacen correctamente.
    def testChangingPromiscMode(self):
        #given
        options = {'plugin':'sniffNetwork', 'promisc':True}
        result1 = self.plug.getPromiscMode()
        self.plug.configure(**options)
        result2 = self.plug.getPromiscMode()
        #when
        self.plug.setPromiscMode(False)
        result3 = self.plug.getPromiscMode()
        #then
        self.assertFalse(result1)
        self.assertTrue(result2)
        self.assertFalse(result3)

#------------------------------------------------------------------------------ 
# Selecting Alert Rule
#------------------------------------------------------------------------------
    # Se quiere establecer una regla sintácticamente correcta
    def testAlertRuleWithCorrectSyntax(self):
        #given
        rule = 'tcp and port 22'
        #when
        result = self.plug.getIsSyntaxCorrect(rule)
        #then
        self.assertTrue(result)

    # Se quiere establecer una regla sintácticamente incorrecta. Se produce un
    # error
    def testAlertRuleWithIncorrectSyntax(self):
        #given
        rule = 'tcp tcp tcp'
        #when
        result = self.plug.getIsSyntaxCorrect(rule)
        #then
        self.assertFalse(result)

    # Se quiere añadir una regla sintacticamente correcta a la lista de reglas 
    # a aplicar a los paquetes recibidos. Se realiza correctamente.
    def testAddNewAlertRule(self):
        #given
        options = {'plugin':'sniffNetwork', 'rule':['udp and port 80']}
        #when
        self.plug.configure(**options)
        #then
        self.assertEquals(len(self.plug.getAlertRules()), 1)
        self.assertEquals(self.plug.getAlertRules()[0].transportProto, DInDeS.TransportProtocol.udp)
        self.assertEquals(self.plug.getAlertRules()[0].ipAddress, '')
        self.assertEquals(self.plug.getAlertRules()[0].port, 80)

    # Se quiere añadir varias reglas sintácticamente correctas a la lista de reglas
    # a aplicar a los paquetes recibidos. Se realiza correctamente.
    def testAddSeveralAlertRules(self):
        #given
        options = {'plugin':'sniffNetwork', 'rule':['udp and net 192.168.1.0', 'tcp and port 1234']}
        #when
        self.plug.configure(**options)
        #then
        self.assertEquals(len(self.plug.getAlertRules()), 2)
        self.assertEquals(self.plug.getAlertRules()[0].ipAddress, '192.168.1.0')
        self.assertEquals(self.plug.getAlertRules()[1].transportProto, DInDeS.TransportProtocol.tcp)

    # Se quiere añadir varias reglas, una correcta y otra incorrecta. A la hora 
    # de añadir la incorrecta, se produce un error. No se almacena ninguna. 
    def testAddCorrectAlertRuleAndOtherWithErrors(self):
        #given
        options = {'plugin':'sniffNetwork', 'rule':['host host', 'src host 192.3.255.12']}
        #when
        methodFail = self.plug.configure
        #then
        self.assertRaises(ErrorCreatingRuleException, methodFail, **options)
        self.assertEqual(len(self.plug.getAlertRules()), 0)

    # Se quiere añadir una regla que ya existe entre las reglas a aplicar. Se produce
    # un error, no se añade la segunda que ya existe.
    def testAddSeveralEqualAlertRules(self):
        #given
        options = {'plugin':'sniffNetwork', 'rule':['tcp', 'tcp']}
        #when
        methodFail = self.plug.configure
        #then
        self.assertRaises(RuleAlreadyExistsException, methodFail, **options)
        self.assertEqual(len(self.plug.getAlertRules()), 1)

    # Se quiere añade una regla que no se le indica el protocolo que quiere evitar.
    # Se crea y añade correctamente.
    def testAddAlertRuleWithNoProto(self):
        #given
        options = {'plugin':'sniffNetwork', 'rule':['host 123.123.123.123']}
        #when
        self.plug.configure(**options)
        #then
        self.assertEqual(len(self.plug.getAlertRules()), 1)
        self.assertEqual(self.plug.getAlertRules()[0].transportProto, DInDeS.TransportProtocol.noTransportProtocol)

#------------------------------------------------------------------------------ 
# Sniffing Network
#------------------------------------------------------------------------------
    # Se recibe un paquete que viola una regla (dst) que se ha almacenado previamente
    def testForbiddenTcpDstPacket(self):
        #given
        options = {'plugin':'sniffNetwork', 'rule':['dst host 127.0.0.1']}
        self.plug.configure(**options)
        packet = Ether() / IP(dst='127.0.0.1', src='10.0.0.1') / TCP()
        #when
        self.plug.notifyPacket(packet)
        #then
        self.assertTrue(self.alertMethod.called)

    # Se recibe un paquete que viola una regla (src) que se ha almacenado previamente
    def testForbiddenTcpSrcPacket(self):
        #given
        options = {'plugin':'sniffNetwork', 'rule':['src host 123.123.123.123']}
        self.plug.configure(**options)
        packet = Ether() / IP(dst='10.0.0.1', src='123.123.123.123') / TCP()
        #when
        self.plug.notifyPacket(packet)
        #then
        self.assertTrue(self.alertMethod.called)

    # Se recibe un paquete que viola una regla (dst or src) que se ha almacenado 
    # previamente
    def testForbiddenTcpPacketWithNoDirection(self):
        #given
        options = {'plugin':'sniffNetwork', 'rule':['host 127.0.0.1']}
        self.plug.configure(**options)
        packet = Ether() / IP(dst='127.0.0.1', src='123.123.123.123') / TCP()
        #when
        self.plug.notifyPacket(packet)
        #then
        self.assertTrue(self.alertMethod.called)

    # Se recibe un paquete que viola una regla (tcp, port) que se ha almacenado previamente
    def testForbiddenTcpPacket(self):
        #given
        options = {'plugin':'sniffNetwork', 'rule':['tcp and port 22']}
        self.plug.configure(**options)
        packet = Ether() / IP() / TCP(sport=22)
        #when
        self.plug.notifyPacket(packet)
        #then
        self.assertTrue(self.alertMethod.called)

    # Se recibe un paquete que no viola una regla (icmp) que se ha almacenado. Se comprueba
    # que no se llama al método de alerta.
    def testAllowedTcpPort(self):
        #given
        options = {'plugin':'sniffNetwork', 'rule':['icmp']}
        self.plug.configure(**options)
        packet = Ether() / IP() / TCP()
        #when
        self.plug.notifyPacket(packet)
        #then
        self.assertFalse(self.alertMethod.called)

#------------------------------------------------------------------------------ 
    # Se recibe un paquete que viola una regla (src, udp) que se ha almacenado previamente
    def testForbiddenUdpSrcPacket(self):
        #given
        options = {'plugin':'sniffNetwork', 'rule':['src host 12.12.12.32 and udp']}
        self.plug.configure(**options)
        packet = Ether() / IP(dst='10.0.0.1', src='12.12.12.32') / UDP()
        #when
        self.plug.notifyPacket(packet)
        #then
        self.assertTrue(self.alertMethod.called)

    # Se recibe un paquete que viola una regla (udp, port) que se ha almacenado previamente
    def testForbiddenUdpPacket(self):
        #given
        options = {'plugin':'sniffNetwork', 'rule':['udp and port 100']}
        self.plug.configure(**options)
        packet = Ether() / IP() / UDP(dport=100)
        #when
        self.plug.notifyPacket(packet)
        #then
        self.assertTrue(self.alertMethod.called)

#------------------------------------------------------------------------------ 
    # Se recibe un paquete que viola una regla (dst, icmp) que se ha almacenado previamente
    def testForbiddenIcmpDstPacket(self):
        #given
        options = {'plugin':'sniffNetwork', 'rule':['dst host 127.0.0.1 and icmp']}
        self.plug.configure(**options)
        packet = Ether() / IP(dst='127.0.0.1', src='12.12.12.32') / ICMP()
        #when
        self.plug.notifyPacket(packet)
        #then
        self.assertTrue(self.alertMethod.called)

    # Se recibe un paquete que viola una regla (icmp) que se ha almacenado previamente
    def testForbiddenIcmpPacket(self):
        #given
        options = {'plugin':'sniffNetwork', 'rule':['host 10.10.10.10 and icmp']}
        self.plug.configure(**options)
        packet = Ether() / IP(dst='127.0.0.1', src='10.10.10.10') / ICMP()
        #when
        self.plug.notifyPacket(packet)
        #then
        self.assertTrue(self.alertMethod.called)

#------------------------------------------------------------------------------ 
    # Se recibe un paquete que no viola una regla (ipv4, udp) que se ha almacenado previamente
    def testReceivingIpv6Packet(self):
        #given
        options = {'plugin':'sniffNetwork', 'rule':['udp and port 2000']}
        self.plug.configure(**options)
        packet = Ether() / IPv6() / UDP(sport=2000)
        #when
        self.plug.notifyPacket(packet)
        #then
        self.assertFalse(self.alertMethod.called)

    # Se recibe un paquete que viola una regla (ip6) que se ha almacenado previamente
    def testForbiddenIpv6Packet(self):
        #given
        options = {'plugin':'sniffNetwork', 'rule':['ip6']}
        self.plug.configure(**options)
        packet = Ether() / IPv6() / TCP(dport=2123)
        #when
        self.plug.notifyPacket(packet)
        #then
        self.assertTrue(self.alertMethod.called)

    # Se recibe un paquete que no viola una regla (ip6, tcp, port) que se ha almacenado previamente
    def testReceivingIpv4PacketWithIpv6AlertRule(self):
        #given
        options = {'plugin':'sniffNetwork', 'rule':['ip6 and tcp port 2000']}
        self.plug.configure(**options)
        packet = Ether() / IP() / TCP(sport=2000)
        #when
        self.plug.notifyPacket(packet)
        #then
        self.assertFalse(self.alertMethod.called)

#------------------------------------------------------------------------------ 
    # Se simula que se ha recibido un paquete que no ha violado una regla. No se 
    # llama al método de alerta y sí al método de información temporal.
    def testNotifyTimevalInformation(self):
        #given
        options = {'plugin':'sniffNetwork', 'timeval':10, 'rule':['tcp and port 3000']}
        self.plug.configure(**options)
        packet = Ether() / IP(dst='127.0.0.1', src='1.2.3.4') / TCP(sport=4000)
        self.plug.notifyPacket(packet)
        #when
        self.plug.getCheckInformation()
        #then
        self.assertFalse(self.alertMethod.called)
        self.assertEquals(self.plug.getNumberData(), 1)
        self.assertTrue(self.notifMethod.called)
        self.assertEquals(self.plug.getNextNotification(), 10)

    # Se recibe 2 paquetes, uno que viola una regla y otro que no. Se llama tanto
    # al método de alerta, como al método de información temporal.
    def testNotifyBothMethods(self):
        #given
        options = {'plugin':'sniffNetwork', 'timeval':10, 'rule':['udp and port 1234']}
        self.plug.configure(**options)
        packet1 = Ether() / IP(dst='127.0.0.1', src='1.2.3.4') / TCP(sport=4000)
        packet2 = Ether() / IP(dst='0.0.0.1', src='1.1.1.1') / UDP(sport=1234)
        #when
        self.plug.notifyPacket(packet1)
        result1 = self.alertMethod.called
        self.plug.notifyPacket(packet2)
        self.plug.getCheckInformation()
        #then
        self.assertFalse(result1)
        self.assertTrue(self.alertMethod.called)
        self.assertEquals(self.plug.getNumberData(), 2)
        self.assertTrue(self.notifMethod.called)
        self.assertEquals(self.plug.getNextNotification(), 10)

#------------------------------------------------------------------------------ 
# Sniffing report method
#------------------------------------------------------------------------------
    # Se quiere comprobar que dada una regla introducida como argumentos a la sonda
    # se crea correctamente una AlertRule
    def testAlertRuleFromString(self):
        #given
        inputRule = 'tcp and port 80'
        methodLoaded = 'sniffNetwork'
        probeId = self.plug.getProbeId()
        sReport = self.plug.getSniffingReportMethods()
        #when
        result = sReport.getAlertRuleFromString(inputRule, methodLoaded, probeId)
        #then 
        self.assertEquals(result.networkProto, DInDeS.NetworkProtocol.noNetworkProtocol)
        self.assertEquals(result.transportProto, DInDeS.TransportProtocol.tcp)
        self.assertEquals(result.direction, DInDeS.PacketDirection.noPacketDirection)
        self.assertEquals(result.ipAddress, '')
        self.assertEquals(result.port, 80)
        self.assertEquals(result.probeId, probeId)

    # Se quiere crear una AlertRule nueva para poder enviarla cuando se rompa una
    # regla establecida
    def testCreateAlertRule(self):
        #given
        inputRule = 'tcp and port 80'
        methodLoaded = 'sniffNetwork'
        packetSummary = 'Ether / IP / ICMP 192.168.122.1 > 192.168.2.101 echo-request 0 / Raw'
        probeId = self.plug.getProbeId()
        sReport = self.plug.getSniffingReportMethods()
        alertRule = sReport.getAlertRuleFromString(inputRule, methodLoaded, probeId)
        #when
        result = sReport.createBrokenRuleReport(alertRule, packetSummary)
        #then
        self.assertNotEquals(alertRule, result)
        self.assertEquals(result.networkProto, alertRule.networkProto)
        self.assertEquals(result.transportProto, alertRule.transportProto)
        self.assertEquals(result.direction, alertRule.direction)
        self.assertEquals(result.ipAddress, alertRule.ipAddress)
        self.assertEquals(result.port, alertRule.port)
        self.assertEquals(result.data, alertRule.data + ' (' + packetSummary + ')')






