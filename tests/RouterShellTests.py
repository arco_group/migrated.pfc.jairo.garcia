#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

from src.dindesConf import slices, directories

import Ice
Ice.loadSlice(slices['dindesSliceLocal'])
import DInDeS

from unittest import TestCase
from mock import Mock
from os import path
from time import asctime

from src.RouterShell import RouterShell
from src.Router import Router



#===============================================================================
# RouterShellTests class
#===============================================================================
class RouterShellTests(TestCase):
    def setUp(self):
        self.__picAndLogDir = directories['picAndLogDir']
        self.__generalLog = 'GeneralLog'

        self.action = DInDeS.MakeAction()
        self.action.transportProto = DInDeS.TransportProtocol.tcp
        self.action.direction = DInDeS.PacketDirection.src
        self.action.ipAddress = '0.0.0.1'
        self.action.port = 1234
        self.action.typeAction = DInDeS.Action.drop
        self.action.timestamp = asctime()

        self.iceInstance = Ice.initialize()

        self.routerName = 'routerName'
        self.router = Router(self.routerName)
        self.router.configureRouter(self.iceInstance)
        self.rProxy = self.router.getRouterProxy()

    def tearDown(self):
        pass

    def existsFile(self, filename):
        if path.isfile(filename):
            return True
        else:
            return False

    def readFile(self, filedir):
        f = open(filedir, 'r')
        info = f.readline()
        f.close()
        return info

#------------------------------------------------------------------------------ 
# actions tests
#------------------------------------------------------------------------------
    # Se crea una nueva acción correctamente y vemos como se añade a la lista de acciones
    # para realizar.
    def testCreateActionAndAddActionToSend(self):
        #given
        shell = RouterShell()
        #when
        shell.addActionToSend(self.action)
        #then
        self.assertEquals(len(shell.getActions()), 1)
        act = shell.getActions()[0]
        self.assertEquals(act.transportProto, DInDeS.TransportProtocol.tcp)
        self.assertEquals(act.direction, DInDeS.PacketDirection.src)
        self.assertEquals(act.ipAddress, '0.0.0.1')
        self.assertEquals(act.port, 1234)
        self.assertEquals(act.typeAction, DInDeS.Action.drop)

    # Se crea una acción correctamente y simulamos que se las mandamos al router.
    def testSendAction(self):
        #given
        shell = RouterShell()
        shell.addActionToSend(self.action)
        shell.sendActions = Mock()
        #when
        shell.sendActions('routerProxy')
        #then
        self.assertTrue(shell.sendActions.called)

    # Se quiere comprobar que se aplican correctamente las acciones que se envían
    # a un router dado
    def testApplySentActions(self):
        #given
        shell = RouterShell()
        shell.addActionToSend(self.action)
        shell.sendActions(self.rProxy)
        self.router.setAction = Mock()
        self.router.applyActions = Mock(return_value=DInDeS.RouterInfo())
        #when
        shell.sendActions(self.rProxy)
        #then
        self.assertTrue(self.router.applyActions.called)

    # Se quiere eliminar todas las acciones a aplicar que se han configurado en el 
    # router
    def testDeleteSentActions(self):
        #given
        self.router.getIpTablesManager().deleteActions = Mock()
        shell = RouterShell()
        shell.addActionToSend(self.action)
        shell.sendActions(self.rProxy)
        #when
        shell.deleteSentActions(self.rProxy)
        #then
        self.assertTrue(self.router.getIpTablesManager().deleteActions.called)

#------------------------------------------------------------------------------ 
# ping probes
#------------------------------------------------------------------------------
    # Se desea probar que la conexión entre la shell y un router dado es correcta
    def testSendPingToRouter(self):
        #given
        self.router.ping = Mock(return_value=DInDeS.RouterInfo())
        shell = RouterShell()
        #when  
        shell.pingRouter(self.rProxy)
        #then
        self.assertTrue(self.router.ping.called)

#------------------------------------------------------------------------------ 
# getRouterInfo tests
#------------------------------------------------------------------------------ 
    # Se quiere conocer las reglas que se han configurado en un Router dado
    def testGetRulesConfigured(self):
        #given
        shell = RouterShell()
        shell.addActionToSend(self.action)
        shell.sendActions(self.rProxy)
        #when
        routerActions = shell.getSentActions(self.rProxy)
        #then
        self.assertEquals(routerActions[0], self.action)

    # Se quiere conocer el nombre identificativo del router
    def testGetRouterName(self):
        #given 
        shell = RouterShell()
        #when
        name = shell.getRouterName(self.rProxy)
        #then
        self.assertEquals(name, self.routerName)

    # Se quiere conocer las direcciones las interfaces del Router
    def testGetRouterIfaces(self):
        #given
        shell = RouterShell()
        #when
        ifaces = shell.getRouterIfaces(self.rProxy)
        #then
        self.assertTrue(len(ifaces) > 0)

#------------------------------------------------------------------------------ 
# generalLog
#------------------------------------------------------------------------------
    # Se comprueba que cada vez que se manda una regla a un router se muestra la 
    # información en el log general
    def testShowInfoInGeneralLog(self):
        #given
        shell = RouterShell()
        shell.addActionToSend(self.action)
        #when
        shell.sendActions(self.rProxy)
        #then
        self.assertTrue(self.existsFile(self.__picAndLogDir + self.__generalLog + '.txt'))
        expectedFile = self.readFile(self.__picAndLogDir + self.__generalLog + '.txt')
        self.assertNotEquals(expectedFile, '')



