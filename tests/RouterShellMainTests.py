#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

from src.dindesConf import slices
from src.Exceptions import IncorrectActionException

import Ice
Ice.loadSlice(slices['dindesSliceLocal'])
import DInDeS

from unittest import TestCase
from mock import Mock

from src.RouterShellMain import RouterShellMain
from src.DindesSystem import DindesSystem



#===============================================================================
# RouterShellMain class
#===============================================================================
class RouterShellMainTest(TestCase):
    def setUp(self):
        self.iceInstance = Ice.initialize()
        self.shellArgs = {'routerProxy':'', 'delete':'', 'sent':'', 'name':'', 'ping':'', 'ifaces':''}
        self.shellArgs.update({'transportProto':'', 'direction':'', 'ipAddress': '', 'port':'', 'typeAction':''})
        self.shell = RouterShellMain()
        self.shell.getRouterProxies = Mock(return_value=['router1 @ -t subscribe', 'router2 @ -t subscribe'])

    def tearDown(self):
        pass

#------------------------------------------------------------------------------
# create actions tests 
#------------------------------------------------------------------------------ 
    # Se crea una nueva acción correctamente y vemos como se añade a la lista de acciones
    # para realizar.
    def testCreateAction(self):
        #given
        action = {'transportProto':'tcp', 'direction':'src', 'ipAddress':'127.0.0.1', 'port':1234, 'typeAction':'drop'}
        shell = RouterShellMain()
        #when
        shell.getRouterShell().createNewAction(action)
        #then
        self.assertEquals(len(shell.getRouterShell().getActions()), 1)
        act = shell.getRouterShell().getActions()[0]
        self.assertEquals(act.transportProto, DInDeS.TransportProtocol.tcp)
        self.assertEquals(act.direction, DInDeS.PacketDirection.src)
        self.assertEquals(act.ipAddress, '127.0.0.1')
        self.assertEquals(act.port, 1234)
        self.assertEquals(act.typeAction, DInDeS.Action.drop)

    # Se crea una acción en el que hay algún tipo de error. Se lanza un error y 
    # vemos como no se añade la acción a la lista de acciones a realizar.
    def testCreateIncorrectAction(self):
        #given
        action = {'transportProto':'qwer', 'direction':'src', 'ipAddress':'127.0.0.1', 'port':1234, 'typeAction':'bla'}
        shell = RouterShellMain()
        #when
        methodFail = shell.getRouterShell().createNewAction
        #then
        self.assertRaises(IncorrectActionException, methodFail, action)
        self.assertEquals(len(shell.getRouterShell().getActions()), 0)

    # Se crea una acción correctamente y simulamos que se las mandamos al router.
    # comprobamos que no hay acciones pendientes de enviarse.
    def testSendAction(self):
        #given
        action = {'transportProto':'udp', 'direction':'dst', 'ipAddress':'127.0.0.1', 'port':1000, 'typeAction':'drop'}
        shell = RouterShellMain()
        shell.getRouterShell().createNewAction(action)
        shell.getRouterShell().sendActions = Mock()
        #when
        shell.sendActionsToRouter('routerProxy')
        #then
        self.assertTrue(shell.getRouterShell().sendActions.called)

#------------------------------------------------------------------------------ 
# ping routers
#------------------------------------------------------------------------------
    # Se desea hacer ping a un router dado. 
    def testSendPingSignal(self):
        #given
        shell = RouterShellMain()
        shell.getRouterShell().pingRouter = Mock()
        #when
        shell.pingRouter('routerProxy')
        #then
        self.assertTrue(shell.getRouterShell().pingRouter.called)

#------------------------------------------------------------------------------ 
# getRouterInfo
#------------------------------------------------------------------------------ 
    # Se quiere conocer las reglas que se han enviado a un Router dado
    def testGettingSentActions(self):
        #given
        shell = RouterShellMain()
        shell.getRouterShell().getSentActions = Mock()
        #when
        shell.getSentActions('routerProxy')
        #then
        self.assertTrue(shell.getRouterShell().getSentActions.called)

    # Se quiere eliminar todas las reglas de filtrado que se han configurado en el 
    # Router
    def testDeleteRules(self):
        #given
        shell = RouterShellMain()
        shell.getRouterShell().deleteSentActions = Mock()
        #when
        shell.deleteActions('routerProxy')
        #then
        self.assertTrue(shell.getRouterShell().deleteSentActions.called)

    # Se quiere conocer el nombre identificativo del router
    def testGetRouterName(self):
        #given
        routerName = 'Router1'
        shell = RouterShellMain()
        shell.getRouterShell().getRouterName = Mock(return_value=routerName)
        #when
        name = shell.getRouterName('routerProxy')
        #then
        self.assertEquals(name, 'Router1')

    # Se quiere conocer las direcciones las interfaces del Router
    def testGetRouterIfaces(self):
        #given
        routerIfaces = {'eth0':'127.0.0.1'}
        shell = RouterShellMain()
        shell.getRouterShell().getRouterIfaces = Mock(return_value=routerIfaces)
        #when
        ifaces = shell.getRouterIfaces('routerProxy')
        #then
        self.assertTrue(len(ifaces) == 1)

#------------------------------------------------------------------------------ 
# Getting router proxies
#------------------------------------------------------------------------------
    # Se quiere obtener los proxys de todos los routers que están en el registry.
    # Se obtienen pasándole ningún argumento
    def testGetAllRouterProxies(self):
        #given
        ds = DindesSystem()
        options = 'DindesSystem'.split()
        ds.getRouterShellManager().getRouterProxies = Mock()
        #when
        ds.run(options)
        #then  
        self.assertTrue(ds.getRouterShellManager().getRouterProxies.called)

    # Se quiere obtener el proxy de un router, dada su posición, y obtenemos el 
    # proxy correcto
    def testGetRouterProxyByPosition(self):
        #given
        ds = DindesSystem()
        options = 'DindesSystem -r 0'.split()
        ds.getRouterShellManager().getRouterShellInstance().getRouterProxies = Mock()
        ds.getRouterShellManager().getRouterShellInstance().printRouterProxies = Mock()
        #when
        ds.run(options)
        #then
        self.assertTrue(ds.getRouterShellManager().getRouterShellInstance().getRouterProxies.called)

#------------------------------------------------------------------------------ 
# Router Shell Main Execution 
#------------------------------------------------------------------------------

    # Se quiere ejecutar la shell para añadir unas acciones a realizar. Se comprueba 
    # la acción, se manda al proxy que se le indica, y se aplica la acción 
    def testSendAndApplyActionFromCommandLine(self):
        #given
        options = self.shellArgs
        options['transportProto'] = 'tcp'
        options['typeAction'] = 'deny'
        options['routerProxy'] = 0

        self.shell.getRouterShell().createNewAction = Mock()
        self.shell.sendActionsToRouter = Mock()
        #when
        self.shell.startRouterShell(options, self.iceInstance)
        #then
        self.assertTrue(self.shell.getRouterShell().createNewAction.called)
        self.assertTrue(self.shell.sendActionsToRouter.called)

    # Se borran las acciones que se han aplicado en el router que se le indica.
    def testDeleteAppliedActionsFromCommandLine(self):
        #given
        options = self.shellArgs
        options['delete'] = True
        options['routerProxy'] = 0
        self.shell.deleteActions = Mock()
        #when
        self.shell.startRouterShell(options, self.iceInstance)
        #then
        self.assertTrue(self.shell.getRouterProxies.called)
        self.assertTrue(self.shell.deleteActions.called)

    # Se quiere hacer ping de un router dado, indicando el proxy del mismo
    def testPingRouterFromCommandLine(self):
        #given
        options = self.shellArgs
        options['ping'] = True
        options['routerProxy'] = 0
        self.shell.pingRouter = Mock()
        #when
        self.shell.startRouterShell(options, self.iceInstance)
        #then
        self.assertTrue(self.shell.getRouterProxies.called)
        self.assertTrue(self.shell.pingRouter.called)

    # Se quiere conocer las acciones que se han enviado a un router dado, conociendo
    # su proxy
    def testGetSentActionsFromCommandLine(self):
        #given
        options = self.shellArgs
        options['sent'] = True
        options['routerProxy'] = 1
        self.shell.getSentActions = Mock()
        #when
        self.shell.startRouterShell(options, self.iceInstance)
        #then
        self.assertTrue(self.shell.getSentActions.called)
        self.assertTrue(self.shell.getRouterProxies.called)

    # Se quiere conocer el nombreId de un router indicando el proxy del mismo
    def testGetRouterNameFromCommandLine(self):
        #given
        options = self.shellArgs
        options['name'] = True
        options['routerProxy'] = 1
        self.shell.getRouterName = Mock()
        #when
        self.shell.startRouterShell(options, self.iceInstance)
        #then
        self.assertTrue(self.shell.getRouterProxies.called)
        self.assertTrue(self.shell.getRouterName.called)

    # Se quiere conocer las interfaces de red que tiene un router dado, inidicando
    # el proxy del mismo
    def testGetRouterIfacesFromCommandLine(self):
        #given
        options = self.shellArgs
        options['ifaces'] = True
        options['routerProxy'] = 0
        self.shell.getRouterIfaces = Mock()
        #when
        self.shell.startRouterShell(options, self.iceInstance)
        #then
        self.assertTrue(self.shell.getRouterIfaces.called)
        self.assertTrue(self.shell.getRouterProxies.called)

    # Se quiere comprobar que se lanza un error cuando se intenta introducir varias 
    # reglas a la vez.
    def testInputServalOptionsAsRouterShellArguments(self):
        #given
        options = self.shellArgs
        options['ifaces'] = True
        options['name'] = True
        options['routerProxy'] = 1
        self.shell.getRouterName = Mock()
        self.shell.getRouterIfaces = Mock()
        #when
        self.shell.startRouterShell(options, self.iceInstance)
        #then
        self.assertFalse(self.shell.getRouterName.called)
        self.assertFalse(self.shell.getRouterIfaces.called)



