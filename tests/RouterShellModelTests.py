#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

from src.dindesConf import slices, directories

import Ice
Ice.loadSlice(slices['dindesSliceLocal'])
import DInDeS

from unittest import TestCase
from time import asctime
from os import path, remove

from src.RouterShellModel import RouterShellModel
from src.MonitorModel import MonitorModel



#===============================================================================
# RouterShellModelTests class
#===============================================================================
class RouterShellModelTests(TestCase):
    def setUp(self):
        self.picAndLogDir = directories['picAndLogDir']
        self.generalLog = 'GeneralLog'

        self.action = DInDeS.MakeAction()
        self.action.transportProto = DInDeS.TransportProtocol.tcp
        self.action.direction = DInDeS.PacketDirection.dst
        self.action.ipAddress = '123.123.123.123'
        self.action.port = 80
        self.action.typeAction = DInDeS.Action.drop
        self.action.timestamp = asctime()

    def tearDown(self):
        if self.existsFile(self.picAndLogDir + 'GeneralLog.txt'):
            remove(self.picAndLogDir + self.generalLog + '.txt')

    def existsFile(self, filename):
        if path.isfile(filename):
            return True
        else:
            return False

    def readFile(self, filedir):
        f = open(filedir, 'r')
        info = f.readline()
        f.close()
        return info

#------------------------------------------------------------------------------ 
# Router Shell Model Tests methods
#------------------------------------------------------------------------------
    # Se comprueba que se añade correctamente una regla creada al log general
    def testAddMakeActionInfoToGeneralLog(self):
        #given
        probeMonitorized = {'probeAux0' : {'probeId':'ProbeAux1ID', 'proxy':'ABC-123'}}
        mmodel = MonitorModel()
        mmodel.createDataBase(probeMonitorized)
        mmodel.createLogFiles()

        routerProxy = 'router1 @ routerNode.router'
        sModel = RouterShellModel()
        #when
        sModel.addInfoToLog(self.action, routerProxy)
        #then
        resultFile = self.readFile(self.picAndLogDir + self.generalLog + '.txt')
        expectFile = self.action.timestamp
        expectFile += ".- New action sent (Proto:'tcp', Dir:'dst', IP:'123.123.123.123', "
        expectFile += "Port:'tcp', Type:'drop') to Router: " + routerProxy + "\n"
        self.assertEquals(resultFile, expectFile)


