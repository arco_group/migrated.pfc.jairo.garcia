#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

import Ice

from unittest import TestCase
from mock import Mock

from src.Probe import Probe



#===============================================================================
# ProbeTests class
#===============================================================================
class ProbeTests(TestCase):
    def setUp(self):
        self.iceInstance = Ice.initialize()
        self.probe = Probe()

    def tearDown(self):
        pass

#------------------------------------------------------------------------------ 
# Configuration
#------------------------------------------------------------------------------
    # Le pasamos los argumentos correctamente, y vemos que se ejecutan los métodos
    # de configuración de la sonda. Nos devuelve 0 puesto que es correcto
    def testConfigureProbe(self):
        #given
        arguments = {'probeName': 'Sonda1', 'plugin':'bytesReceived', 'iface':'wlan0', 'timeval':'2', 'rule':['icemp'], 'promisc':False}
        self.probe.getProbeFacade().setInputArguments = Mock()
        self.probe.getProbeFacade().loadClass = Mock()
        self.probe.getProbeFacade().createProxyEndpoints = Mock()
        self.probe.getProbeFacade().createTopicManager = Mock()
        #when
        result = self.probe.configureProbe(arguments, self.iceInstance)
        #then
        self.assertEquals(result, 0)
        self.assertTrue(self.probe.getProbeFacade().setInputArguments.called)
        self.assertTrue(self.probe.getProbeFacade().loadClass.called)
        self.assertTrue(self.probe.getProbeFacade().createProxyEndpoints.called)
        self.assertTrue(self.probe.getProbeFacade().createTopicManager.called)

    # Le pasamos como argumentos un nombre de método a cargar desconocido y es 
    # erróneo. Se produce un error, y nos devuelve 1 confirmándolo
    def testConfigureProbeWithLoadingClassException(self):
        #given
        arguments = {'probeName': 'Sonda2', 'plugin':'blaReceived', 'iface':'wlan0', 'timeval':'2', 'rule':['icmp'], 'promisc':False}
        self.probe.getProbeFacade().createProxyEndpoints = Mock()
        #when
        result = self.probe.configureProbe(arguments, self.iceInstance)
        #then
        self.assertEquals(result, 1)
        self.assertFalse(self.probe.getProbeFacade().createProxyEndpoints.called)

    # Indicamos que queremos ejecutar la sonda y vemos como se ejecuta el método
    def testExecuteProbe(self):
        #given
        self.probe.getProbeFacade().executeMethod = Mock()
        #when
        result = self.probe.executeProbe()
        #then
        self.assertEquals(result, 0)
        self.assertTrue(self.probe.getProbeFacade().executeMethod.called)

    # Le indicamos que queremos ejecutar un método cargado, sin configurar previamente
    # la sonda. Se produce un error y nos devuelve 1 confirmándolo
    def testExecuteProbeWithExceptions(self):
        #given
        probe = Probe()
        #when
        result = probe.executeProbe()
        #then
        self.assertEquals(result, 1)



