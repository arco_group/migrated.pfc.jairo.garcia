#!/usr/bin/python
# -*- mode:python; coding:utf-8; tab-width:4 -*-

from src.dindesConf import slices
from src.Exceptions import ArgumentParsingException

import Ice
Ice.loadSlice(slices['dindesSliceLocal'])
import DInDeS

from unittest import TestCase
from time import asctime
from mock import Mock

from src.Router import Router, IptablesManager
from src.RouterMain import ArgumentManager



#===============================================================================
# RouterTests
#===============================================================================
class RouterTests(TestCase):
    def setUp(self):
        self.iceInstance = Ice.initialize()
        self.action = DInDeS.MakeAction()
        self.action.transportProto = DInDeS.TransportProtocol.tcp
        self.action.direction = DInDeS.PacketDirection.dst
        self.action.ipAddress = '123.123.123.123'
        self.action.port = 80
        self.action.typeAction = DInDeS.Action.drop
        self.action.timestamp = asctime()

        self.router = Router('routerId')

    def tearDown(self):
        pass

#------------------------------------------------------------------------------ 
# RouterMain
#------------------------------------------------------------------------------ 
    # Comprobamos que nos devuelve el correcto nombre del router cuando analizamos
    # los argumentos
    def testCheckArgumentsInRouterMain(self):
        #given
        argument = '-n routerName'.split()
        aManager = ArgumentManager()
        #when
        result = aManager.analyzeArguments(argument)
        #then
        self.assertEquals(result, 'routerName')

    # Comprobamos que si introducimos un argumento diferente al nombre nos produce
    # un error
    def testCheckUnknownArgumentInRouterMain(self):
        #given
        argument = '-e bla'.split()
        aManager = ArgumentManager()
        #when
        methodFail = aManager.analyzeArguments
        #then
        self.assertRaises(ArgumentParsingException, methodFail, argument)

#------------------------------------------------------------------------------ 
# Configure Router
#------------------------------------------------------------------------------
    # Inicializamos el router para que pueda recibir acciones y aplicarlas 
    def testConfigureRouter(self):
        #given
        router = Router('routerId')
        #when
        router.configureRouter(self.iceInstance)
        #then
        self.assertNotEquals(router.getRouterProxy(), None)

    # Inicializamos el router y obtenemos el nombre del mismo.
    def testGetRouterName(self):
        #given
        routerName = 'routerId'
        router = Router(routerName)
        router.configureRouter(self.iceInstance)
        #when
        name = router.getName()
        #then
        self.assertEquals(name, routerName)

    # Consultamos el número de interfaces de red con sus respectivas IPs
    def testGetRouterIfaces(self):
        #given
        router = Router('routerId')
        #when
        ifaces = router.getIfaces()
        #then
        self.assertTrue(len(ifaces) > 0)

#------------------------------------------------------------------------------ 
# Working with actions
#------------------------------------------------------------------------------
    # Se simula que se ha recibido una acción correcta (se comprueba cuando se envia
    # en el monitor) y vemos si se almacena correctamente en la lista de acciones 
    # a realizar
    def testSettingSeveralActions(self):
        #given
        action = self.action
        action2 = DInDeS.MakeAction()
        #when
        self.router.setAction(action)
        self.router.setAction(action2)
        #then
        self.assertEquals(len(self.router.getActionsToApply()), 2)

    # Se simula que se quiere aplicar las acciones que se han recibido. 
    def testApplyActions(self):
        #given
        self.router.getIpTablesManager().run = Mock()
        self.router.setAction(self.action)
        result = self.router.getActionsToApply()
        #when
        self.router.applyActions()
        #then
        self.assertEquals(len(result), 1)
        self.assertTrue(self.router.getIpTablesManager().run.called)

#------------------------------------------------------------------------------ 
# Iptables manager
#------------------------------------------------------------------------------ 
    # Se quiere comprobar que la transformación entre el objeto Ice MakeAction y
    # la instrucción iptable es correcta
    def testConvertMakeActionObjectToIptableRule(self):
        #given
        iManager = IptablesManager()
        #when
        rule = iManager.configureAction(self.action)
        #then
        finalRule = 'iptables -t filter -A FORWARD -p tcp -d 123.123.123.123 --dport 80 -j DROP '
        self.assertEquals(rule, finalRule)

    # Se quiere comprobar cuando se añade una regla sin ningún parámetro establecido
    def testConvertEmptyMakeActionToIptableRule(self):
        #given
        makeAction = DInDeS.MakeAction()
        iManager = IptablesManager()
        #when
        rule = iManager.configureAction(makeAction)
        #then
        finalRule = 'iptables -t filter -A FORWARD '
        self.assertEquals(rule, finalRule)



