#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

from src.LibvirtManager import LibvirtManager

from unittest import TestCase
from mock import Mock



#===============================================================================
# LibvirtManager tests
#===============================================================================
class LibvirtManagerTest(TestCase):
    def setUp(self):
        self.lvs = LibvirtManager()

    def tearDown(self):
        pass


#===============================================================================
# libvirtScript class tests
#===============================================================================
    def testAnalizeStartArgument(self):
        #given
        args = {'startVMName':['RouterOcelot'], 'startAllVM':False, 'stopVMName':[], 'stopAllVM':False, 'listNotRunning':False, 'listRunning':False}
        self.lvs.startVirtualMachineByName = Mock()
        self.lvs.stopVirtualMachineByName = Mock()
        self.lvs.listNotRunningMachines = Mock()
        self.lvs.listRunningMachines = Mock()
        #when
        self.lvs.run(args)
        #then
        self.assertTrue(self.lvs.startVirtualMachineByName.called)
        self.assertFalse(self.lvs.stopVirtualMachineByName.called)
        self.assertFalse(self.lvs.listNotRunningMachines.called)
        self.assertFalse(self.lvs.listRunningMachines.called)

    def testAnalizeStopArgument(self):
        #given
        args = {'startVMName':[], 'startAllVM':False, 'stopVMName':['RouterOcelot'], 'stopAllVM':False, 'listNotRunning':False, 'listRunning':False}
        self.lvs.startVirtualMachineByName = Mock()
        self.lvs.stopVirtualMachineByName = Mock()
        self.lvs.listNotRunningMachines = Mock()
        self.lvs.listRunningMachines = Mock()
        #when
        self.lvs.run(args)
        #then
        self.assertFalse(self.lvs.startVirtualMachineByName.called)
        self.assertTrue(self.lvs.stopVirtualMachineByName.called)
        self.assertFalse(self.lvs.listNotRunningMachines.called)
        self.assertFalse(self.lvs.listRunningMachines.called)

    def testAnalizeListNotRunningVM(self):
        #given
        args = {'startVMName':[], 'startAllVM':False, 'stopVMName':[], 'stopAllVM':False, 'listNotRunning':True, 'listRunning':False}
        self.lvs.startVirtualMachineByName = Mock()
        self.lvs.stopVirtualMachineByName = Mock()
        self.lvs.listNotRunningMachines = Mock()
        self.lvs.listRunningMachines = Mock()
        #when
        self.lvs.run(args)
        #then
        self.assertFalse(self.lvs.startVirtualMachineByName.called)
        self.assertFalse(self.lvs.stopVirtualMachineByName.called)
        self.assertTrue(self.lvs.listNotRunningMachines.called)
        self.assertFalse(self.lvs.listRunningMachines.called)

    def testAnalizeListRunningVM(self):
        #given
        args = {'startVMName':[], 'startAllVM':False, 'stopVMName':[], 'stopAllVM':False, 'listNotRunning':False, 'listRunning':True}
        self.lvs.startVirtualMachineByName = Mock()
        self.lvs.stopVirtualMachineByName = Mock()
        self.lvs.listNotRunningMachines = Mock()
        self.lvs.listRunningMachines = Mock()
        #when
        self.lvs.run(args)
        #then
        self.assertFalse(self.lvs.startVirtualMachineByName.called)
        self.assertFalse(self.lvs.stopVirtualMachineByName.called)
        self.assertFalse(self.lvs.listNotRunningMachines.called)
        self.assertTrue(self.lvs.listRunningMachines.called)

#------------------------------------------------------------------------------ 

    def testStartVirtualMachineByName(self):
        #given
        self.lvs.startVirtualMachineByName = Mock()
        vmName = self.lvs.listNotRunningMachines()[0]
        #when
        self.lvs.startVirtualMachineByName(vmName)
        #then
        self.assertTrue(self.lvs.startVirtualMachineByName.called)

    def testStopVirtualMachineByName(self):
        #given
        self.lvs.stopVirtualMachineByName = Mock()
        vmName = self.lvs.listNotRunningMachines()[0]
        #when
        self.lvs.stopVirtualMachineByName(vmName)
        #then
        self.assertTrue(self.lvs.stopVirtualMachineByName.called)



