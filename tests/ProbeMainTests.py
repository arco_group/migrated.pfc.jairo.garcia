#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*- 

from src.Exceptions import NoInputArgumentsIntroducedException, ArgumentParsingException
from src.Exceptions import UnknownNetworkIfaceException, NegativeTimevalSelectedException
from src.Exceptions import UnknownPromiscModeException

import Ice

from unittest import TestCase

from src.ProbeMain import ArgumentManager
from src.Probe import Probe



#===============================================================================
# ProbeMainTests class
#===============================================================================
class ProbeMainTests(TestCase):
    def setUp(self):
        self.__iceInstance = Ice.initialize()
        self.aManager = ArgumentManager()

    def tearDown(self):
        pass

#------------------------------------------------------------------------------ 
# Probe input arguments
#------------------------------------------------------------------------------
    # Le indicamos como argumentos una cadena vacía con ningún argumentos. El 
    # sistema lanzará un error.
    def testInputArgumentsWithNoArguments(self):
        #given
        options = ''.split()
        #when
        methodFail = self.aManager.analyzeArguments
        #then
        self.assertRaises(NoInputArgumentsIntroducedException, methodFail, options)

    # Le pasamos como argumentos una serie de argumentos válidos. Comprobamos que 
    # se ha creado el diccionario de argumentos correctamente.
    def testInputArgumentsProbeOk(self):
        #given
        options = '-n sonda2 -p sniffNetwork -i wlan0 -t 2 -r icmp'.split()
        #when
        result = self.aManager.analyzeArguments(options)
        #then
        finalConfig = {'probeName':'sonda2', 'plugin':'sniffNetwork', 'iface':'wlan0', 'timeval':2, 'rule':['icmp'], 'promisc':False , 'topicProxy':None}
        self.assertEquals(result, finalConfig)

    # Le pasamos los argumentos con alguna opción que es desconocida. Se lanza un
    # error.
    def testInputArgumentsWithError(self):
        #given
        options = '-p sniffNetwork -i wlan0 -y 3'.split()
        #when
        methodFail = self.aManager.analyzeArguments
        #then
        self.assertRaises(ArgumentParsingException, methodFail, options)

    # Le pasamos los argumentos con una interfaz de red que no existe. Se lanza
    # un error.
    def testInputArgumentsWithUnknownIface(self):
        #given
        options = '-p sniffNetwork -i bla0 -t 3'.split()
        #when
        methodFail = self.aManager.analyzeArguments
        #then
        self.assertRaises(UnknownNetworkIfaceException, methodFail, options)

    # Le pasamos como argumentos un intervalo de información negativo. Se lanza
    # un error.
    def testInputArgumentsWithNegativeInterval(self):
        #given
        options = '-p sniffNetwork -i wlan0 -t -123'.split()
        #when 
        methodFail = self.aManager.analyzeArguments
        #then
        self.assertRaises(NegativeTimevalSelectedException, methodFail, options)

    # Le pasamos como argumentos un valor erroneo como modo monitor. Se lanza un 
    # error.
    def testInputArgumentsWithUnknownPromiscMode(self):
        #given
        options = '-p sniffNetwork -i lo -t 3 -m asdf'.split()
        #when 
        methodFail = self.aManager.analyzeArguments
        #then
        self.assertRaises(UnknownPromiscModeException, methodFail, options)

    # Inicializamos la sonda introduciéndole argumentos correctos y comprobamos
    # que se ha creado el nombre de la sonda correctamente
    def testGetProbeId(self):
        #given
        options = '-n sonda -p sniffNetwork -i wlan0 -t 123'.split()
        args = self.aManager.analyzeArguments(options)
        probe = Probe()
        probe.configureProbe(args, self.__iceInstance)
        #when
        result = probe.getProbeId()
        #then
        self.assertEquals(result, 'sonda')



