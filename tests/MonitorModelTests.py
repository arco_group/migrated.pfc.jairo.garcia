#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

from src.dindesConf import slices, directories
from src.Exceptions import NoProbesMonitorizedException

import Ice
Ice.loadSlice(slices['dindesSliceLocal'])
import DInDeS

from unittest import TestCase
from time import asctime
from os import path, remove

from src.MonitorModel import MonitorModel



#===============================================================================
# MonitorModelTests class
#===============================================================================
class MonitorModelTests(TestCase):
    def setUp(self):
        self.databaseDir = directories['databaseDir']
        self.picAndLogDir = directories['picAndLogDir']

        self.alert = DInDeS.AlertRule()
        self.alert.methodName = DInDeS.MethodLoaded.bytesReceived
        self.alert.data = 'tcp and port 2345'
        self.alert.timestamp = str(asctime())
        self.alert.probeId = 'ProbeAlertAux'

        self.data = DInDeS.TimevalData()
        self.data.methodName = DInDeS.MethodLoaded.sniffNetwork
        self.data.data = "12341 / 999999"
        self.data.alertState = False
        self.data.timestamp = str(asctime())
        self.data.probeId = 'ProbeInfoAux'

        self.probeInfo = DInDeS.ProbeInfo()

    def tearDown(self):
        if self.existsFile(self.picAndLogDir + 'GeneralLog.txt'):
            remove(self.picAndLogDir + 'GeneralLog.txt')

    def existsFile(self, filename):
        if path.isfile(filename):
            return True
        else:
            return False

    def readFile(self, filedir):
        f = open(filedir, 'r')
        info = f.readline()
        f.close()
        return info

#------------------------------------------------------------------------------ 
# Monitor model methods
#------------------------------------------------------------------------------
    # Le pasamos las sondas que estamos monitorizando y creamos la base de datos rrd
    # de la última sonda a la que nos hemos subscrito. Comprobamos si hemos actualizado
    # correctamente el conjunto de sondas subscritas en MonitorModel.
    def testCreateDataBase(self):
        #given
        probeName = 'ProbeAux0'
        probeMonitorized = {probeName : {'probeId':'ProbeAux0ID', 'proxy':'ABC-123'}}
        mmodel = MonitorModel()
        #when
        mmodel.createDataBase(probeMonitorized)
        #then
        self.assertTrue(self.existsFile(self.databaseDir + probeName + '.rrd'))
        self.assertEquals(mmodel.getProbesMonitorized(), probeMonitorized)

    # Le indicamos que queremos crear los ficheros de log. Comprobamos si se han
    # creado correctamente
    def testCreateLogFiles(self):
        #given
        probeName = 'ProbeAux1'
        probeMonitorized = {probeName : {'probeId':'ProbeAux1ID', 'proxy':'ABC-123'}}
        mmodel = MonitorModel()
        mmodel.createDataBase(probeMonitorized)
        #when
        mmodel.createLogFiles()
        #then
        self.assertTrue(self.existsFile(self.picAndLogDir + probeName + 'Alerts.txt'))
        self.assertTrue(self.existsFile(self.picAndLogDir + probeName + 'Info.txt'))
        self.assertTrue(self.existsFile(self.picAndLogDir + probeName + 'Status.txt'))
        self.assertTrue(self.existsFile(self.picAndLogDir + 'GeneralLog.txt'))

    # Le indicamos que queremos crear los ficheros de log pero no hemos subscrito
    # ninguna sonda antes. Se lanza un error.
    def testCreateLogFilesWithNoProbeMonitorizedException(self):
        #given
        mmodel = MonitorModel()
        #when
        methodFail = mmodel.createLogFiles
        #then
        self.assertRaises(NoProbesMonitorizedException, methodFail)

#------------------------------------------------------------------------------ 
    # Le indicamos los datos de la información temporal que queremos añadir al
    # fichero de información y vemos si se ha añadido correctamente
    def testAddInformation(self):
        #given
        probeName = 'ProbeAux2'
        probeMonitorized = {probeName : {'probeId':'ProbeInfoAux', 'proxy':'ABC-123'}}
        mmodel = MonitorModel()
        mmodel.createDataBase(probeMonitorized)
        mmodel.createLogFiles()
        #when
        mmodel.addInformation(self.data)
        #then
        result = self.readFile(self.picAndLogDir + probeName + 'Info.txt')
        infoString = self.data.timestamp + '.- Info from ' + str(self.data.probeId)
        infoString += ' (AlertState: ' + str(self.data.alertState) + '): ' + str(self.data.data) + '\n'
        self.assertEquals(result, infoString)

    # Le indicamos los datos de la información temporal que queremos añadir al 
    # fichero de alertas y vemos si se ha añadido correctamente
    def testAddAlert(self):
        #given
        probeName = 'ProbeAux22'
        probeMonitorized = {probeName : {'probeId':'ProbeInfoAux', 'proxy':'ABC-123'}}
        mmodel = MonitorModel()
        mmodel.createDataBase(probeMonitorized)
        mmodel.createLogFiles()
        #when
        self.data.alertState = True
        mmodel.addInformation(self.data)
        #then
        result = self.readFile(self.picAndLogDir + probeName + 'Alerts.txt')
        infoString = self.data.timestamp + '.- Alert from ' + str(self.data.probeId)
        infoString += ' (AlertState: ' + str(self.data.alertState) + '): ' + str(self.data.data) + '\n'
        self.assertEquals(result, infoString)

    # Le indicamos los datos de la alerta que queremos añadir al fichero de log general
    # y vemos si se ha añadido correctamente
    def testAddAlertToGeneralLog(self):
        #given
        probeName = 'ProbeAux3'
        probeMonitorized = {probeName : {'probeId':'ProbeAlertAux', 'proxy':'ABC-123'}}
        mmodel = MonitorModel()
        mmodel.createDataBase(probeMonitorized)
        mmodel.createLogFiles()
        #when
        info = 'New broken rule from ' + str(self.alert.probeId)
        info += ': ' + self.alert.data + '\n'
        mmodel.addInfoToGeneralLog(info)
        #then
        result = self.readFile(self.picAndLogDir + 'GeneralLog.txt')
        self.assertEquals(result.split('.- ')[1], info)

#------------------------------------------------------------------------------
    # Le indicamos que queremos actualizar el estado de una sonda. Se simula que 
    # la sonda está operativa.
    def testSetProbeStatusOK(self):
        #given
        probeName = 'ProbeAux4'
        probeProxy = 'ABC-123'
        probeMonitorized = {probeName : {'probeId':'ProbeStatusAux', 'proxy':probeProxy}}
        self.probeInfo.alertState = False
        mmodel = MonitorModel()
        mmodel.createDataBase(probeMonitorized)
        mmodel.createLogFiles()
        #when
        mmodel.setProbeStatus(self.probeInfo, probeProxy)
        #then
        result = self.readFile(self.picAndLogDir + probeName + 'Status.txt')
        self.assertEquals(result, 'Ok')

    # Le indicamos que queremos actualizar el estado de una sonda. Se simula que 
    # la sonda está en estado de alerta.
    def testSetProbeStatusAlert(self):
        #given
        probeName = 'ProbeAux5'
        probeProxy = 'ABCDEF'
        probeMonitorized = {probeName : {'probeId':'ProbeStatusAux', 'proxy':probeProxy}}
        self.probeInfo.alertState = True
        mmodel = MonitorModel()
        mmodel.createDataBase(probeMonitorized)
        mmodel.createLogFiles()
        #when
        mmodel.setProbeStatus(self.probeInfo, probeProxy)
        #then
        result = self.readFile(self.picAndLogDir + probeName + 'Status.txt')
        self.assertEquals(result, 'Alert')

    # Le indicamos que queremos actualizar el estado de una sonda. Se simula que 
    # la sonda está caída.
    def testSetProbeStatusDown(self):
        #given
        probeName = 'ProbeAux6'
        probeProxy = 'ABC-123'
        probeMonitorized = {probeName : {'probeId':'ProbeStatusAux', 'proxy':probeProxy}}
        mmodel = MonitorModel()
        mmodel.createDataBase(probeMonitorized)
        mmodel.createLogFiles()
        #when
        mmodel.setProbeStatus(None, probeProxy)
        #then
        result = self.readFile(self.picAndLogDir + probeName + 'Status.txt')
        self.assertEquals(result, 'Down')

#------------------------------------------------------------------------------ 
    # Le indicamos que queremos crear los gráficos resumen de las informaciones 
    # temporales recibidas.
    def testCreateInformationGraph(self):
        #given
        probeName = 'ProbeAux7'
        probeMonitorized = {probeName : {'probeId':'ProbeAux6ID', 'proxy':'ABC-123'}}
        mmodel = MonitorModel()
        mmodel.createDataBase(probeMonitorized)
        #when
        mmodel.createInformationGraph()
        #then
        self.assertTrue(self.existsFile(self.picAndLogDir + probeName + 'Info.png'))

    # Le indicamos que queremos crear los gráficos resumen de las alertas recibidas
    def testCreateAlertGraph(self):
        #given
        probeName = 'probeAux8'
        probeMonitorized = {probeName : {'probeId':'ProbeAux7ID', 'proxy':'ABC-123'}}
        mmodel = MonitorModel()
        mmodel.createDataBase(probeMonitorized)
        #when
        mmodel.createAlertsGraph()
        #then
        self.assertTrue(self.existsFile(self.picAndLogDir + probeName + 'Alerts.png'))



