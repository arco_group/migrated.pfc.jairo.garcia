#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

from src.dindesConf import slices

import Ice
Ice.loadSlice(slices['dindesSliceLocal'])
import DInDeS

from unittest import TestCase
from mock import Mock
from threading import Event
from time import asctime

from src.Probe import Probe
from src.Monitor import Monitor
from src.Router import Router
from src.RouterShell import RouterShell



#===============================================================================
# MockMonitor class
#===============================================================================
class MockMonitor(Monitor):
    alert = Mock()
    timevalInformation = Mock()



#===============================================================================
# SystemTests class
#=============================================================================== 
class SystemTests(TestCase):
    def setUp(self):
        #Probe simulation
        self.iceInstance = Ice.initialize()
        arguments = {'probeName':'Sonda1', 'plugin':'bytesTransmitted', 'iface':'wlan0', 'timeval':'1', 'rule':['1234'], 'promisc':False, 'topicProxy':''}
        self.probe = Probe()
        self.p = self.probe.getProbeFacade()
        self.p.setInputArguments(arguments, self.iceInstance)
        self.p.loadClass()
        self.p.createProxyEndpoints()
        self.p.createTopicManager('TopicSystem')
        self.p.getPluginManager().getClassSelected().configure(**self.p.getInputArguments())

        self.monitor = MockMonitor()
        self.monitorProxy = None

        self.alertRule = DInDeS.AlertRule()
        self.alertRule.data = 'tcp and port 1234'

        self.data = DInDeS.TimevalData()

        self.action = DInDeS.MakeAction()
        self.action.transportProto = DInDeS.TransportProtocol.tcp
        self.action.direction = DInDeS.PacketDirection.src
        self.action.ipAddress = '127.0.0.1'
        self.action.port = 1234
        self.action.typeAction = DInDeS.Action.drop
        self.action.timestamp = asctime()

    def tearDown(self):
        if self.monitorProxy:
            self.probe.getProbeFacade().unsubscribeMonitor(self.monitorProxy, None)


#===============================================================================
# communication tests 
#===============================================================================
    # Probamos la comunicación entre una sonda y un monitor. Éste se subscribe a 
    # la sonda, con el proxy que calcula la sonda. Después se manda un mensaje
    # de información temporal desde la sonda al monitor
    def testNewComunicationBetweenMonitorAndProbes(self):
        #given
        event = Event()
        self.monitor.timevalInformation = Mock()
        self.monitor.timevalInformation.side_effect = lambda x, y: event.set()

        adapter = self.iceInstance.createObjectAdapterWithEndpoints('AdapterTest1', 'tcp')
        self.monitorProxy = adapter.addWithUUID(self.monitor)
        adapter.activate()

        self.p.subscribeMonitor(self.monitorProxy)
        #when
        pMethodLoaded = self.p.getPluginManager().getClassSelected().getMethodLoaded()
        pMethodLoaded()
        notifyMethod = self.p.getPluginManager().getClassSelected().getNotifMethod()
        notifyMethod(self.data)
        #then 
        event.wait(0.5)
        self.assertTrue(self.monitor.timevalInformation.called)

    # Probamos la comunicación entre la sonda y un monitor utilizando el método
    # de alerta. Se comprueba que se manda correctamente el mensaje de alerta.
    def testCommunicateAlertBetweenMonitorProbe(self):
        #given
        event = Event()
        self.monitor = MockMonitor()
        self.monitor.alert = Mock()
        self.monitor.alert.side_effect = lambda x, y: event.set()

        adapter = self.iceInstance.createObjectAdapterWithEndpoints('AdapterTest2', 'tcp')
        self.monitorProxy = adapter.addWithUUID(self.monitor)
        adapter.activate()

        self.p.subscribeMonitor(self.monitorProxy)
        #when
        pMethodLoaded = self.p.getPluginManager().getClassSelected().getMethodLoaded()
        pMethodLoaded()
        alertMethod = self.p.getPluginManager().getClassSelected().getAlertMethod()
        alertMethod(self.alertRule)
        #then 
        event.wait(0.5)
        self.assertTrue(self.monitor.alert.called)
        self.assertEquals(self.monitor.alert.call_args[0][0].data, self.alertRule.data)

    # Probamos que se envía correctamente la acción desde la shell al Router
    def testSendActionFromShellToRouter(self):
        #given 
        router = Router('routerId')
        router.configureRouter(self.iceInstance)
        routerProxy = router.getRouterProxy()
        shell = RouterShell()
        shell.addActionToSend(self.action)
        #when
        shell.sendActions(routerProxy)
        #then
        self.assertEquals(len(router.getActionsToApply()), 1)



