#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

from src.DindesSystem import DindesSystem

from unittest import TestCase
from mock import Mock



#===============================================================================
# Dindes Script Tests
#===============================================================================
class DindesScriptTests(TestCase):
    def setUp(self):
        self.dindes = DindesSystem()

    def tearDown(self):
        pass

#------------------------------------------------------------------------------
# launch tests
#------------------------------------------------------------------------------
    def testsLaunchMonitorNode(self):
        #given
        options = 'DindesSystem -M'.split()
        self.dindes.getMonitorNodeManager().start = Mock()
        #when
        self.dindes.run(options)
        #then
        self.assertTrue(self.dindes.getMonitorNodeManager().start.called)

    def testsCreateIcegridTemplate(self):
        #given
        options = 'DindesSystem -C'.split()
        self.dindes.getIcegridManager().create = Mock()
        #when
        self.dindes.run(options)
        #then
        self.assertTrue(self.dindes.getIcegridManager().create.called)

    def testsLaunchIcepatch(self):
        #given
        options = 'DindesSystem -H'.split()
        self.dindes.getIcegridManager().launchIcepatch = Mock()
        #when
        self.dindes.run(options)
        #then
        self.assertTrue(self.dindes.getIcegridManager().launchIcepatch.called)

    def testsLaunchIcegridadmin(self):
        #given
        options = 'DindesSystem -I'.split()
        self.dindes.getIcegridManager().launchIcegridadmin = Mock()
        #when
        self.dindes.run(options)
        #then
        self.assertTrue(self.dindes.getIcegridManager().launchIcegridadmin.called)

    def testsLaunchVirtualMachines(self):
        #given
        options = 'DindesSystem -s RouterOcelot'.split()
        self.dindes.getManageVMManager().start = Mock()
        #when
        self.dindes.run(options)
        #then
        self.assertTrue(self.dindes.getManageVMManager().start.called)

    def testsLaunchRouterShell(self):
        #given
        options = 'DindesSystem -t tcp'.split()
        self.dindes.getRouterShellManager().start = Mock()
        #when
        self.dindes.run(options)
        #then
        self.assertTrue(self.dindes.getRouterShellManager().start.called)

    def testsStartRunServer(self):
        #given
        options = 'DindesSystem -R'.split()
        self.dindes.getWebServerManager().start = Mock()
        #when
        self.dindes.run(options)
        #then
        self.assertTrue(self.dindes.getWebServerManager().start.called)



