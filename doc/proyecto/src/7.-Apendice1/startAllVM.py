class LibvirtManager():
    def __init__(self):
        self.conn = libvirt.open('qemu:///system')

    # ...

    def startAllVirtualMachines(self):
        allStoppedMachines = self.listNotRunningMachines()
        print 'Starting %d machines...' % len(allStoppedMachines)
        for vm in allStoppedMachines:
            self.startVirtualMachineByName([vm])
        print 'All VM started.'
