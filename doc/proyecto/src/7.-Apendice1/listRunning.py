class LibvirtManager():
    def __init__(self):
        self.conn = libvirt.open('qemu:///system')

    # ...

    def listRunningMachines(self):
        print 'Listing running machines...'
        runningMachines = []
        machinesIDs = self.conn.listDomainsID()
        for i in machinesIDs:
            machine = self.conn.lookupByID(i)
            runningMachines.append(machine.name())
        print runningMachines
        print 'Done.'
        return runningMachines
