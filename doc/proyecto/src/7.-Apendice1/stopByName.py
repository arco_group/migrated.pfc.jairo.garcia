class LibvirtManager():
    def __init__(self):
        self.conn = libvirt.open('qemu:///system')

    # ...

    def stopVirtualMachineByName(self, vmNames):
        for name in vmNames:
            vm = self.conn.lookupByName(name)
            print "Stopping %s..." % name
            vm.shutdown()
            print "%s stopped." % name
