class LibvirtManager():
    def __init__(self):
        self.conn = libvirt.open('qemu:///system')

    # ...

    def listNotRunningMachines(self):
        print 'Listing not running machines...'
        machines = self.conn.listDefinedDomains()
        print machines
        print 'Done.'
        return machines
