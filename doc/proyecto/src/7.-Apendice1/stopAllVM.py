class LibvirtManager():
    def __init__(self):
        self.conn = libvirt.open('qemu:///system')

    # ...

    def stopAllVirtualMachines(self):
        runningMachines = self.listRunningMachines()
        print 'Stopping %d machines...' % len(runningMachines)
        for i in runningMachines:
            self.stopVirtualMachineByName([i])
        print 'All VM stopped.'
