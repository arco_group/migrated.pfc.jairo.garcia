auto eth0
iface eth0 inet static
        address 192.168.122.2
        netmask 255.255.255.0
        network 192.168.122.0
        broadcast 192.168.122.255
auto eth1
iface eth1 inet static
        address 192.168.2.1
        netmask 255.255.255.0
        network 192.168.2.0
        broadcast 192.168.2.255
up route add -net 192.168.4.0 netmask 255.255.255.0 gw 192.168.2.2
up route add -net 0.0.0.0 netmask 0.0.0.0 gw 192.168.122.1
