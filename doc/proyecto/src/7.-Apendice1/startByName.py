class LibvirtManager():
    def __init__(self):
        self.conn = libvirt.open('qemu:///system')

    # ...

    def startVirtualMachineByName(self, vmNames):
        for name in vmNames:
            vm = self.conn.lookupByName(name)
            print "Starting %s..." % name
            vm.create()
            print "%s started." % name
