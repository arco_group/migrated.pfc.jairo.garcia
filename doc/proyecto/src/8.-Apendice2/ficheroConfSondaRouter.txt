#Node properties
IceGrid.Node.Name=<Nombre_del_nodo>
IceGrid.Node.Data=<Directorio_base_de_datos_de_la_sonda>
IceGrid.Node.Endpoints=default

IceGrid.Node.AllowRunningServersAsRoot=<Ejecución_como_superusuario>

Ice.Default.Locator=IceGrid/Locator:tcp -h <IP_Registry> -p <Puerto_Registry>

