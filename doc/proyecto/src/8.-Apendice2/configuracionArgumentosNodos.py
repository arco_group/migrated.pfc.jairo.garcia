#===============================================================================
# Probes
#===============================================================================
probes = [{'id':'0', 'activation':'always' , 'node':'MailServerMantis', 'plugin':'sniffNetwork', 'iface':'eth0', 'timestamp':'10', 'rule':'icmp', 'promisc':'False'},
    {'id':'1', 'activation':'always', 'node':'WebServerWolf', 'plugin':'packetsReceived', 'iface':'eth0', 'timestamp':'10', 'rule':'9999999', 'promisc':'False'},
    {'id':'2', 'activation':'always', 'node':'WorkStationRex', 'plugin':'sniffNetwork', 'iface':'eth0', 'timestamp':'10', 'rule':'tcp and port 80', 'promisc':'False'},
    ]

#===============================================================================
# Routers
#===============================================================================
routers = [{'id':'0', 'activation':'always', 'node':'RouterOcelot'},
           {'id':'1', 'activation':'always', 'node':'RouterRaven'},
           ]

#===============================================================================
# Monitor
#===============================================================================
monitor = {'activation':'manual', 'node':'Monitor'} 
