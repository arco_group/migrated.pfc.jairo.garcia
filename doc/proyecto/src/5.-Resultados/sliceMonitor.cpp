module DInDeS {
  interface Monitor {
    void alert(AlertRule rule);
    void timevalInformation(TimevalData info);
  };
};

