module DInDeS {
  enum MethodLoaded {noMethodLoaded, bytesTransmitted, bytesReceived, packetsTransmitted,
                   errorsTransmitted, errorsReceived, multicastTransmitted, packetsReceived,
                   multicastReceived, broadcastTransmitted, broadcastReceived, sniffNetwork};
  enum NetworkProtocol {noNetworkProtocol, arp, ip, ip6};
  enum TransportProtocol {noTransportProtocol, icmp, tcp, udp};
  enum PacketDirection {noPacketDirection, dst, src};
  enum Action {noAction, allow, drop, reject};
  dictionary < string, string > RouterIface;

  struct AlertRule {
    MethodLoaded methodName; // nombre del tipo de metodo cargado
    NetworkProtocol networkProto; // protocolo red
    TransportProtocol transportProto; // protocolo transporte
    PacketDirection direction; // indica si la ip es src o dst
    string ipAddress; // ip
    int port; // puerto
    string data; // informacion completa a enviar
    string timestamp; // cuando se ha roto una regla, se indica la hora
    string probeId; // se indica la id de la sonda de donde se ha roto la regla
  };

  struct TimevalData{
    MethodLoaded methodName; // nombre del metodo cargado
    string data; // informacion a enviar (ratio)
    bool alertState; // indica si la sonda esta en estado de alerta
    string timestamp; // indica hora a la que se manda la informacion
    string probeId; // indica la id de la sonda
  };

  struct MakeAction{
    TransportProtocol transportProto; // TransportProtocol, protocolo transporte
    PacketDirection direction; // indica si la ip es src o dst
    string ipAddress; // ip
    int port; // puerto
    Action typeAction; // Action, drop, reject, allow
    string timestamp; // se indica cuando se ha creado una regla
  };

  sequence < MakeAction > ActionList;

  struct ProbeInfo{
    string probeId; // id de la sonda
    bool alertState; // si la sonda esta en estado de alerta
  };

  struct RouterInfo{
    string routerName; // nombre del router
    RouterIface ifaces; // diccionario con las interfaces de red y su ip
    ActionList sentActions; // lista con las acciones enviadas al router
  };

  interface Monitor {
    void alert(AlertRule rule); // mensaje enviado cuando se produce una alerta
    void timevalInformation(TimevalData info); // mensaje enviado cuando se termina el periodo de informacion
  };

  interface ProbeQuery {
    ProbeInfo subscribeMonitor(Monitor * m); // subscribe un Monitor
    void unsubscribeMonitor(Monitor * m); // desubscribe un Monitor
    ProbeInfo ping(); // manda un mensaje ping
    int getNumberOfAlerts(); // devuelve el numero de alertas producidas hasta ese momento
    int getNumberOfData(); // devuelve el numero de datos almacenados hasta ese momento
  };

  interface RouterQuery {
    void setAction(MakeAction act); // establece una accion
    RouterInfo applyActions(); // aplica un conjunto de acciones
    RouterInfo deleteActions(); // borra las acciones enviadas
    RouterInfo ping(); // manda un mensaje ping
    RouterInfo getName(); // devuelve el id del router
    RouterInfo getIfaces(); // devuelve el nombre y las IP de las interfaces del router
    RouterInfo getActionsConfigured(); // devuelve las reglas que se le han enviado
  };

};
