module DInDeS {
  struct ProbeInfo{
    string probeId; 
    bool alertState; 
  };

  interface ProbeQuery {
    ProbeInfo subscribeMonitor(Monitor * m);
    void unsubscribeMonitor(Monitor * m); 
    ProbeInfo ping(); 
    int getNumberOfAlerts(); 
    int getNumberOfData(); 
  };
};
