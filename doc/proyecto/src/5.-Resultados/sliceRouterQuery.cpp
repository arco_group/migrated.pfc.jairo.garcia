module DInDeS {
  dictionary < string, string > RouterIfaces;
  sequence < MakeAction > ActionList;

  struct RouterInfo{
    string routerName; 
    RouterIfaces ifaces;
    ActionList sentActions;
  };

  interface RouterQuery {
    void setAction(MakeAction act);
    bool applyActions();
    bool deleteActions();
    RouterInfo ping();
    string getName();
    RouterIfaces getIfaces();
    ActionList getActionsConfigured();
  };
};
