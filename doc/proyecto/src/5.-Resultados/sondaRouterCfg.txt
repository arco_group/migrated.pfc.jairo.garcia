#Node properties
IceGrid.Node.Name=<Nombre_del_nodo>
IceGrid.Node.Data=db/
IceGrid.Node.Endpoints=default

IceGrid.Node.AllowRunningServersAsRoot=1

Ice.Default.Locator=IceGrid/Locator:tcp -h 127.0.0.1 -p 20001
