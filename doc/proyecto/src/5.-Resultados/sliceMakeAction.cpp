module DInDeS {
  enum TransportProtocol {noTransportProtocol, icmp, tcp, udp};
  enum PacketDirection {noPacketDirection, dst, src};
  enum Action {noAction, allow, drop, reject};

  struct MakeAction{
    TransportProtocol transportProto; 
    PacketDirection direction; 
    string ipAddress; 
    int port;
    Action typeAction; 
    string timestamp; 
  };
};
