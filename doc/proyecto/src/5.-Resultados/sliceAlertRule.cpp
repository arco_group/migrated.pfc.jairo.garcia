module DInDeS {
  enum MethodLoaded {noMethodLoaded, bytesTransmitted, bytesReceived, packetsTransmitted, errorsTransmitted, errorsReceived, multicastTransmitted, packetsReceived, multicastReceived, broadcastTransmitted, broadcastReceived, sniffNetwork};
  enum NetworkProtocol {noNetworkProtocol, arp, ip, ip6};
  enum TransportProtocol {noTransportProtocol, icmp, tcp, udp};
  enum PacketDirection {noPacketDirection, dst, src};

  struct AlertRule {
    MethodLoaded methodName;
    NetworkProtocol networkProto;
    TransportProtocol transportProto;
    PacketDirection direction; 
    string ipAddress; 
    int port; 
    string data; 
    string timestamp;
    string probeId; 
  };
};
