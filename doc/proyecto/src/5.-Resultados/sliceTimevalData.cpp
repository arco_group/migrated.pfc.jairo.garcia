module DInDeS {
  enum MethodLoaded {noMethodLoaded, bytesTransmitted, bytesReceived, packetsTransmitted, errorsTransmitted, errorsReceived, multicastTransmitted, packetsReceived, multicastReceived, broadcastTransmitted, broadcastReceived, sniffNetwork};

  struct TimevalData{
    MethodLoaded methodName;
    string data;
    bool alertState;
    string timestamp; 
    string probeId; 
  };
};
