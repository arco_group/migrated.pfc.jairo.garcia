#!/bin/bash
NAME=main

mkdir -p output

echo -e "Compilando "$NAME". Primera pasada.";
pdflatex -output-directory=output $NAME.tex 

bibtex ./output/$NAME.aux

echo -e "Compilando "$NAME". Segunda pasada.";
pdflatex -output-directory=output $NAME.tex

echo -e "Compilando "$NAME". Tercera pasada.";
pdflatex -output-directory=output $NAME.tex

mv output/$NAME.pdf .

cp $NAME.pdf Anteproyecto.pdf

rm $NAME.pdf
rm -r output

