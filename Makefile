# -*- mode: makefile-gmake; coding: utf-8 -*-

all:
#===============================================================================
# IceBox: subscription & events
#===============================================================================
startIcebox:
	cd src/;icebox --Ice.Config=./iceboxConf/icebox.cfg;

#===============================================================================
# DindesSystem
#===============================================================================
DindesSystem:
	cd src/;python DindesSystem.py $(options);

#===============================================================================
# launch tests
#===============================================================================
launchTests:
	cd src/;nosetests --with-progressive --progressive-editor=gedit ../tests/*.py;

launchGeneralVMTests:
	cd src/;nosetests --with-progressive --progressive-editor=gedit ../tests/testGeneral/GeneralVMTests.py;

#===============================================================================
# clean project
#===============================================================================
clean:
	$(shell find . -name "*.pyc" | xargs /bin/rm)
	$(shell find . -name "*.bz2" | xargs /bin/rm)
	$(RM) ./src/IcePatch2.sum
	$(RM) ./src/iceboxConf/db/*
	$(RM) ./src/icegridConf/db/monitor/* -r
	$(RM) ./src/icegridConf/db/registry/* -r
	$(RM) ./src/icegridConf/db/routerShell/* -r
	$(RM) ./tests/testGeneral/db/* -r
	$(RM) ./src/rrdtoolDatabase/*
	$(RM) ./src/web/web/static/info/*

deleteFilesDepatch:
	$(shell find . -name "*.pyc" | xargs /bin/rm)
	$(shell find . -name "*.bz2" | xargs /bin/rm)
	$(RM) ./src/IcePatch2.sum

